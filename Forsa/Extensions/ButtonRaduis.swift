//
//  ButtonRaduis.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/1/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
@IBDesignable
class ButtonRaduis: UIButton {

    
    override func awakeFromNib() {
        self.layer.masksToBounds = false
        self.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.layer.borderWidth = 1
        self.layer.cornerRadius = self.frame.size.height/2
        self.clipsToBounds = true
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.layer.masksToBounds = false
        self.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.layer.borderWidth = 1
        self.layer.cornerRadius = self.frame.size.height/2
        self.clipsToBounds = true
    }
}
