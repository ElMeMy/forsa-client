//
//  BandgeView.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/30/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
@IBDesignable

class BadgeView: UIView {
    
    override func awakeFromNib() {
        
        self.layoutIfNeeded()
        layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        layer.cornerRadius = 25.0
        layer.masksToBounds = true
        
    }
}
