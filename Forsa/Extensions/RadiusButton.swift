//
//  RadiusButton.swift
//  Alassem
//
//  Created by AAIT on 1/12/19.
//  Copyright © 2019 anader. All rights reserved.
//

import UIKit
@IBDesignable

class Radius_of_button: UIButton {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
}
