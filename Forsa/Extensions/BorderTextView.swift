//
//  BorderView.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/19/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit
@IBDesignable

class BorderTextView: UITextView {
    
    
    override func awakeFromNib() {
        self.layer.masksToBounds = false
        self.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.layer.borderWidth = 1
        self.layer.cornerRadius = self.frame.size.height/10
        self.clipsToBounds = true
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.layer.masksToBounds = false
        self.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.layer.borderWidth = 1
        self.layer.cornerRadius = self.frame.size.height/10
        self.clipsToBounds = true
    }
    
    
}
