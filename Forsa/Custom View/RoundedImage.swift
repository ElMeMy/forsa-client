//
//  RoundedImage.swift
//  Golden Store
//
//  Created by ElMemy on 4/17/18.
//  Copyright © 2018 Mohammed Elnaggar. All rights reserved.
//

import UIKit
@IBDesignable
class RoundedImage: UIImageView {

    @IBInspectable var cornerRaduis: CGFloat = 3.0 {
        didSet {
            self.layer.cornerRadius = cornerRaduis
        }
    }
    override func awakeFromNib() {
        self.layer.cornerRadius = cornerRaduis
        self.clipsToBounds = true
    }

    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.layer.cornerRadius = cornerRaduis
        self.clipsToBounds = true
    }
}
