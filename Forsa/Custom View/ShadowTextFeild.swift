//
//  shadow.swift
//  Vehicle support
//
//  Created by ahmed elmemy on 2/10/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
@IBDesignable
class ShadowTextFeild: UITextField {
    
    @IBInspectable var cornerRaduis: CGFloat = 3.0 {
        didSet {
            self.layer.shadowColor = UIColor.gray.cgColor
            self.layer.shadowOpacity = 0.4
            self.layer.shadowOffset = CGSize.zero
            self.layer.shadowRadius = 3
        }
    }
    
    
    override func awakeFromNib() {
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.4
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 3
    }
    
    override func prepareForInterfaceBuilder() {
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.4
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 3
    }
    
}
