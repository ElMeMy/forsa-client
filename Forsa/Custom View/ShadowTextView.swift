//
//  ShadowTextView.swift
//  Vehicle support
//
//  Created by ahmed elmemy on 2/15/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
@IBDesignable
class ShadowTextView: UITextView {

    @IBInspectable var cornerRaduis: CGFloat = 3.0 {
        didSet {
            var borderColor : UIColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1.0)
            self.layer.borderWidth = 0.5
            self.layer.borderColor = borderColor.cgColor
            self.layer.cornerRadius = 5.0
        }
    }
    
    
    override func awakeFromNib() {
        var borderColor : UIColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1.0)
        self.layer.borderWidth = 0.5
        self.layer.borderColor = borderColor.cgColor
        self.layer.cornerRadius = 5.0
    }
    
    override func prepareForInterfaceBuilder() {
        var borderColor : UIColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1.0)
        self.layer.borderWidth = 0.5
        self.layer.borderColor = borderColor.cgColor
        self.layer.cornerRadius = 5.0
    }
    
}
