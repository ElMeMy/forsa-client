//
//  BorderView.swift
//  Gifts
//
//  Created by Abdallah Nader on 6/18/18.
//  Copyright © 2018 anader. All rights reserved.
//

import UIKit

@IBDesignable
class BorderView: UIView {
    
    // if cornerRadius variable is set/changed, change the corner radius of the UIView
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
            layer.shadowOffset = CGSize(width: -1, height: 1)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
            layer.shadowOffset = CGSize(width: -1, height: 1)
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
            layer.shadowOffset = CGSize(width: -1, height: 1)
        }
    }
    
}
