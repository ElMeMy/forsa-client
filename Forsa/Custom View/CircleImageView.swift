//
//  CircleImageView.swift
//  Tattamn
//
//  Created by Abdallah Nader on 5/20/18.
//  Copyright © 2018 anader. All rights reserved.
//

import UIKit
@IBDesignable
class CircleImageView: UIImageView {

    override func awakeFromNib() {
        self.layer.masksToBounds = false
        self.layer.cornerRadius = self.frame.size.height/2
        self.clipsToBounds = true
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.layer.masksToBounds = false
        self.layer.cornerRadius = self.frame.size.height/2
        self.clipsToBounds = true
    }

}
