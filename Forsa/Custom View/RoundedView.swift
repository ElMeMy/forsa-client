//
//  RoundedView.swift
//  Golden Store
//
//  Created by Abdallah Nader on 4/17/18.
//  Copyright © 2018 Mohammed Elnaggar. All rights reserved.
//

import UIKit
@IBDesignable
class RoundedView: UIView {

    @IBInspectable var cornerRaduis: CGFloat = 3.0 {
        didSet {
            self.layer.cornerRadius = cornerRaduis
//            self.layer.borderWidth = 2.0
//            self.layer.borderColor = borderColor
            self.layer.shadowRadius = 1.0
            self.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }
    }
    
//    @IBInspectable var borderColor: CGColor = #colorLiteral(red: 0.3993991017, green: 0.7978107929, blue: 0.7646084428, alpha: 1) {
//        didSet {
//            self.layer.borderColor = borderColor
//        }
//    }
    override func awakeFromNib() {
        self.layer.cornerRadius = cornerRaduis
//        self.layer.borderWidth = 2.0
//        self.layer.borderColor = borderColor
        self.layer.shadowRadius = 1.0
        self.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.layer.cornerRadius = cornerRaduis
//        self.layer.borderWidth = 2.0
//        self.layer.borderColor = borderColor
        self.layer.shadowRadius = 1.0
        self.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    }

}
