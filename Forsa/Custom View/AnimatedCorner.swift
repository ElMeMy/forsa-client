//
//  AnimatedCorner.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/12/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit
@IBDesignable

class AnimatedCorner: UITextField {
    
    override func awakeFromNib() {
        self.layer.cornerRadius = 25.0
      
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.layer.cornerRadius = 25.0
    }
    
}
