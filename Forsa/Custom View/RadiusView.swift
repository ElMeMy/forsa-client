//
//  RadiusView.swift
//  Vehicle support
//
//  Created by ahmed elmemy on 2/18/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
@IBDesignable
class RadiusView: UIView {
    
    override func awakeFromNib() {
        
        self.layoutIfNeeded()
        layer.cornerRadius = self.frame.height / 2.0
        layer.masksToBounds = true
        
    }
}
