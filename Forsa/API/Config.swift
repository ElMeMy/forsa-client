//
//  Config.swift
//  Golden Store
//
//  Created by Mohammed Elnaggar on 4/16/18.
//  Copyright © 2018 Mohammed Elnaggar. All rights reserved.
//

import Foundation

struct URLs {
    
    //Base URL
    static let BASE = "http://forsa.4hoste.com/ProviderIos/"
    static let BASE_2 = "http://forsa.4hoste.com/ClientIos/"
    static let Google_GeoCoding = "https://maps.googleapis.com/maps/api/geocode/json?latlng="

    static let Login                  = BASE + "login"
    static let GetOrder               = BASE_2 + "GetOrder"
    static let GetNotifaction         = BASE_2 + "GetNotifaction"
    static let OrderDetails           = BASE_2 + "OrderDetails"
    static let ChangeStutes           = BASE_2 + "ChangeStutes"
    static let GetSetting             = BASE_2 + "GetSetting"
    static let Pay                    = BASE_2 + "Pay"
    static let GetDataOfClient        = BASE_2 + "GetDataOfClient"
    static let UpdateUserData         = BASE_2 + "UpdateUserData"
    static let UpdateUserProvider     = BASE   + "UpdateUserData"
    static let GetDataOfProvider      = BASE   + "GetDataOfProvider"
    static let GetCities              = BASE   + "GetCities"
    static let GetRegion              = BASE   + "GetRegion"
    static let GetJob_title           = BASE   + "GetJob_title"
    static let Condtion_For_client    = BASE_2 + "Condtion_For_client"
    static let AboutUs_For_client     = BASE_2 + "AboutUs_For_client"
    static let RegisterClient         = BASE_2 + "Register"
    static let ReSendCode             = BASE_2 + "ReSendCode"
    static let ChangePasswordByCode   = BASE_2 + "ChangePasswordByCode"
    static let ChangePassword         = BASE_2 + "ChangePassword"
    static let contactUs_for_client   = BASE_2 + "contactUs_for_client"
    static let AddAdvertsment         = BASE_2 + "AddAdvertsment"
    static let GetMyAdvert            = BASE_2 + "GetMyAdvert"
    static let EditAdvertsment        = BASE_2 + "EditAdvertsment"
    static let AdvertDescription      = BASE_2 + "AdvertDescription"
    static let GetNotifyByClient      = BASE_2 + "GetNotifyByClient"

    
    //Provider
    static let ReSendCodeProvider     = BASE + "ReSendCode"
    static let ConfirmCodeProvider    = BASE + "ConfirmCodeRegister"
    static let Search                 = BASE + "Search"
    static let AdvertsmentDetails     = BASE + "AdvertsmentDetails"
    static let AddOrder               = BASE + "AddOrder"
    static let GetNotifactionProvider = BASE + "GetNotifaction"
    static let GetOrders              = BASE + "GetOrder"
    static let registerURL            = BASE + "Register"
    static let contactUs_for_provider = BASE + "contactUs_for_provider"
    static let EditBranch             = BASE + "EditBranch"
    static let GetNotifyByprovider             = BASE + "GetNotifyByprovider"



 }



