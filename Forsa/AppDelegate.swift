//
//  AppDelegate.swift
//  Vehicle support
//
//  Created by ahmed elmemy on 2/7/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SlideMenuControllerSwift
import NVActivityIndicatorView
import Firebase
import UserNotifications
import NotificationBannerSwift
import NotificationCenter
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
//        Language.setAppLanguage(lang: "en")

        
        NVActivityIndicatorView.DEFAULT_TYPE = .ballTrianglePath
        NVActivityIndicatorView.DEFAULT_COLOR = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
        
        // Keyboard Work
        IQKeyboardManager.shared.enable = true
        // status bar work
        SlideMenuOptions.hideStatusBar = false
        
//        Language.setAppLanguage(lang: "en")
        //status bar color
        
        
        //change language for localization
        Localizer.DoTheExhange()
        split(txt: Language.currentLanguage())
        
        if Language.currentLanguage().contains("en") {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        } else {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        
        
        
        
        UIApplication.shared.statusBarStyle = .lightContent
        
         let type_user = UserDefaults.standard.value(forKey: "type_user") as? String
         let lang = Language.currentLanguage()

        if  type_user == "client"{
            
            if lang == "en"
            {
                let vc2 = Storyboard.Home.instantiate(MainVc.self)
                let right = Storyboard.Home.instantiate(SideMenuVC.self)
                let sideMenu = SlideMenuController(mainViewController: vc2, leftMenuViewController: right)
                self.window?.rootViewController = sideMenu
            }
            else
            {
                let vc2 = Storyboard.Home.instantiate(MainVc.self)
                let right = Storyboard.Home.instantiate(SideMenuVC.self)
                let sideMenu = SlideMenuController(mainViewController: vc2, rightMenuViewController: right)
                self.window?.rootViewController = sideMenu
            }
            
        }
        else if type_user == "provider"
        {
            
            if lang == "en"
            {
                let vc2 = Storyboard.Home.instantiate(MainProviderVc.self)
                let right = Storyboard.Home.instantiate(SideMenuVC.self)
                let sideMenu = SlideMenuController(mainViewController: vc2, leftMenuViewController: right)
                self.window?.rootViewController = sideMenu
            }
            else
            {
                let vc2 = Storyboard.Home.instantiate(MainProviderVc.self)
                let right = Storyboard.Home.instantiate(SideMenuVC.self)
                let sideMenu = SlideMenuController(mainViewController: vc2, rightMenuViewController: right)
                self.window?.rootViewController = sideMenu
            }
        }
        else
        {
            let vc = Storyboard.Authentication.instantiate(LangVc.self)
            self.window?.rootViewController = vc
        }
        //
      
        //
                FirebaseApp.configure()
                Messaging.messaging().delegate = self
        // Notifications
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        return true
    }
    
    
    func split(txt: String){
        let splitted = txt.split { ["-"].contains($0.description) }
        print("✈️\(splitted)")
        if splitted[0] == "en"{
            Language.setAppLanguage(lang: "en")
            
        }else if splitted[0] == "ar"{
            Language.setAppLanguage(lang: "ar")
            
        }else {
            Language.setAppLanguage(lang: "en")
            
        }
    }

    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}



@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
        func userNotificationCenter(_ center: UNUserNotificationCenter,
                                    willPresent notification: UNNotification,
                                    withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
            let userInfo = notification.request.content.userInfo
    
            if let messageID = userInfo[gcmMessageIDKey] {
                print("Message ID: \(messageID)")
                // handle notification  message
                print(userInfo)
                let dict = userInfo[AnyHashable("aps")] as! [String : Any]
                print(dict)
                let al = dict["alert"] as! [String:Any]
                //get data
//                let banner = NotificationBanner(title: al["title"] as! String, subtitle: (al["body"] as! String ), leftView: nil, rightView: nil, style: BannerStyle.info, colors: CustomBannerColors())
//                banner.subtitleLabel?.textAlignment = .right
//                banner.titleLabel?.textAlignment = .right
//                banner.onTap = {
//                    let vc = Storyboard.Home.instantiate(NotificationsVc.self)
//                    self.window?.rootViewController = vc
//                }
//                banner.show()
            }
            // Print full message.
            print(userInfo)
    
            // Change this to your preferred presentation option
            completionHandler([])
        }
    
        func userNotificationCenter(_ center: UNUserNotificationCenter,
                                    didReceive response: UNNotificationResponse,
                                    withCompletionHandler completionHandler: @escaping () -> Void) {
            let userInfo = response.notification.request.content.userInfo
            // Print message ID.
            if let messageID = userInfo[gcmMessageIDKey] {
                print("Message ID: \(messageID)")
            }
            // Print full message.
            print(userInfo)
            // handle notification  message
            let dict = userInfo[AnyHashable("aps")] as! [String : Any]
            let al = dict["alert"] as! [String:Any]
            print(dict )
            //here handle notify
            //========================
//            let banner = NotificationBanner(title: al["title"] as! String, subtitle: (al["body"] as! String ), leftView: nil, rightView: nil, style: BannerStyle.info, colors: CustomBannerColors())
//            banner.subtitleLabel?.textAlignment = .right
//            banner.titleLabel?.textAlignment = .right
//            banner.onTap = {
//                let vc = Storyboard.Home.instantiate(NotificationsVc.self)
//                self.window?.rootViewController = vc
//            }
//            banner.show()
            //=========================
            completionHandler()
        }
}
//
extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        UserDefaults.standard.setValue(fcmToken, forKey: "mobileToken")
    }


    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")

    }

}
//
public protocol BannerColorsProtocol {
    func color(for style: BannerStyle) -> UIColor
}
class CustomBannerColors: BannerColorsProtocol {

    internal func color(for style: BannerStyle) -> UIColor {
        switch style {
        case .danger:   return UIColor(red:0.90, green:0.31, blue:0.26, alpha:1.00)
        case .info:     return UIColor(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            //            #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            //            #colorLiteral(red: 0.9477240443, green: 0.3579949141, blue: 0.506521821, alpha: 1)
        //            #colorLiteral(red: 0.9137254902, green: 0.7725490196, blue: 0.2745098039, alpha: 1)
        case .none:     return UIColor.clear
        case .success:  return UIColor(red:0.22, green:0.80, blue:0.46, alpha:1.00)
        case .warning:  return UIColor(red:1.00, green:0.66, blue:0.16, alpha:1.00)
        }
    }
}
