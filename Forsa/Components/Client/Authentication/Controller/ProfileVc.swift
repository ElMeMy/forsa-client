//
//  ProfileVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/20/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class ProfileVc: UIViewController {

    
    
    public var profileView: ProfileVIew! {
        guard isViewLoaded else { return nil }
        return (view as! ProfileVIew)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            profileView.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)

        }
        else
        {
            profileView.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        
        profileView.EditButton.setImage(#imageLiteral(resourceName: "edit"), for: .normal)
    }
    override func viewWillAppear(_ animated: Bool) {
        Profile()
    }

    @IBAction func EditPressed(_ sender: Any) {
        let vc = Storyboard.Authentication.instantiate(EditProfileVc.self)
        present(vc,animated: true,completion: nil)
    }
    
    @IBAction func BackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
