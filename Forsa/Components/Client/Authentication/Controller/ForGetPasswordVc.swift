//
//  ForGetPasswordVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/24/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class ForGetPasswordVc: UIViewController {

    
    public var ForgetView: ForGetView! {
        guard isViewLoaded else { return nil }
        return (view as! ForGetView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let lang = Language.currentLanguage()
        if lang == "en"
        {
            ForgetView.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)

        }
        else
        {
            ForgetView.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        ForgetView.BgImg.image = #imageLiteral(resourceName: "bg")
        ForgetView.LogoImg.image = #imageLiteral(resourceName: "logo_splash")
    }
    @IBAction func SendCodePreesed(_ sender: Any) {
        Forget()
    }
    

    @IBAction func BackPreesed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
