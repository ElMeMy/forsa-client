//
//  EditNewPasswordVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/24/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class EditNewPasswordVc: UIViewController {

    
    public var editpassword: ChangePasswordView! {
        guard isViewLoaded else { return nil }
        return (view as! ChangePasswordView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            editpassword.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)

        }
        else
        {
            editpassword.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        editpassword.LogoImg.image = #imageLiteral(resourceName: "logo_splash")
        editpassword.BgImg.image = #imageLiteral(resourceName: "bg")

    }
    
    @IBAction func SendPreesed(_ sender: Any) {
        EditPassword()
    }
    
    @IBAction func BackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    


}
