//
//  NewPasswordVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/24/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class NewPasswordVc: UIViewController {

    var phone :String?
    public var PasswordLogin: ResendPassword! {
        guard isViewLoaded else { return nil }
        return (view as! ResendPassword)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        let lang = Language.currentLanguage()
        if lang == "en"
        {
            PasswordLogin.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)

        }
        else
        {
            PasswordLogin.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        PasswordLogin.LogoImage.image = #imageLiteral(resourceName: "logo_splash")
        PasswordLogin.BgImg.image = #imageLiteral(resourceName: "bg")
    }
    
    @IBAction func SendPreesed(_ sender: Any) {
        resendPassword()
    }
    
   

}
