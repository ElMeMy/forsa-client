//
//  EditBranchVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/16/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
import SwiftyJSON
import SlideMenuControllerSwift
class EditBranchViewController: UIViewController {
    
    var BranchDataItem  = [BrachDataModel]()

    @IBOutlet weak var BranchTableVIew: UITableView!
    @IBOutlet weak var BackOutlet: UIButton!
    
    var DeleteAction: (()->())?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        BackOutlet.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)
        
        BranchTableVIew.delegate = self
        BranchTableVIew.dataSource = self
        
        GetDataOfProvider()

    }
    @IBAction func DeleteAction(_ sender: Any) {
        DeleteAction?()
    }
    

    @IBAction func BackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func EditBranchPressed(_ sender: Any) {
        
        var dictArray = [[String: Any]]()
        
       
        
        for i in BranchDataItem {
            let jsonObj = convertToDeictitem(item: i)
            dictArray.append(jsonObj)
        }
        
        print(dictArray)
        
        let user_id = UserDefaults.standard.value(forKey: "Id") as? Int ?? 0

        
        let parameters = [
            "provider_id" : user_id,
            "branch": JSON(dictArray).rawString() as AnyObject,
            ] as [String : Any]
        
        self.startAnimating()
        
        API.POST(url: URLs.EditBranch, parameters: parameters, headers: nil, completion: {
            (success, value) in
            
            self.stopAnimating()
            
            if success {
                
                if let returnValue = value["key"] as? Int {
                    
                    if returnValue == 0 {
                        let msg = value["msg"]
                        
                        let alert = UIAlertController(title: "ERROR".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    if returnValue == 1 {
                        
                        
                        let type_user = UserDefaults.standard.value(forKey: "type_user") as? String
                        let lang = Language.currentLanguage()
                        
                        if type_user == "provider"
                        {
                            
                            if lang == "en"
                            {
                                let vc2 = Storyboard.Home.instantiate(MainProviderVc.self)
                                let right = Storyboard.Home.instantiate(SideMenuVC.self)
                                let sideMenu = SlideMenuController(mainViewController: vc2, leftMenuViewController: right)
                                self.view.window?.rootViewController = sideMenu
                            }
                            else
                            {
                                let vc2 = Storyboard.Home.instantiate(MainProviderVc.self)
                                let right = Storyboard.Home.instantiate(SideMenuVC.self)
                                let sideMenu = SlideMenuController(mainViewController: vc2, rightMenuViewController: right)
                                self.view.window?.rootViewController = sideMenu
                                
                            }
                        }
                        
                    }
                } else {
                    
                    let msg = value["msg"] as? String
                    let alert = UIAlertController(title: "ERROR".localized(), message: msg!, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                
            } else {
                
            }
        })
        

    }
    
    
    @IBAction func AddBranch(_ sender: Any) {
        let vc = Storyboard.Authentication.instantiate(AddBranchVc.self)
        vc.branchDataItem = self.BranchDataItem
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    func GetDataOfProvider(){
        
        let user_id = UserDefaults.standard.value(forKey: "Id") as? Int ?? 0
        
        
        let parameters = [
            "provider_id" : user_id,
            
            ] as [String : Any]
        
        self.startAnimating()
        
        API.POST(url: URLs.GetDataOfProvider, parameters: parameters, headers: nil, completion: {
            (success, value) in
            
            self.stopAnimating()
            if success {
                
                if let returnKey = value["key"] as? Int {
                    if returnKey == 1 {
                        let provider = value["provider"]!["branches"] as? [[String: Any]]
                        
                        self.BranchDataItem = []
                        for i in provider!
                        {
                            let city = i["city"] as? String ?? ""
                            let Id   = i["Id"] as? Int ?? 0

                            let Region = i["Region"] as? String ?? ""

                            
                            let city_id = i["city_id"] as? Int ?? 0

                            
                            let street = i["street"] as? String ?? ""

                            
                            let region = i["region"] as? Int ?? 0

                            
                            let work_hours = i["work_hours"] as? String ?? ""

                            
                            
                            self.BranchDataItem.append(BrachDataModel(id: Id, TheCity: city, Region: Region, Street: street, Hour: work_hours, cityId: city_id, regionId: region))
                            
                        }
                        
                        self.BranchTableVIew.reloadData()

                    }
                }
            } else {
                
            }
        })
        
    }
    
    
}






extension EditBranchViewController: UITableViewDataSource, UITableViewDelegate{
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return BranchDataItem.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "EditBrachCell") as? EditBrachCell
        {
            let Call = BranchDataItem[indexPath.row]
            cell.updateView(BranchDataItem: Call)
            cell.DeleteAction =
            {
                    self.BranchDataItem.remove(at: indexPath.row)
                    self.BranchTableVIew.reloadData()
            }
            return cell
        }
        else
        {
            return EditBrachCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 250
    }
    
    
    func convertToDeictitem(item: BrachDataModel) -> [String: Any] {
        return ["city_id": item.cityId!, "region": item.regionId!, "street": item.Street!, "work_hours": item.Hour!]
    }
    
}

