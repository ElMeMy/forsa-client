//
//  LoginClient.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/17/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class LoginClientVc: UIViewController {

    
    public var LoginClientModel: LoginClientModel? {
        didSet {
            LoginClientLayer()
        }
    }
    

    public var LoginClient: LoginClientView! {
        guard isViewLoaded else { return nil }
        return (view as! LoginClientView)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LoginClient.BgImg.image = #imageLiteral(resourceName: "bg")
        LoginClient.LogoImg.image = #imageLiteral(resourceName: "logo_splash")
    }
    @IBAction func LoginPreesed(_ sender: Any) {
        LoginClientLayer()
    }
    
    @IBAction func RegisterPressed(_ sender: Any) {
        let vc = Storyboard.Authentication.instantiate(RegisterClientVc.self)
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func ForGetPawordPressed(_ sender: Any) {
        let vc = Storyboard.Authentication.instantiate(ForGetPasswordVc.self)
        self.present(vc, animated: true, completion: nil)
    }
    
    


}
