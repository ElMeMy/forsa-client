//
//  LoginProviderClientVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/17/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class LoginProviderClientVc: UIViewController {

    
    public var LoginCPView: LoginClientProviderView! {
        guard isViewLoaded else { return nil }
        return (view as! LoginClientProviderView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       LoginCPView.BackGroundImage.image = #imageLiteral(resourceName: "bg")
       LoginCPView.LogoImg.image = #imageLiteral(resourceName: "logo_splash")
    }
    
    
    
    @IBAction func LoginClient(_ sender: Any) {
        let vc = Storyboard.Authentication.instantiate(LoginClientVc.self)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func LoginProvider(_ sender: Any) {
        let vc = Storyboard.Authentication.instantiate(LoginProviderVc.self)
        self.present(vc, animated: true, completion: nil)

    }
    
}
