//
//  RegisterClientVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/17/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
import BEMCheckBox
class RegisterClientVc: UIViewController {

    var City      = [CityModel]()
    var Region    = [RegionModel]()
//    var Jobs      = [JobModel]()
    var Gender    = [GenderModel]()
    var Moahel    = [MoahelModel]()
    let pickerViewCity   = UIPickerView()
    let pickerViewRegion = UIPickerView()
    let pickerViewJop    = UIPickerView()
    let pickerViewGender = UIPickerView()
    let pickerViewMoahel = UIPickerView()

    let datePicker = UIDatePicker()

    
    var city_id   : Int?
    var Region_id : Int?
//    var Job_id    : Int?
    var Gender_id : String?
    var Moahed_id : String?
    var emptyCity = false
//    var Dates = String()

    
    public var registerView: RegisterClinetView! {
        guard isViewLoaded else { return nil }
        return (view as! RegisterClinetView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        registerView.down2Image.image = #imageLiteral(resourceName: "down")
        registerView.down3Image.image = #imageLiteral(resourceName: "down")
        registerView.down4Image.image = #imageLiteral(resourceName: "down")
        registerView.down5Image.image = #imageLiteral(resourceName: "down")
        registerView.ClenderImage.image = #imageLiteral(resourceName: "calendar")
        
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            registerView.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)

        }
        else
        {
            registerView.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        
        pickerViewCity.delegate = self
        registerView.CityTf.inputView = pickerViewCity
        pickerViewCity.tag = 1
        
        
        pickerViewRegion.delegate = self
        registerView.RegionTf.inputView = pickerViewRegion
        pickerViewRegion.tag = 2
        
        
//        pickerViewJop.delegate = self
//        registerView.JopTitle.inputView = pickerViewJop
//        pickerViewJop.tag = 3
        
        
        pickerViewGender.delegate = self
        registerView.GenderTf.inputView = pickerViewGender
        pickerViewGender.tag = 4
        
        
        pickerViewMoahel.delegate = self
        registerView.QualificationTg.inputView = pickerViewMoahel
        pickerViewMoahel.tag = 5
        
        GetGender()
//        GetJob()
        GetCity()
        showDatePicker()

    
        Moahel.append(MoahelModel(id: "1", name: "secondary".localized()))
        Moahel.append(MoahelModel(id: "2", name: "Collectors".localized()))
        Moahel.append(MoahelModel(id: "3", name: "M.A.".localized()))
    }
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        registerView.DateTf.inputAccessoryView = toolbar
        registerView.DateTf.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        registerView.DateTf.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }





    @IBAction func RegisterPressed(_ sender: Any) {
         Register()
    }
    @IBAction func TermsPressed(_ sender: Any) {
        let vc = Storyboard.Home.instantiate(TermsVc.self)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func BackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
//    @IBAction func CelenderButton(_ sender: Any) {
//        let vc = Storyboard.Authentication.instantiateEn(CelenderRegisterVc.self)
//        vc.delegate = self
//        present(vc,animated: true,completion: nil)
//    }

}

//extension RegisterClientVc: sendDataBackDelegatee{
//    func finishPassing(dates : String) {
//        self.Dates = dates
//        registerView.DateTf.text =  Dates
//
//    }

//}

