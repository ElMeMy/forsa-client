//
//  ProfileVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/20/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class EditProfileVc: UIViewController {

    var images = [AdvImage]()
    var City      = [CityModel]()
    var Region    = [RegionModel]()
    var Jobs      = [JobModel]()
    var Moahel    = [MoahelModel]()
    let pickerViewCity   = UIPickerView()
    let pickerViewRegion = UIPickerView()
    let pickerViewJop    = UIPickerView()
    let pickerViewMoahel = UIPickerView()

    
    var city_id   : Int?
    var Region_id : Int?
    var Job_id    : Int?
    var Moahed_id : String?
    var qualification_id : String?
    var emptyCity = false

    public var profileView: EditProfileView! {
        guard isViewLoaded else { return nil }
        return (view as! EditProfileView)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            profileView.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)

        }
        else
        {
            profileView.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        profileView.downImage.setImage(#imageLiteral(resourceName: "down"), for: .normal)
        profileView.down2Image.setImage(#imageLiteral(resourceName: "down"), for: .normal)
        profileView.PencileImage.setImage(#imageLiteral(resourceName: "pencil"), for: .normal)
        GetProfile()
        GetCity()
        GetJob()
        pickerViewCity.delegate = self
        profileView.CityTf.inputView = pickerViewCity
        pickerViewCity.tag = 1
        
        
        pickerViewRegion.delegate = self
        profileView.RegionTf.inputView = pickerViewRegion
        pickerViewRegion.tag = 2
        
//        pickerViewJop.delegate = self
//        profileView.JopTitle.inputView = pickerViewJop
//        pickerViewJop.tag = 3
        
        
        pickerViewMoahel.delegate = self
        profileView.QualificationTg.inputView = pickerViewMoahel
        pickerViewMoahel.tag = 4
        
        
        
        Moahel.append(MoahelModel(id: "1", name: "secondary".localized()))
        Moahel.append(MoahelModel(id: "2", name: "Collectors".localized()))
        Moahel.append(MoahelModel(id: "3", name: "M.A.".localized()))

        
        
    }
    
    @IBAction func BackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func EditProfilePressed(_ sender: Any) {
        UpdateUserData()
    }
    
    @IBAction func UpdatePassword(_ sender: Any) {
        let vc = Storyboard.Authentication.instantiate(EditNewPasswordVc.self)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func ImagePressed(_ sender: Any) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        //        imagePickerController.view.tag = 1
        let actionSheet = UIAlertController(title: "photocamera", message: "choose a sourse", preferredStyle: .actionSheet)

        actionSheet.addAction(UIAlertAction(title: "photoLibrary", style: .default, handler: { (action:UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            imagePickerController.allowsEditing = true
            
            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }

}
