//
//  CelenderRegisterVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/16/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
import FSCalendar

protocol sendDataBackDelegatee {
    func finishPassing(dates: String)
}

class CelenderRegisterVc: UIViewController, FSCalendarDelegate {
    
    @IBOutlet weak var Celender: FSCalendar!
    var selectedDate = ""
    var selected = false
    
    var delegate: sendDataBackDelegatee?
    var dates = [String]()
    
    var dateString : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        Celender.swipeToChooseGesture.isEnabled = true
        
    }
    
    @IBAction func CancelPressed(_ sender: Any) {
        if Language.currentLanguage().contains("en"){
            dismiss(animated: true, completion: nil)
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }else{
            dismiss(animated: true, completion: nil)
            
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        
    }

    
    @IBAction func ConfirmPressed(_ sender: Any) {
        
        if Language.currentLanguage().contains("en"){
            delegate?.finishPassing(dates: dateString!)
            self.dismiss(animated: true, completion: nil)
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
        }else{
            delegate?.finishPassing(dates: dateString ?? "")
            self.dismiss(animated: true, completion: nil)
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
    
    }
    
    
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        
        print(monthPosition)
        //        return true
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        formatter.locale = Locale(identifier: "en_US")
        let now = Date()
        //let currentDate = Date()
        //print(formatter.string(from: currentDate))
        
        
        print(formatter.string(from: date))
        
        
         dateString = formatter.string(from: date)
        
        
        if  date < now{
            selected = true
        }else {
            selected = true
            selectedDate = dateString!
        }
        
        
        return selected
    }
    
}
