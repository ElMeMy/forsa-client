//
//  MoahelModel.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/18/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

class MoahelModel {
    var id: String?
    var name: String?
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}
