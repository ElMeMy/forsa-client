//
//  GenderModel.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/23/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


class GenderModel {
    var id: String?
    var name: String?
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}
