//
//  MainExtention.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/18/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

extension MainVc: UITableViewDataSource, UITableViewDelegate{
    
    func setUp(){
        mainview.AdsTableView.delegate = self
        mainview.AdsTableView.dataSource = self
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return AdsItem.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "AdsCell") as? AdsCell
        {
            let Call = AdsItem[indexPath.row]
            cell.updateView(AdsItem: Call)
            return cell
        }
        else
        {
            return AdsCell()
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Storyboard.Home.instantiate(EmplyeeDetailsVc.self)
        let order_id = AdsItem[indexPath.row].order_id
        vc.order_id = order_id
        present(vc,animated: true, completion: nil)

    }
    
}
