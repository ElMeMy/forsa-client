//
//  NatExtention.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/18/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

extension NotfigationVc: UITableViewDataSource, UITableViewDelegate{
    
    func setUpNatfigation(){
        natView.NatTableView.delegate = self
        natView.NatTableView.dataSource = self
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return NatItem.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "NatCell") as? NatCell
        {
            let Call = NatItem[indexPath.row]
            cell.updateView(NatItem: Call)
            return cell
        }
        else
        {
            return AdsCell()
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            
                let type =  NatItem[indexPath.row].type
                let vc = Storyboard.Home.instantiate(EmplyeeDetailsVc.self)
                let id = NatItem[indexPath.row].fk_order
                vc.order_id = id
                vc.type = type
                present(vc,animated: true, completion: nil)
            
    
    
        }
    
}
