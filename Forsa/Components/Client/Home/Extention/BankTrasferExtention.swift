//
//  BankTrasferExtention.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/19/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

extension BankTransferVc: UIImagePickerControllerDelegate ,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var selectedImage: UIImage?
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            let pic = image.jpegData(compressionQuality: 0.75)
            
            self.images.append(AdvImage(img: image, imgData: pic!))
            
            
            
            picker.dismiss(animated: true, completion: nil)
            
        }
            
        else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            let imageData:Data = (image).pngData()! as Data
            
            self.images.append(AdvImage(img: image, imgData: imageData))
            picker.dismiss(animated: true, completion: nil)
            
        }
        
        
    }
}

