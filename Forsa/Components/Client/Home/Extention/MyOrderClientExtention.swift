//
//  MyOrderClientExtention.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/30/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

extension MyOrderClientVC: UITableViewDataSource, UITableViewDelegate{
    
    func setUpMyOrdersClient(){
        Orders.OrdersTableView.delegate = self
        Orders.OrdersTableView.dataSource = self
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return OrderItem.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrderClientCell") as? MyOrderClientCell
        {
            
            let Call   = OrderItem[indexPath.row]
            cell.updateView(OrderItem: Call)
            cell.EditAction = {
                
                let vc = Storyboard.Home.instantiate(EditConfirmVc.self)
                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                vc.order_id = self.OrderItem[indexPath.row].job_id
                self.present(vc,animated: true,completion: nil)
            }

            cell.DeleteAction = {
                
                let vc = Storyboard.Home.instantiate(DeleteConfirmVc.self)
                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                vc.order_id = self.OrderItem[indexPath.row].job_id
                self.present(vc,animated: true,completion: nil)
               
            }
            return cell
        }
        else
        {
            return OrdersCell()
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170.0
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
            let vc = Storyboard.Home.instantiate(EmployeeDetailsClientVc.self)
            vc.order_id = OrderItem[indexPath.row].job_id
            present(vc,animated: true, completion: nil)
        
    }
    
    
    
    
}



