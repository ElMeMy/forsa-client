//
//  MainClientExtent.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/1/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit


extension MainClientVc: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1
        {
            return City.count
        }
        else if pickerView.tag == 2
        {
            return Region.count
        }
        else if pickerView.tag == 3
        {
            return Jobs.count
        }
        return 1
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 1
        {
            return City[row].name
        }
        else if pickerView.tag == 2
        {
            return Region[row].name
        }
        else if pickerView.tag == 3
        {
            return Jobs[row].name
        }
        
        return ""
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1
        {
            self.MainClient.cityTextField.text = self.City[row].name
            city_id = City[row].id!
            getRegion()
            
        }
        else if pickerView.tag == 2
        {
            self.MainClient.RegionTextField.text = self.Region[row].name
            Region_id = Region[row].id!
        }
        
        else if pickerView.tag == 3
        {
            self.MainClient.JopTitle.text = self.Jobs[row].name
            Job_id = Jobs[row].id!
            
        }
        
        
    }
    
}
