//
//  EmplyeeDetailsVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/18/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class EmplyeeDetailsVc: UIViewController {
    
    var order_id: Int?
    var fk_job: Int?
    var stuteValiable: Int?
    var phone: String?
    var type:Int?
    
    
    public var EmployeeDetails: EmployeeDetailsView! {
        guard isViewLoaded else { return nil }
        return (view as! EmployeeDetailsView)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            EmployeeDetails.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)
            
        }
        else
        {
            EmployeeDetails.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        EmplyeeDetails()
        if type != 9 
        {
            EmployeeDetails.PhoneLabel.isHidden = true
            EmployeeDetails.PhoneCountLabel.isHidden = true
        }
        else
        {
            EmployeeDetails.PhoneLabel.isHidden = false
            EmployeeDetails.PhoneCountLabel.isHidden = false
            
            
        }
        
        
    }
    
    
    @IBAction func BackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func RefuesdPressed(_ sender: Any) {
        let vc = Storyboard.Home.instantiate(TheResonRefusedVc.self)
        vc.order_id = order_id
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func AcceptPressed(_ sender: Any) {
        if stuteValiable == 0
        {
            AcceptOrder()
            let vc = Storyboard.Home.instantiate(AcceptOrderVc.self)
            vc.order_id = order_id
            vc.fk_job = fk_job
            vc.phone = phone
            self.present(vc, animated: true, completion: nil)
        }
        else if stuteValiable == 1
        {
            let vc = Storyboard.Home.instantiate(BankTransferVc.self)
            vc.order_id = order_id
            vc.fk_job = fk_job
            vc.phone = phone
            self.present(vc, animated: true, completion: nil)
        }
        else
        {
            AcceptOrder()
            let vc = Storyboard.Home.instantiate(AcceptOrderVc.self)
            vc.order_id = order_id
            vc.fk_job = fk_job
            vc.phone = phone
            self.present(vc, animated: true, completion: nil)
        }
        
        
        
    }
    
}
