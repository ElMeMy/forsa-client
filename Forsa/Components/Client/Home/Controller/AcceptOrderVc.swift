//
//  AcceptOrderVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/19/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class AcceptOrderVc: UIViewController {
    
    
    var order_id : Int?
    var fk_job : Int?
    var phone: String?
    
    public var AcceptOrder: AcceptOrderView! {
        guard isViewLoaded else { return nil }
        return (view as! AcceptOrderView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            AcceptOrder.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)
        }
        else
        {
            AcceptOrder.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        
        AcceptOrder.LogoImage.image = #imageLiteral(resourceName: "vvector")
        GetSetting()
    }
    
    
    
    
    
    @IBAction func BackPreesed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func PayPressed(_ sender: Any) {
        let vc = Storyboard.Home.instantiate(BankTransferVc.self)
        vc.order_id = order_id
        vc.phone = phone
        vc.fk_job = fk_job
        self.present(vc, animated: true, completion: nil)
    }
}
