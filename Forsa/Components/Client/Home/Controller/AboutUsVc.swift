//
//  AboutUsVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/23/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class AboutUsVc: UIViewController {
    
    public var About: AboutView! {
        guard isViewLoaded else { return nil }
        return (view as! AboutView)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            About.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)
            
        }
        else
        {
            About.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        
        GetAbout()
    }
    
    @IBAction func BackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
