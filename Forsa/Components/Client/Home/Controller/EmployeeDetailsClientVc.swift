//
//  EmployeeDetailsClientVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/30/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class EmployeeDetailsClientVc: UIViewController {
    
    
    var order_id: Int?
    
    
    public var Employee: EmployeeProviderView! {
        guard isViewLoaded else { return nil }
        return (view as! EmployeeProviderView)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            Employee.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)
            
        }
        else
        {
            Employee.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        EmplyeeDetailsClientService()
    }
    
    @IBAction func BackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func EditPressed(_ sender: Any) {
        let vc = Storyboard.Home.instantiate(EditAdsVc.self)
        vc.order_id = order_id
        present(vc,animated: true,completion: nil)
    }
    
    func EmplyeeDetailsClientService()
    {
        
        let user_id = UserDefaults.standard.value(forKey: "Id") as? Int ?? 0
        
        let parameters = [
            "id_job"      : order_id!,
            "provider_id" : user_id,
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            ] as [String : Any]
        
        startAnimating()
        API.POST(url: URLs.AdvertsmentDetails, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    break
                case 1:
                    if let data = dict["result"]
                    {
                        print("result",data)
                        
                        
                        let name        = data["name"] as? String ?? ""
                        self.Employee.NameLabel.text = name
                        
                        let img        = data["img"] as? String ?? ""
                        let url = URL(string: img ?? "")
                        self.Employee.ProfileImage.kf.setImage(with: url)
                        
                        let HourLabel   = data["hours_no"] as? String ?? ""
                        self.Employee.HourLabel.text = HourLabel
                        
                        let AboutLabel   = data["about"] as? String ?? ""
                        self.Employee.AboutLabel.text = AboutLabel
                        
                        let AgeLabel    = data["age"] as? Int ?? 0
                        self.Employee.AgeLabel.text = "\(AgeLabel)"
                        
                        let CityLabel    = data["city"] as? String ?? ""
                        self.Employee.CityLabel.text = CityLabel
                        
                        let RegionLabel  = data["Region"] as? String ?? ""
                        self.Employee.RegionLabel.text = RegionLabel
                        
                        let QualificationLabel  = data["qualification"] as? String ?? ""
                        self.Employee.QualificationLabel.text = QualificationLabel
                        
                        
                        let ExperiecneLabel  = data["previous_experience"] as? String ?? ""
                        self.Employee.ExperiecneLabel.text = ExperiecneLabel
                        
                        
                        let SkillLabel  = data["educational_courses"] as? String ?? ""
                        self.Employee.SkillLabel.text = SkillLabel
                        
                        
                        let NumHourLabel  = data["hours_no"] as? String ?? ""
                        self.Employee.NumHourLabel.text = NumHourLabel
                        
                        
                        
                        
                    }else{
                        self.stopAnimating()
                        
                        //weak internet
                        let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                    
                    
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
}
