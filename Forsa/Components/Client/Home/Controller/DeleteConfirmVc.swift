//
//  DeleteConfirmVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 8/1/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
class DeleteConfirmVc: UIViewController {
    
    
    var order_id:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func DeletePressed(_ sender: Any) {
        
        let parameters = [
            "fk_job"    :order_id!,
            "stutes"    : "7",
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            ] as [String : Any]
        
        self.startAnimating()
        API.POST(url: URLs.ChangeStutes, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    break
                case 1:
                    let msg = dict["msg"]
                    
                    let alert = UIAlertController(title: "Done".localized(), message: msg as? String, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                        let type_user = UserDefaults.standard.value(forKey: "type_user") as? String
                        let lang = Language.currentLanguage()
                        
                        if  type_user == "client"{
                            
                            if lang == "en"
                            {
                                let vc2 = Storyboard.Home.instantiate(MainVc.self)
                                let right = Storyboard.Home.instantiate(SideMenuVC.self)
                                let sideMenu = SlideMenuController(mainViewController: vc2, leftMenuViewController: right)
                                self.view.window?.rootViewController = sideMenu
                            }
                            else
                            {
                                let vc2 = Storyboard.Home.instantiate(MainVc.self)
                                let right = Storyboard.Home.instantiate(SideMenuVC.self)
                                let sideMenu = SlideMenuController(mainViewController: vc2, rightMenuViewController: right)
                                self.view.window?.rootViewController = sideMenu
                            }
                            
                        }
                        else if type_user == "provider"
                        {
                            
                            if lang == "en"
                            {
                                let vc2 = Storyboard.Home.instantiate(MainProviderVc.self)
                                let right = Storyboard.Home.instantiate(SideMenuVC.self)
                                let sideMenu = SlideMenuController(mainViewController: vc2, leftMenuViewController: right)
                                self.view.window?.rootViewController = sideMenu
                            }
                            else
                            {
                                let vc2 = Storyboard.Home.instantiate(MainProviderVc.self)
                                let right = Storyboard.Home.instantiate(SideMenuVC.self)
                                let sideMenu = SlideMenuController(mainViewController: vc2, rightMenuViewController: right)
                                self.view.window?.rootViewController = sideMenu
                            }
                        }
                        else
                        {
                            let vc = Storyboard.Authentication.instantiate(LangVc.self)
                            self.view.window?.rootViewController = vc
                        }
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
    @IBAction func BackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
