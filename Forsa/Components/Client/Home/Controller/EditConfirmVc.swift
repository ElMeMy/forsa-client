//
//  EditConfirmVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 8/1/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class EditConfirmVc: UIViewController {
    
    var order_id:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func EditPressed(_ sender: Any) {
        
        let vc = Storyboard.Home.instantiate(EditAdsVc.self)
        vc.order_id = order_id
        self.present(vc,animated: true,completion: nil)
    }
    
    @IBAction func BackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
