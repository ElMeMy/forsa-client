//
//  EditAdsVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/30/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SlideMenuControllerSwift
class EditAdsVc: UIViewController {
    
    
    var order_id: Int?
    
    var City      = [CityModel]()
    var Region    = [RegionModel]()
    var Jobs      = [JobModel]()
    
    
    let pickerViewCity   = UIPickerView()
    let pickerViewRegion = UIPickerView()
    let pickerViewJop    = UIPickerView()
    
    
    var city_id   : Int?
    var Region_id : Int?
    var Job_id    : Int?
    var fk_job_titleEdit    : Int?
    var emptyCity = false
    var Dates = [String]()
    
    
    var city_idEdit   : Int?
    var Region_idEdit : Int?
    
    let FromdatePicker = UIDatePicker()
    let TodatePicker   = UIDatePicker()
    
    
    public var MainClient: MainClientView! {
        guard isViewLoaded else { return nil }
        return (view as! MainClientView)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MainClient.BgImage.image  = #imageLiteral(resourceName: "bg")
        MainClient.backGroundImage.image  = #imageLiteral(resourceName: "vvvector")
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            MainClient.SideMenuButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)
        }
        else
        {
            MainClient.SideMenuButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
            
        }
        MainClient.Down2Image.image  = #imageLiteral(resourceName: "down")
        MainClient.Down3Image.image  = #imageLiteral(resourceName: "down")
        
        
        pickerViewCity.delegate = self
        MainClient.cityTextField.inputView = pickerViewCity
        pickerViewCity.tag = 1
        
        
        pickerViewRegion.delegate = self
        MainClient.RegionTextField.inputView = pickerViewRegion
        pickerViewRegion.tag = 2
        
        
        pickerViewJop.delegate = self
        MainClient.JopTitle.inputView = pickerViewJop
        pickerViewJop.tag = 3
        
        
        MainClient.Down4Image.image = #imageLiteral(resourceName: "down")
        
        GetCity()
        
        GetJob()
        
        
        showFromDatePicker()
        showToatePicker()
        EmplyeeDetailsClientServices()
        
    }
    
    
    func GetJob() {
        let body = ["lang" : Language.currentLanguage().contains("en") ? "en" : "ar",]
        
        startAnimating()
        API.POST(url: URLs.GetJob_title, parameters: body, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                let dict = value
                
                if let v = dict["key"] as? Int{
                    
                    switch v {
                    case  0:
                        
                        let msg = dict["msg"]
                        
                        let alert = UIAlertController(title: "error".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    case 1:
                        
                        if let jobs = dict["Job_title"] as? [[String: Any]]{
                            
                            for a in jobs{
                                let id = a["Id"] as! Int
                                let name = a["name"] as! String
                                self.Jobs.append(JobModel(id: id, name: name))
                                
                            }
                            
                            
                        }else{
                            self.stopAnimating()
                            
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                        
                        
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    func EmplyeeDetailsClientServices()
    {
        
        
        let parameters = [
            "id_job"      : order_id!,
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            ] as [String : Any]
        
        startAnimating()
        API.POST(url: URLs.AdvertDescription, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    break
                case 1:
                    if let data = dict["result"]
                    {
                        print("result",data)
                        
                        
                        
                        let city  = data["city"] as? String ?? ""
                        self.MainClient.cityTextField.text = city
                        
                        let Region  = data["Region"] as? String ?? ""
                        self.MainClient.RegionTextField.text = Region
                        
                        let hours_no  = data["hours_no"] as? String ?? ""
                        self.MainClient.NumberHour.text = hours_no
                        
                        
                        let expected_salary  = data["expected_salary"] as? String ?? ""
                        self.MainClient.SalaryTf.text = expected_salary
                        
                        
                        let FromDate    = data["Start"] as? String ?? ""
                        self.MainClient.FromDateTF.text = FromDate
                        
                        
                        let ToDate     = data["end"] as? String ?? ""
                        self.MainClient.ToDateTf.text = ToDate
                        
                        
                        
                        let Joptitle     = data["Job_title"] as? String ?? ""
                        self.MainClient.JopTitle.text = Joptitle
                        
                        
                        
                        self.fk_job_titleEdit     = data["fk_job_title"] as? Int ?? 0
                        
                        
                        let attendees_time     = data["attendees_time"] as? String ?? ""
                        self.MainClient.TimeButtonOuttlelt.setTitle(attendees_time, for: .normal)
                        
                        
                        let take_off     = data["take_off"] as? String ?? ""
                        self.MainClient.Time2ButtonOuttlelt.setTitle(take_off, for: .normal)
                        
                        self.city_idEdit    = data["city_id"] as? Int ?? 0
                        self.Region_idEdit  = data["Region_id"] as? Int ?? 0
                        
                    }else{
                        self.stopAnimating()
                        
                        //weak internet
                        let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                    
                    
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
    func showFromDatePicker(){
        //Formate Date
        FromdatePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        MainClient.FromDateTF.inputAccessoryView = toolbar
        MainClient.FromDateTF.inputView = FromdatePicker
        
    }
    
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        MainClient.FromDateTF.text = formatter.string(from: FromdatePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    
    
    func showToatePicker(){
        //Formate Date
        TodatePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedateToPicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        MainClient.ToDateTf.inputAccessoryView = toolbar
        MainClient.ToDateTf.inputView = TodatePicker
        
    }
    
    
    
    @objc func donedateToPicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        MainClient.ToDateTf.text = formatter.string(from: TodatePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDateToPicker(){
        self.view.endEditing(true)
    }
    
    
    
    
    @IBAction func PressedButton(_ sender: Any) {
        EditAdsVc()
    }
    
    @objc func actionPickerCancel(){
        
    }
    
    @objc func actionPickerDone(){
        
    }
    
    @IBAction func TimeButton(_ sender: UIButton) {
        
        let datePicker = ActionSheetDatePicker(title: nil, datePickerMode: .time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            
            print(String(describing: value!))
            
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            formatter.locale = Locale(identifier: "en")
            formatter.timeZone = TimeZone(identifier: TimeZone.current.identifier)
            print(formatter.timeZone)
            let localDateString = formatter.string(from: value! as! Date)
            
            self.MainClient.TimeButtonOuttlelt.setTitle(String(describing: localDateString), for: .normal)
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
        let bar1 = UIBarButtonItem.init(title: "Done".localized(), style: .plain, target: self, action: #selector(actionPickerDone))
        let bar2 = UIBarButtonItem.init(title: "cancel".localized(), style: .plain, target: self, action: #selector(actionPickerCancel))
        datePicker?.setDoneButton(bar1)
        datePicker?.setCancelButton(bar2)
        
        
        datePicker?.show()
    }
    
    
    @IBAction func Time2Button(_ sender: UIButton) {
        
        let datePicker = ActionSheetDatePicker(title: nil, datePickerMode: .time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            
            print(String(describing: value!))
            
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            formatter.locale = Locale(identifier: "en")
            formatter.timeZone = TimeZone(identifier: TimeZone.current.identifier)
            print(formatter.timeZone)
            let localDateString = formatter.string(from: value! as! Date)
            
            self.MainClient.Time2ButtonOuttlelt.setTitle(String(describing: localDateString), for: .normal)
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
        let bar1 = UIBarButtonItem.init(title: "Done".localized(), style: .plain, target: self, action: #selector(actionPickerDone))
        let bar2 = UIBarButtonItem.init(title: "cancel".localized(), style: .plain, target: self, action: #selector(actionPickerCancel))
        datePicker?.setDoneButton(bar1)
        datePicker?.setCancelButton(bar2)
        
        
        datePicker?.show()
    }
    
    @IBAction func BackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    func GetCity() {
        let body = ["lang" : Language.currentLanguage().contains("en") ? "en" : "ar",]
        
        startAnimating()
        API.POST(url: URLs.GetCities, parameters: body, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                let dict = value
                
                if let v = dict["key"] as? Int{
                    
                    switch v {
                    case  0:
                        
                        let msg = dict["msg"]
                        
                        let alert = UIAlertController(title: "error".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    case 1:
                        
                        if let City = dict["City"] as? [[String: Any]]{
                            
                            for a in City{
                                let id = a["Id"] as! Int
                                let name = a["name"] as! String
                                self.City.append(CityModel(id: id, name: name))
                                
                            }
                            
                            
                        }else{
                            self.stopAnimating()
                            
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                        
                        
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    
    
    func getRegion() {
        let body = ["lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
                    "city_id" : city_id!
            ] as [String : Any]
        
        startAnimating()
        API.POST(url: URLs.GetRegion, parameters: body, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                let dict = value
                
                if let v = dict["key"] as? Int{
                    
                    switch v {
                    case  0:
                        
                        let msg = dict["msg"]
                        
                        let alert = UIAlertController(title: "error".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    case 1:
                        
                        if let Region = dict["Region"] as? [[String: Any]]{
                            
                            
                            if self.emptyCity{
                                self.MainClient.cityTextField.isUserInteractionEnabled = true
                                self.MainClient.cityTextField.inputView = self.pickerViewCity
                            }
                            
                            self.Region = []
                            for a in Region{
                                let id = a["Id"] as! Int
                                let name = a["name"] as! String
                                self.Region.append(RegionModel(id: id, name: name))
                                
                            }
                            
                            
                        }else{
                            self.stopAnimating()
                            
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                        
                        
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    func EditAdsVc()
    {
        
        guard let city = MainClient.cityTextField.text , !( MainClient.cityTextField.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The City Is Requried".localized())}
        
        guard let Region = MainClient.RegionTextField.text , !( MainClient.RegionTextField.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The Region Is Requried".localized())}
        
        
        
        guard let hour = MainClient.NumberHour.text , !( MainClient.NumberHour.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The Number Hour Is Requried".localized())}
        
        
        guard let From = MainClient.FromDateTF.text , !( MainClient.FromDateTF.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The From Date Is Requried".localized())}
        
        
        guard let To = MainClient.ToDateTf.text , !( MainClient.ToDateTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The To Date Is Requried".localized())}
        
        
        guard let Salary = MainClient.SalaryTf.text , !( MainClient.SalaryTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The Salary  Is Requried".localized())}
        
        guard let Time1 = MainClient.TimeButtonOuttlelt.currentTitle , !( MainClient.TimeButtonOuttlelt.currentTitle?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The Presence  Is Requried".localized())}
        
        guard let Time2 = MainClient.Time2ButtonOuttlelt.currentTitle , !( MainClient.Time2ButtonOuttlelt.currentTitle?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The departure  Is Requried".localized())}
        
        
        
        
        
        let user_id = UserDefaults.standard.value(forKey: "Id") as? Int ?? 0
        
        let parameters = [
            "city_id"    : city_id ?? city_idEdit ?? 0,
            "region_id"  : Region_id ?? Region_idEdit ?? 0,
            "start"      : From,
            "end"        : To,
            "hour_no"    : hour,
            "attendees_time"     : Time1,
            "take_off"           : Time2,
            "expected_salary"    : Salary,
            "client_id"    : user_id,
            "fk_job_title"    : Job_id ?? fk_job_titleEdit ?? 0,
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            ] as [String : Any]
        startAnimating()
        API.POST(url: URLs.EditAdvertsment, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    break
                case 1:
                    let msg = dict["msg"]
                    
                    let alert = UIAlertController(title: "Done".localized(), message: msg as? String, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                        let type_user = UserDefaults.standard.value(forKey: "type_user") as? String
                        let lang = Language.currentLanguage()
                        
                        if  type_user == "client"{
                            
                            if lang == "en"
                            {
                                let vc2 = Storyboard.Home.instantiate(MainVc.self)
                                let right = Storyboard.Home.instantiate(SideMenuVC.self)
                                let sideMenu = SlideMenuController(mainViewController: vc2, leftMenuViewController: right)
                                self.view.window?.rootViewController = sideMenu
                            }
                            else
                            {
                                let vc2 = Storyboard.Home.instantiate(MainVc.self)
                                let right = Storyboard.Home.instantiate(SideMenuVC.self)
                                let sideMenu = SlideMenuController(mainViewController: vc2, rightMenuViewController: right)
                                self.view.window?.rootViewController = sideMenu
                            }
                            
                        }
                        else if type_user == "provider"
                        {
                            
                            if lang == "en"
                            {
                                let vc2 = Storyboard.Home.instantiate(MainProviderVc.self)
                                let right = Storyboard.Home.instantiate(SideMenuVC.self)
                                let sideMenu = SlideMenuController(mainViewController: vc2, leftMenuViewController: right)
                                self.view.window?.rootViewController = sideMenu
                            }
                            else
                            {
                                let vc2 = Storyboard.Home.instantiate(MainProviderVc.self)
                                let right = Storyboard.Home.instantiate(SideMenuVC.self)
                                let sideMenu = SlideMenuController(mainViewController: vc2, rightMenuViewController: right)
                                self.view.window?.rootViewController = sideMenu
                            }
                        }
                        else
                        {
                            let vc = Storyboard.Authentication.instantiate(LangVc.self)
                            self.view.window?.rootViewController = vc
                        }
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
}

extension EditAdsVc: sendDataBackDelegate{
    func finishPassing(dates : [String]) {
        self.Dates = dates
    }
    
}

extension EditAdsVc: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1
        {
            return City.count
        }
        else if pickerView.tag == 2
        {
            return Region.count
        }
        else if pickerView.tag == 3
        {
            return Jobs.count
        }
        return 1
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 1
        {
            return City[row].name
        }
        else if pickerView.tag == 2
        {
            return Region[row].name
        }
        else if pickerView.tag == 3
        {
            return Jobs[row].name
        }
        
        
        return ""
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1
        {
            self.MainClient.cityTextField.text = self.City[row].name
            city_id = City[row].id!
            getRegion()
            
        }
        else if pickerView.tag == 2
        {
            self.MainClient.RegionTextField.text = self.Region[row].name
            Region_id = Region[row].id!
        }
            
        else if pickerView.tag == 3
        {
            self.MainClient.JopTitle.text = self.Jobs[row].name
            Job_id = Jobs[row].id!
            
        }
        
        
        
    }
    
}
