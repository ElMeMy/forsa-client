//
//  BankTransferVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/19/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class BankTransferVc: UIViewController {
    
    
    var images = [AdvImage]()
    var order_id : Int?
    var fk_job : Int?
    var phone : String?
    var Items = [BankModel]()

    
    public var bankView: BankView! {
        guard isViewLoaded else { return nil }
        return (view as! BankView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bankView.bankTableView.delegate = self
        bankView.bankTableView.dataSource = self
        
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            bankView.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)
            
        }
        else
        {
            bankView.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        
        bankView.CameraImage.setImage(#imageLiteral(resourceName: "camera"), for: .normal)
        BankTransfer()
        
        
    }
    
    @IBAction func ImagePressed(_ sender: Any) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        //        imagePickerController.view.tag = 1
        let actionSheet = UIAlertController(title: "photocamera", message: "choose a sourse", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "photoLibrary", style: .default, handler: { (action:UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            imagePickerController.allowsEditing = true
            
            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        actionSheet.addAction(UIAlertAction(title: "camera", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    
    @IBAction func PayPressed(_ sender: Any) {
        SendBankTransfet()
    }
    
    @IBAction func BackPrssed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}





extension BankTransferVc: UITableViewDataSource, UITableViewDelegate{
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Items.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "BankCell") as? BankCell
        {
            let Call = Items[indexPath.row]
            cell.updateView(item: Call)
            return cell
        }
        else
        {
            return BankCell()
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
 
    
}
