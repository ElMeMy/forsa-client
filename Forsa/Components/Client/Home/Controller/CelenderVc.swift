//
//  CelenderVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/1/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
import FSCalendar

protocol sendDataBackDelegate {
    func finishPassing(dates: [String])
}

class CelenderVc: UIViewController, FSCalendarDelegate {
    
    @IBOutlet weak var Celender: FSCalendar!
    var selectedDate = ""
    var selected = false
    
    var delegate: sendDataBackDelegate?
    var dates = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Celender.swipeToChooseGesture.isEnabled = true
        
        
        
    }
    
    @IBAction func CancelPressed(_ sender: Any) {
        if Language.currentLanguage().contains("en"){
            dismiss(animated: true, completion: nil)
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }else{
            dismiss(animated: true, completion: nil)
            
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        
    }
    
    @IBAction func ConfirmPressed(_ sender: Any) {
        
        if Language.currentLanguage().contains("en"){
            delegate?.finishPassing(dates: dates)
            self.dismiss(animated: true, completion: nil)
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
        }else{
            delegate?.finishPassing(dates: dates)
            self.dismiss(animated: true, completion: nil)
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        
    }
    
    
    
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        
        print(monthPosition)
        //        return true
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "en_US")
        let now = Date()
        //let currentDate = Date()
        //print(formatter.string(from: currentDate))
        
        
        print(formatter.string(from: date))
        
        
        let dateString = formatter.string(from: now)
        
        dates.append(dateString)
        
        if  date < now{
            selected = false
        }else {
            selected = true
            selectedDate = dateString
        }
        
        
        return selected
    }
    
}
