//
//  ShowNameAndPhoneVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 10/15/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
class ShowNameAndPhoneVc: UIViewController {
    @IBOutlet weak var NameOutlet: UILabel!
    @IBOutlet weak var PhoneOutlet: UILabel!
    var userId : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        GetDataOfProvider()
        
    }
    func GetDataOfProvider(){
        
        
        
        let parameters = [
            "provider_id" : userId!,
            
            ] as [String : Any]
        
        self.startAnimating()
        
        API.POST(url: URLs.GetDataOfProvider, parameters: parameters, headers: nil, completion: {
            (success, value) in
            
            self.stopAnimating()
            if success {
                
                if let returnKey = value["key"] as? Int {
                    if returnKey == 1 {
                        let provider = value["provider"] as? [String: Any]
                        
                        
                        
                        let name = provider!["name"] as? String ?? ""
                        self.NameOutlet.text = name
                        
                        
                        
                        let phone = provider!["phone"] as? String ?? ""
                        self.PhoneOutlet.text = phone
                        
                        
                        
                        
                        
                    }
                }
            } else {
                
            }
        })
        
    }
    
    
    @IBAction func EditPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
