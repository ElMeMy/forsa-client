//
//  TheResonRefusedVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/19/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class TheResonRefusedVc: UIViewController {
    
    
    var order_id : Int?
    public var Refuesd: RefuesdView! {
        guard isViewLoaded else { return nil }
        return (view as! RefuesdView)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            Refuesd.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)
            
        }
        else
        {
            Refuesd.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
    }
    
    @IBAction func BackPreesed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func SendPressed(_ sender: Any) {
        ResonRefues()
    }
}
