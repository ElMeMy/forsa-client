//
//  LogOutVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/31/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class LogOutVc: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func LogOutPressed(_ sender: Any) {
        let vc = Storyboard.Authentication.instantiate(LoginProviderClientVc.self)
        UserDefaults.standard.removeObject(forKey: "Id")
        UserDefaults.standard.removeObject(forKey: "token")
        UserDefaults.standard.removeObject(forKey: "name")
        UserDefaults.standard.removeObject(forKey: "img")
        UserDefaults.standard.removeObject(forKey: "type_user")
        present(vc,animated: true,completion: nil)
    }
    @IBAction func CancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
