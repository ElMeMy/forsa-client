//
//  MainClientVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/1/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
class MainClientVc: UIViewController {
    
    @IBOutlet weak var CountNat: UILabel!
    var City      = [CityModel]()
    var Region    = [RegionModel]()
    var Jobs      = [JobModel]()
    
    
    let pickerViewCity   = UIPickerView()
    let pickerViewRegion = UIPickerView()
    let pickerViewJop    = UIPickerView()
    
    
    var city_id   : Int?
    var Region_id : Int?
    var Job_id    : Int?
    var emptyCity = false
    var Dates = [String]()
    
    let FromdatePicker = UIDatePicker()
    let TodatePicker   = UIDatePicker()
    
    
    public var MainClient: MainClientView! {
        guard isViewLoaded else { return nil }
        return (view as! MainClientView)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MainClient.BgImage.image  = #imageLiteral(resourceName: "bg")
        MainClient.backGroundImage.image  = #imageLiteral(resourceName: "vvvector")
        MainClient.NatButton.setImage(#imageLiteral(resourceName: "notifications"), for: .normal)
        
        
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            MainClient.SideMenuButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
            
        }
        else
        {
            MainClient.SideMenuButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)
        }
        
        MainClient.Down2Image.image  = #imageLiteral(resourceName: "down")
        MainClient.Down3Image.image  = #imageLiteral(resourceName: "down")
        
        
        pickerViewCity.delegate = self
        MainClient.cityTextField.inputView = pickerViewCity
        pickerViewCity.tag = 1
        
        
        pickerViewRegion.delegate = self
        MainClient.RegionTextField.inputView = pickerViewRegion
        pickerViewRegion.tag = 2
        
        
        
        pickerViewJop.delegate = self
        MainClient.JopTitle.inputView = pickerViewJop
        pickerViewJop.tag = 3
        
        GetCity()
        GetJob()
        MainClient.Down4Image.image = #imageLiteral(resourceName: "down")
        
        
        showFromDatePicker()
        showToatePicker()
        GetCountNat()
        
    }
    
    func GetCountNat() {
        let User_id = UserDefaults.standard.value(forKey: "Id") as? Int
        
        let body = ["client_id" : User_id]
        
        startAnimating()
        API.POST(url: URLs.GetNotifyByClient, parameters: body as [String : Any], headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                let dict = value
                
                if let v = dict["key"] as? Int{
                    
                    switch v {
                    case  0:
                        
                        break
                        
                    case 1:
                        
                        let CountNotify = dict["CountNotify"] as! Int
                        
                        
                        self.CountNat.text = "\(CountNotify)"
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    func GetJob() {
        let body = ["lang" : Language.currentLanguage().contains("en") ? "en" : "ar",]
        
        startAnimating()
        API.POST(url: URLs.GetJob_title, parameters: body, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                let dict = value
                
                if let v = dict["key"] as? Int{
                    
                    switch v {
                    case  0:
                        
                        let msg = dict["msg"]
                        
                        let alert = UIAlertController(title: "error".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    case 1:
                        
                        if let jobs = dict["Job_title"] as? [[String: Any]]{
                            
                            for a in jobs{
                                let id = a["Id"] as! Int
                                let name = a["name"] as! String
                                self.Jobs.append(JobModel(id: id, name: name))
                                
                            }
                            
                            
                        }else{
                            self.stopAnimating()
                            
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                        
                        
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func showFromDatePicker(){
        //Formate Date
        FromdatePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        MainClient.FromDateTF.inputAccessoryView = toolbar
        MainClient.FromDateTF.inputView = FromdatePicker
        
    }
    
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        MainClient.FromDateTF.text = formatter.string(from: FromdatePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    
    
    func showToatePicker(){
        //Formate Date
        TodatePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedateToPicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        MainClient.ToDateTf.inputAccessoryView = toolbar
        MainClient.ToDateTf.inputView = TodatePicker
        
    }
    
    
    
    @objc func donedateToPicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        MainClient.ToDateTf.text = formatter.string(from: TodatePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDateToPicker(){
        self.view.endEditing(true)
    }
    
    
    @IBAction func BackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func CelenderButton(_ sender: Any) {
        let vc = Storyboard.Home.instantiateEn(CelenderVc.self)
        vc.delegate = self
        present(vc,animated: true,completion: nil)
    }
    
    @IBAction func PressedButton(_ sender: Any) {
        makeAds()
    }
    
    @objc func actionPickerCancel(){
        
    }
    
    @objc func actionPickerDone(){
        
    }
    
    @IBAction func TimeButton(_ sender: UIButton) {
        
        let datePicker = ActionSheetDatePicker(title: nil, datePickerMode: .time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            
            print(String(describing: value!))
            
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            formatter.locale = Locale(identifier: "en")
            formatter.timeZone = TimeZone(identifier: TimeZone.current.identifier)
            print(formatter.timeZone)
            let localDateString = formatter.string(from: value! as! Date)
            
            self.MainClient.TimeButtonOuttlelt.setTitle(String(describing: localDateString), for: .normal)
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
        let bar1 = UIBarButtonItem.init(title: "Done".localized(), style: .plain, target: self, action: #selector(actionPickerDone))
        let bar2 = UIBarButtonItem.init(title: "cancel".localized(), style: .plain, target: self, action: #selector(actionPickerCancel))
        datePicker?.setDoneButton(bar1)
        datePicker?.setCancelButton(bar2)
        
        
        datePicker?.show()
    }
    
    
    @IBAction func Time2Button(_ sender: UIButton) {
        
        let datePicker = ActionSheetDatePicker(title: nil, datePickerMode: .time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            
            print(String(describing: value!))
            
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            formatter.locale = Locale(identifier: "en")
            formatter.timeZone = TimeZone(identifier: TimeZone.current.identifier)
            print(formatter.timeZone)
            let localDateString = formatter.string(from: value! as! Date)
            
            self.MainClient.Time2ButtonOuttlelt.setTitle(String(describing: localDateString), for: .normal)
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
        let bar1 = UIBarButtonItem.init(title: "Done".localized(), style: .plain, target: self, action: #selector(actionPickerDone))
        let bar2 = UIBarButtonItem.init(title: "cancel".localized(), style: .plain, target: self, action: #selector(actionPickerCancel))
        datePicker?.setDoneButton(bar1)
        datePicker?.setCancelButton(bar2)
        
        
        datePicker?.show()
    }
    
    @IBAction func NatPressed(_ sender: Any) {
        let vc = Storyboard.Home.instantiate(NotfigationVc.self)
        present(vc,animated: true, completion: nil)
    }
    @IBAction func DoneAction(_ sender: Any) {
        makeAds()
    }
    
}

extension MainClientVc: sendDataBackDelegate{
    func finishPassing(dates : [String]) {
        self.Dates = dates
    }
    
}

