//
//  ShowNameClientVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 10/15/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class ShowNameClientVc: UIViewController {
    @IBOutlet weak var NameOutlet: UILabel!
    @IBOutlet weak var PhoneOutlet: UILabel!
    var userId : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        Profile()
        
    }
    func Profile()
    {
        
        let parameters = [
            "client_id"    : userId!,
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            ] as [String : Any]
        
        startAnimating()
        API.POST(url: URLs.GetDataOfClient, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    break
                case 1:
                    if let data = dict["client"]{
                        
                        
                        let name = data["name"] as? String ?? ""
                        self.NameOutlet.text = name
                        
                        
                        
                        let phone = data["phone"] as? String ?? ""
                        self.PhoneOutlet.text = phone
                        
                        
                        
                        
                    }else{
                        self.stopAnimating()
                        
                        //weak internet
                        let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                    
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
    
    @IBAction func EditPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
