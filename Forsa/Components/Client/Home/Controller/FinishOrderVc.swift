//
//  FinishOrderVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 8/4/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
class FinishOrderVc: UIViewController {
    
    var phone : String?
    
    @IBOutlet weak var PhoneOutlet: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        PhoneOutlet.text = phone
    }
    
    
    @IBAction func EditPressed(_ sender: Any) {
        let type_user = UserDefaults.standard.value(forKey: "type_user") as? String
        let lang = Language.currentLanguage()
        
        if  type_user == "client"{
            
            if lang == "en"
            {
                let vc2 = Storyboard.Home.instantiate(MainVc.self)
                let right = Storyboard.Home.instantiate(SideMenuVC.self)
                let sideMenu = SlideMenuController(mainViewController: vc2, leftMenuViewController: right)
                self.view.window?.rootViewController = sideMenu
            }
            else
            {
                let vc2 = Storyboard.Home.instantiate(MainVc.self)
                let right = Storyboard.Home.instantiate(SideMenuVC.self)
                let sideMenu = SlideMenuController(mainViewController: vc2, rightMenuViewController: right)
                self.view.window?.rootViewController = sideMenu
            }
            
        }
    }
    
}
