//
//  BankCell.swift
//  Forsa
//
//  Created by ahmed elmemy on 12/24/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class BankCell: UITableViewCell {
    @IBOutlet weak var nameBank: UILabel!
    @IBOutlet weak var numberBank: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func updateView(item: BankModel) {
        
        nameBank.text               = item.name
        numberBank.text              = item.accountNumber
    }
}
