//
//  EmployeeDetailsView.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/18/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

class EmployeeDetailsView: UIView {
    
    @IBOutlet weak var BackButton: UIButton!
    @IBOutlet weak var ProfileImage: CircleImageView!
    @IBOutlet weak var NameCompany: UILabel!
    @IBOutlet weak var Hour: UILabel!
    @IBOutlet weak var AboutCompanyLabel: UITextView!
    @IBOutlet weak var CompanyActivityLabel: UILabel!
    @IBOutlet weak var NumberBranchLabel: UILabel!
    @IBOutlet weak var PlaceBranchLabel: UILabel!
    @IBOutlet weak var RegionLabel: UILabel!
    @IBOutlet weak var ExperienceLabel: UILabel!
    @IBOutlet weak var StreetLabel: UILabel!
    @IBOutlet weak var HourLabel: UILabel!
    @IBOutlet weak var DayAvilable: UILabel!

    @IBOutlet weak var AcceptButton: Radius_of_button!
    @IBOutlet weak var RejectionButton: Radius_of_button!
    @IBOutlet weak var PhoneCountLabel: UILabel!
    @IBOutlet weak var PhoneLabel: UILabel!
}
