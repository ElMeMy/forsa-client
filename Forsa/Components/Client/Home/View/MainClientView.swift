//
//  MainClientView.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/1/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class MainClientView: UIView {


    @IBOutlet weak var BgImage: UIImageView!
    @IBOutlet weak var NatButton: UIButton!
    @IBOutlet weak var SideMenuButton: UIButton!
    @IBOutlet weak var cityTextField: TextFieldRadius!
    @IBOutlet weak var RegionTextField: TextFieldRadius!
    @IBOutlet weak var FromDateTF: TextFieldRadius!
    @IBOutlet weak var ToDateTf: TextFieldRadius!
    @IBOutlet weak var NumberHour: TextFieldRadius!
    @IBOutlet weak var SalaryTf: TextFieldRadius!
    @IBOutlet weak var JopTitle: TextFieldRadius!
    @IBOutlet weak var backGroundImage: UIImageView!
    @IBOutlet weak var DownImage: UIImageView!
    @IBOutlet weak var Down2Image: UIImageView!
    @IBOutlet weak var Down3Image: UIImageView!
    @IBOutlet weak var TimeButtonOuttlelt: UIButton!
    @IBOutlet weak var Time2ButtonOuttlelt: UIButton!
    @IBOutlet weak var Down4Image: UIImageView!
    
}
