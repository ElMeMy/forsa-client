//
//  BankView.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/19/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class BankView: UIView {

    
    @IBOutlet weak var BackButton: UIButton!
    @IBOutlet weak var CameraImage: UIButton!
    @IBOutlet weak var PriceLabel: UILabel!
    @IBOutlet weak var AccountLabel: UILabel!
    @IBOutlet weak var BankTf: TextFieldRadius!
    @IBOutlet weak var AccountNameTf: TextFieldRadius!
    @IBOutlet weak var AccountNumTf: TextFieldRadius!
    @IBOutlet weak var AmountTf: TextFieldRadius!
    @IBOutlet weak var bankTableView: UITableView!
}
