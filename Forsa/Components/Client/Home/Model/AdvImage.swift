//
//  AdvImages.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/19/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
import ImageSlideshow

class AdvImage {
    var img:UIImage
    var imgData:Data
    init(img:UIImage, imgData:Data) {
        self.img = img
        self.imgData = imgData
    }
}
class AdvImageEdit {
    var img:UIImage
    var imgData:Data
    var id:Int
    var kf:KingfisherSource?
    init(img:UIImage, imgData:Data, id:Int) {
        self.img = img
        self.imgData = imgData
        self.id = id
    }
}
