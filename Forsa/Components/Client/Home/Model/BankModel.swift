//
//  BankModel.swift
//  Forsa
//
//  Created by ahmed elmemy on 12/24/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import Foundation

struct BankModel {
    
    private(set) public  var id : Int
    private(set) public  var name : String
    private(set) public  var accountNumber : String
 
    init(id:Int,name:String,accountNumber:String) {
        
        
        self.id = id
        self.name = name
        self.accountNumber = accountNumber

        
    }
    
    
}
