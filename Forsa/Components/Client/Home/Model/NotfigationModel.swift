//
//  NotfigationModel.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/18/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import Foundation

struct NotfigationModel {
    
    public  var Id : Int
    public  var type : Int
    public  var user_name : String
    public  var fk_order : Int
    public  var name : String
    public  var reason : String
    public  var fk_job : Int
    public  var provider_id : Int


    init(Id:Int,type:Int,user_name:String,fk_order:Int,name:String,reason:String,fk_job:Int,provider_id:Int) {
        
        self.Id = Id
        self.type = type
        self.user_name = user_name
        self.fk_order = fk_order
        self.name = name
        self.reason = reason
        self.fk_job = fk_job
        self.provider_id = provider_id
    }
    
    
}
