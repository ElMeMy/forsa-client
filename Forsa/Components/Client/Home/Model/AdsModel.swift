//
//  AdsModel.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/18/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import Foundation

struct AdsModel {
    
    private(set) public  var order_id : Int
    private(set) public  var job_id : Int
    private(set) public  var name : String
    private(set) public  var user_name : String
    private(set) public  var img : String
    private(set) public  var expected_salary : String
    private(set) public  var about : String
    private(set) public  var fk_provider : Int
    private(set) public  var stutes : String
    private(set) public  var stutes_no : Int
    
    init(order_id:Int,job_id:Int,name:String,user_name:String,img:String,expected_salary:String,about:String,fk_provider:Int,stutes:String,stutes_no:Int) {
        
        
        self.order_id = order_id
        self.job_id = job_id
        self.name = name
        self.user_name = user_name
        self.img = img
        self.expected_salary = expected_salary
        self.about = about
        self.fk_provider = fk_provider
        self.stutes = stutes
        self.stutes_no = stutes_no

    }

   
}
