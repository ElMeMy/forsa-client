//
//  EmplyeeDetails.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/18/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

extension EmplyeeDetailsVc {
    
    func EmplyeeDetails()
    {
        
        let parameters = [
            "order_id"    : order_id!,
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            ] as [String : Any]
        startAnimating()
        API.POST(url: URLs.OrderDetails, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    break
                case 1:
                    if let data = dict["result"]
                    {
                        print("result",data)
                        
                        
                       let job_id        = data["fk_job"] as! Int
                        self.fk_job = job_id
                        let name        = data["name"] as? String ?? ""
                        self.EmployeeDetails.NameCompany.text = name
                        
                        let about        = data["about"] as? String ?? ""
                        self.EmployeeDetails.AboutCompanyLabel.text = about
                        
                        let work_hours  = data["work_hours"] as? String ?? ""
                        self.EmployeeDetails.Hour.text = work_hours
                        
                        let img        = data["img"] as? String ?? ""
                        let url = URL(string: img ?? "")
                        self.EmployeeDetails.ProfileImage.kf.setImage(with: url)
                        
                        let activity   = data["activity"] as? String ?? ""
                        self.EmployeeDetails.CompanyActivityLabel.text = activity
                        
                        
                        let branch_count = data["branch_count"] as? Int ?? 0
                        self.EmployeeDetails.NumberBranchLabel.text = "\(branch_count)"
                        
                        
                        let city   = data["city"] as? String ?? ""
                        self.EmployeeDetails.PlaceBranchLabel.text = city
                        
                        let Region   = data["Region"] as? String ?? ""
                        self.EmployeeDetails.RegionLabel.text = Region
                        
                        let street_branch   = data["street_branch"] as? String ?? ""
                        self.EmployeeDetails.StreetLabel.text = street_branch

                        
                        let timeDeleteOrdar   = data["work_hours"] as? String ?? ""
                        self.EmployeeDetails.HourLabel.text = timeDeleteOrdar

                        self.phone   = data["phone"] as? String ?? ""

                        self.EmployeeDetails.PhoneCountLabel.text = self.phone
                        
                        let stutes     = data["stutes"] as? Int ?? 0
                        self.stuteValiable = data["stutes"] as? Int ?? 0
                        if stutes == 0
                        {
                            
                        }
                        else if stutes == 1
                        {
                            self.EmployeeDetails.RejectionButton.isHidden = true
                        }
                        else if stutes == 2
                        {
                            self.EmployeeDetails.AcceptButton.isHidden = true
                            
                        }
                        else if stutes == 3
                        {
                            self.EmployeeDetails.AcceptButton.isHidden = true
                            self.EmployeeDetails.RejectionButton.isHidden = true
                        }
                        else if stutes == 4
                        {
                            self.EmployeeDetails.AcceptButton.isHidden = true
                            self.EmployeeDetails.RejectionButton.isHidden = true
                        }
                        else if stutes == 5
                        {
                            self.EmployeeDetails.AcceptButton.isHidden = true
                            self.EmployeeDetails.RejectionButton.isHidden = true
                        }
                        
                    }else{
                        self.stopAnimating()
                        
                        //weak internet
                        let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }

                    
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
                let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                    self.EmplyeeDetails()
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
}
