//
//  Terms.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/23/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit
extension TermsVc {
    
func GetTerms()
{
    let parameters = [
        "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
        ] as [String : Any]
    
    startAnimating()
    API.POST(url: URLs.Condtion_For_client, parameters: parameters, headers: nil) { (success, value) in
        if success{
            self.stopAnimating()
            
            let key = value["key"] as! Int
            let dict = value
            
            switch key {
            case  0:
                break
            case 1:
                if let data = dict["msg"]{
                    
                    self.term.Text.text = data as! String
                    
                }else{
                    self.stopAnimating()
                    
                    //weak internet
                    let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
                
            default : print(" ")
            }
            
        }else{
            self.stopAnimating()
            
        }
    }
}

}
