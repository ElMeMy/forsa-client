//
//  CallUs.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/27/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

extension CallUsVC {
    func CallUs()
    {
        
        guard let Message = Call.TextOutlet.text , !( Call.TextOutlet.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The Message Is Requried".localized())}
        
        let user_id = UserDefaults.standard.value(forKey: "Id") as? Int ?? 0

        let parameters = [
            "text"       : Message,
            "fk_user"    : user_id,
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            ] as [String : Any]
        
        
        
        let type_user = UserDefaults.standard.value(forKey: "type_user") as? String
        
        if  type_user == "client"{
             url = URLs.contactUs_for_client
        }
        else
        {
             url = URLs.contactUs_for_provider
        }
        startAnimating()
        API.POST(url: url!, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    break
                case 1:
                    let msg = dict["msg"]
                    
                    let alert = UIAlertController(title: "Done".localized(), message: msg as? String, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                        self.Call.TextOutlet.text = ""
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
    
    
    func FaceBook()
    {
        
        startAnimating()
        API.POST(url: URLs.GetSetting, parameters: [:], headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    break
                case 1:
                    if let data = dict["Setting"]{
                        
                        let faceBook = data["facebook"] as! String
                      

                        if let requestUrl = NSURL(string: faceBook) {
                            UIApplication.shared.openURL(requestUrl as URL)
                        }
                        
                    }else{
                        self.stopAnimating()
                        
                        //weak internet
                        let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
    
    
    func Twitter()
    {
        
        startAnimating()
        API.POST(url: URLs.GetSetting, parameters: [:], headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    break
                case 1:
                    if let data = dict["Setting"]{
                        
                        let twiter = data["twiter"] as! String
                        
                        if let requestUrl = NSURL(string: twiter) {
                            UIApplication.shared.openURL(requestUrl as URL)
                        }
                        
                    }else{
                        self.stopAnimating()
                        
                        //weak internet
                        let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
    
    func linkedIn()
    {
        
        startAnimating()
        API.POST(url: URLs.GetSetting, parameters: [:], headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    break
                case 1:
                    if let data = dict["Setting"]{
                        
                        let linkedin = data["linkedin"] as! String
                        
                        if let requestUrl = NSURL(string: linkedin) {
                            UIApplication.shared.openURL(requestUrl as URL)
                        }
                        
                    }else{
                        self.stopAnimating()
                        
                        //weak internet
                        let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
    

    
    
       func GetSettings()
       {
           
           startAnimating()
           API.POST(url: URLs.GetSetting, parameters: [:], headers: nil) { (success, value) in
               if success{
                   self.stopAnimating()
                   
                   let key = value["key"] as! Int
                   let dict = value
                   
                   switch key {
                   case  0:
                       break
                   case 1:
                       if let data = dict["Setting"]{
                           
                              
                               let number = data["Number"] as! String
                               self.Call.Number.text = number
                        
                        
                              let Email = data["Email"] as! String
                              self.Call.Email.text = Email
                               
                           
                       }else{
                           self.stopAnimating()
                           
                           //weak internet
                           let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                           alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                               
                           }))
                           self.present(alert, animated: true, completion: nil)
                           
                       }
                       
                       
                   default : print(" ")
                   }
                   
               }else{
                   self.stopAnimating()
                   
               }
           }
       }
       
}
