//
//  AcceptOrder.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/19/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

extension EmplyeeDetailsVc {
    func AcceptOrder()
    {
        
        let parameters = [
            "id_order"    : order_id!,
            "stutes"    : "1",
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            ] as [String : Any]
        
        startAnimating()
        API.POST(url: URLs.ChangeStutes, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    break
                case 1:
                    let msg = dict["msg"]
                    
                    let alert = UIAlertController(title: "Done".localized(), message: msg as? String, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                        self.dismiss(animated: true, completion: nil)
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
}
