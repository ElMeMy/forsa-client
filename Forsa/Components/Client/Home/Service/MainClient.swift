//
//  mainProvider.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/26/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

extension MainClientVc {
    
    func GetCity() {
        let body = ["lang" : Language.currentLanguage().contains("en") ? "en" : "ar",]
        
        startAnimating()
        API.POST(url: URLs.GetCities, parameters: body, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                let dict = value
                
                if let v = dict["key"] as? Int{
                    
                    switch v {
                    case  0:
                        
                        let msg = dict["msg"]
                        
                        let alert = UIAlertController(title: "error".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    case 1:
                        
                        if let City = dict["City"] as? [[String: Any]]{
                            
                            for a in City{
                                let id = a["Id"] as! Int
                                let name = a["name"] as! String
                                self.City.append(CityModel(id: id, name: name))
                                
                            }
                            
                            
                        }else{
                            self.stopAnimating()
                            
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                        
                        
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    
    
    func getRegion() {
        let body = ["lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
                    "city_id" : city_id!
            ] as [String : Any]
        
        startAnimating()
        API.POST(url: URLs.GetRegion, parameters: body, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                let dict = value
                
                if let v = dict["key"] as? Int{
                    
                    switch v {
                    case  0:
                        
                        let msg = dict["msg"]
                        
                        let alert = UIAlertController(title: "error".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    case 1:
                        
                        if let Region = dict["Region"] as? [[String: Any]]{
                            
                            
                            if self.emptyCity{
                                self.MainClient.cityTextField.isUserInteractionEnabled = true
                                self.MainClient.cityTextField.inputView = self.pickerViewCity
                            }
                            
                            self.Region = []
                            for a in Region{
                                let id = a["Id"] as! Int
                                let name = a["name"] as! String
                                self.Region.append(RegionModel(id: id, name: name))
                                
                            }
                            
                            
                        }else{
                            self.stopAnimating()
                            
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                        
                        
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    func makeAds()
    {
        
        guard let city = MainClient.cityTextField.text , !( MainClient.cityTextField.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The City Is Requried".localized())}
        
        guard let Region = MainClient.RegionTextField.text , !( MainClient.RegionTextField.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The Region Is Requried".localized())}
        
        
        
        guard let hour = MainClient.NumberHour.text , !( MainClient.NumberHour.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The Number Hour Is Requried".localized())}
        
        
        guard let From = MainClient.FromDateTF.text , !( MainClient.FromDateTF.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The From Date Is Requried".localized())}
        
        
        guard let To = MainClient.ToDateTf.text , !( MainClient.ToDateTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The To Date Is Requried".localized())}

        
        guard let Salary = MainClient.SalaryTf.text , !( MainClient.SalaryTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The Salary  Is Requried".localized())}
        
        guard let Time1 = MainClient.TimeButtonOuttlelt.currentTitle , !( MainClient.TimeButtonOuttlelt.currentTitle?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The Presence  Is Requried".localized())}

        guard let Time2 = MainClient.Time2ButtonOuttlelt.currentTitle , !( MainClient.Time2ButtonOuttlelt.currentTitle?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The departure  Is Requried".localized())}




        
        let user_id = UserDefaults.standard.value(forKey: "Id") as? Int ?? 0

        let parameters = [
            "city_id"    : city_id ?? 0,
            "region_id"  : Region_id ?? 0,
            "start"      : From,
            "end"        : To,
            "hour_no"    : hour,
            "attendees_time"     : Time1,
            "take_off"           : Time2,
            "expected_salary"    : Salary,
            "client_id"    : user_id,
            "fk_job_title"    : Job_id ?? 0,
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            ] as [String : Any]
        startAnimating()
        API.POST(url: URLs.AddAdvertsment, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    let msg = dict["msg"]

                    let alert = UIAlertController(title: "error".localized(), message: msg as? String, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                    }))
                    self.present(alert, animated: true, completion: nil)
                case 1:
                    let msg = dict["msg"]
                    
                    let alert = UIAlertController(title: "Done".localized(), message: msg as? String, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
  
    
}




