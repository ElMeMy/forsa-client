//
//  AcceptOrderService.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/11/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

extension AcceptOrderVc {
    
    func GetSetting() {
        let body = ["lang" : Language.currentLanguage().contains("en") ? "en" : "ar","fk_job" : fk_job!] as [String : Any]
        
        startAnimating()
        API.POST(url: URLs.GetSetting, parameters: body, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                let dict = value
                
                if let v = dict["key"] as? Int{
                    
                    switch v {
                    case  0:
                        
                        let msg = dict["msg"]
                        
                        let alert = UIAlertController(title: "error".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    case 1:
                        
                        if let Setting = dict["Setting"]!["DatePaymentOrdar"] as? String{
                                self.AcceptOrder.HourLabel.text = Setting
                        }else{
                            self.stopAnimating()
                            
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                        
                        
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    
    
  
    
    
    
}




