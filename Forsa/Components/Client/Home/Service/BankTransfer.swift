//
//  BankTransfer.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/19/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit
import SlideMenuControllerSwift
extension BankTransferVc {
    
    func BankTransfer()
    {
        let parameters = [
            "fk_job" : fk_job!,
            ] as [String : Any]
        

        startAnimating()
        API.POST(url: URLs.GetSetting, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    break
                case 1:
                    
                    if let data = dict["Setting"]
                    {
                        print("result",data)
                        
                        
                        let cost                  = data["cost"] as! Double 
                        self.bankView.PriceLabel.text = "\(cost)"
                        self.bankView.AmountTf.text =  "\(cost)"
                        
                        if let data = dict["banks"] as? [[String: Any]]{
                            self.Items=[]
                            //get data from JSON
                            for i in data{
                                let id          = i["Id"] as? Int ?? 0
                                let name        = i["bank_name"] as? String ?? ""
                                let bankNumber        = i["bank_number"] as? String ?? ""
                                self.Items.append(BankModel(id: id, name: name, accountNumber: bankNumber))
                                
                                
                            }
                            
                            self.bankView.bankTableView.animateTable()
                            
                   
                            
                            
                        }
                        
                    }else{
                        self.stopAnimating()
                        
                        //weak internet
                        let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }

                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
    
    
    func SendBankTransfet()
    {
        
        guard let BankName = bankView.BankTf.text , !( bankView.BankTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The Bank Name Is Requried".localized())}
        
        
        guard let AccountName = bankView.AccountNameTf.text , !( bankView.AccountNameTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The Account Name Is Requried".localized())}
        
        
        guard let AccountNum = bankView.AccountNumTf.text , !( bankView.AccountNumTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The Account Number Is Requried".localized())}


        
        guard let Amount = bankView.AmountTf.text , !( bankView.AmountTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "The Amount Is Requried".localized())}
        
        let Id = UserDefaults.standard.value(forKey: "Id") as? Int ?? 0

        
        
        let parameters = [
            "id_order" : "\(order_id!)",
            "client_id" : "\(Id)",
            "bank_name" : BankName,
            "owner_name" : AccountName,
            "number" : AccountNum,
            "price" : Amount,
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            ] as [String : Any]
        var imagesData = [Data]()
        var imagesKeys = [String]()
        print("ddldllddld",parameters)
        for i in images{
            imagesData.append(i.imgData)
            imagesKeys.append("image")
        }
        startAnimating()
        
        API.POSTImage(url: URLs.Pay, Images: imagesData, Keys: imagesKeys, header: [:], parameters: parameters) { (success, value) in
            
            if success{
                self.stopAnimating()
                let dict = value
                if let v = dict["key"] as? Int{
                    switch v {
                    case  0:
                        
                           if let msg = dict["msg"]{
                            
                            let alert = UIAlertController(title: "Error".localized(), message: msg as? String, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title:"Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                                self.dismiss(animated: true, completion: nil)
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    case 1:
                        if let msg = dict["msg"]{
                            
                            let alert = UIAlertController(title: "Done".localized(), message: msg as? String, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title:"Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                             
                                let type_user = UserDefaults.standard.value(forKey: "type_user") as? String
                                let lang = Language.currentLanguage()
                                
                                if  type_user == "client"{
                                    
                                    if lang == "en"
                                    {
                                        let vc2 = Storyboard.Home.instantiate(MainVc.self)
                                        let right = Storyboard.Home.instantiate(SideMenuVC.self)
                                        let sideMenu = SlideMenuController(mainViewController: vc2, leftMenuViewController: right)
                                        self.view.window?.rootViewController = sideMenu
                                    }
                                    else
                                    {
                                        let vc2 = Storyboard.Home.instantiate(MainVc.self)
                                        let right = Storyboard.Home.instantiate(SideMenuVC.self)
                                        let sideMenu = SlideMenuController(mainViewController: vc2, rightMenuViewController: right)
                                        self.view.window?.rootViewController = sideMenu
                                    }
                                    
                                }

                            }))
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            self.stopAnimating()
                            
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                    default : print(" ")
                    }
                }
                
            }
        }
        
    }
}
