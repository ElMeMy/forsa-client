//
//  NatRefusedCell.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/27/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class NatRefusedCell: UITableViewCell {
    
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var CloseButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.outerView.clipsToBounds = true
        self.outerView.layer.cornerRadius = 15
        outerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]

    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        NameLabel.text = nil
    }
    func updateView(NatItem: NotfigationModel) {
        NameLabel.text          = NatItem.name
    }
    
}
