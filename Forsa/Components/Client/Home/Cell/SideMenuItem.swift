//
//  SideMenuItem.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/19/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

class SideMenuItem {
    var title:String
    var img:UIImage
    var active:Bool
    
    init(title:String,img:UIImage,active:Bool) {
        self.title = title
        self.img = img
        self.active = active
    }
}
