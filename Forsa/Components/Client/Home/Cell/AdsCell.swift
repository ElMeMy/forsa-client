//
//  AdsCell.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/18/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class AdsCell: UITableViewCell {

    @IBOutlet weak var outleltView: UIView!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var AdsImage: CircleImageView!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var status: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        outleltView.layer.cornerRadius = 10
        outleltView.dropShadow()

    }
    func updateView(AdsItem: AdsModel) {
        
           self.AdsImage.setImageWith(AdsItem.img.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed))
        
        Name.text               = AdsItem.name
        desc.text               = AdsItem.about
        status.text             = AdsItem.stutes
    }
    
}
