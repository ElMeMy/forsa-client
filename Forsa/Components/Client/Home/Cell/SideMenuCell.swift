//
//  SideMenuCell.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/20/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//
import UIKit

class SideMenuCell: UITableViewCell {
    
    @IBOutlet weak var ImageIcon: UIImageView!
    @IBOutlet weak var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func configCell(item: SideMenuItem) {
        ImageIcon.image = item.img
        name.text = item.title
    }
    
}
