//
//  MyOrderClientCell.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/30/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class MyOrderClientCell: UITableViewCell {
    
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var stutesLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var RegionLabel: UILabel!
    @IBOutlet weak var Job_titleLabel: UILabel!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var OrderStatus: UIImageView!
    @IBOutlet weak var DeleteButton: UIButton!
    @IBOutlet weak var EditButton: UIButton!
    var DeleteAction : (()->())?
    var EditAction : (()->())?

    override func awakeFromNib() {
        super.awakeFromNib()
        DeleteButton.setImage(#imageLiteral(resourceName: "noun_Delete_1981338"), for: .normal)
        EditButton.setImage(#imageLiteral(resourceName: "pencil"), for: .normal)
        self.outerView.clipsToBounds = true
        self.outerView.layer.cornerRadius = 15
        outerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
    }
    
    func updateView(OrderItem: OrderModel) {
        NameLabel.text          = OrderItem.name
        cityLabel.text          = OrderItem.city
        RegionLabel.text        = OrderItem.Region
        Job_titleLabel.text     = OrderItem.Job_title
        stutesLabel.text     = OrderItem.stutes
    }
    @IBAction func DeleteAction(_ sender: Any) {
        DeleteAction?()

    }
    @IBAction func EditAction(_ sender: Any) {
        EditAction?()
        
    }
    
}
