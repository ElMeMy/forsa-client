//
//  EditProviderExtetnion.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/15/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//



import UIKit
import ActionSheetPicker_3_0


extension EditProviderVc: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1
        {
            return Branch.count
        }
        return 1
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 1
        {
            return Branch[row].name
        }
        
        return ""
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1
        {
            self.RegisterPro.NumberBranchTextField.text = self.Branch[row].name
            Branch_id = Branch[row].id!

        }
        
    }
    
    
}





