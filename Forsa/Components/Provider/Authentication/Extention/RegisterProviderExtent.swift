//
//  RegisterProviderExtent.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/2/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit
import ActionSheetPicker_3_0


extension RegisterProviderVc: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1
        {
            return Branch.count
        }
        return 1
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 1
        {
            return Branch[row].name
        }
      
        return ""
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1
        {
            self.RegisterPro.NumberBranchTextField.text = self.Branch[row].name
            Branch_id = Branch[row].id!
            
            if Branch_id == 1 {
                
                self.BranchDataItem = []
                self.BranchDataItem.append(BrachDataModel(id: 0, TheCity: "", Region: "", Street: "", Hour: "", cityId: 0, regionId: 0))
                RegisterPro.HeightTableView.constant = 250

                self.RegisterPro.BranchTableView.reloadData()
                
            }else if Branch_id == 2{
                self.BranchDataItem = []
                self.BranchDataItem.append(BrachDataModel(id: 0, TheCity: "", Region: "", Street: "", Hour: "", cityId: 0, regionId: 0))
                 self.BranchDataItem.append(BrachDataModel(id: 0, TheCity: "", Region: "", Street: "", Hour: "", cityId: 0, regionId: 0))
                RegisterPro.HeightTableView.constant = 250 * 2
                    
                self.RegisterPro.BranchTableView.reloadData()
            }else if Branch_id == 3{
                self.BranchDataItem = []
                 self.BranchDataItem.append(BrachDataModel(id: 0, TheCity: "", Region: "", Street: "", Hour: "", cityId: 0, regionId: 0))
                 self.BranchDataItem.append(BrachDataModel(id: 0, TheCity: "", Region: "", Street: "", Hour: "", cityId: 0, regionId: 0))
                 self.BranchDataItem.append(BrachDataModel(id: 0, TheCity: "", Region: "", Street: "", Hour: "", cityId: 0, regionId: 0))
                RegisterPro.HeightTableView.constant = 250 * 3
                
                self.RegisterPro.BranchTableView.reloadData()
            }
            
//            self.BranchDataItem = []
//            self.BranchDataItem.append([BranchModel(id: 1, name: "ed"),BranchModel(id: 1, name: "ed")])
//            self.RegisterPro.BranchTableView.reloadData()
        }
        
    }
    
    
}




extension RegisterProviderVc: UITableViewDataSource, UITableViewDelegate{
    
//    var doneBtnTitle = "Done"
//    var cancelBtnTitle = "Cancel"
    
    func setUpBranch(){
        RegisterPro.BranchTableView.delegate = self
        RegisterPro.BranchTableView.dataSource = self
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return BranchDataItem.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "BranchCell") as? BranchCell
        {
            let Call = BranchDataItem[indexPath.row]
            cell.updateView(BranchDataItem: Call)
            cell.regionBtn.isHidden = true
         
            cell.cityButon = {
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.alignment = .center
                
                let pick2 = ActionSheetStringPicker(title: nil, rows: self.citiesPickerName, initialSelection: 0, doneBlock: {
                    picker, indexes, values in
                    
                    let chosenName = values as? String
                    
                    for i in 0..<self.citiesPickerName.count {
                        if chosenName == self.citiesPickerName[i] {
                            cell.chosenCityID = self.citiesPickerId[i]
                        }
                    }
                    cell.regionBtn.isHidden = false
//                    print("idddd \(Call.cityId)")
                    
                    cell.TheCityTf.text = values as? String
          
                    return
                }, cancel: { ActionMultipleStringCancelBlock in return },origin: cell.cityBtn)
                
                let bar1 = UIBarButtonItem.init(title: "Done".localized(), style: .plain, target: self, action: #selector(self.actionPickerDone))
                let bar2 = UIBarButtonItem.init(title: "Cancel".localized(), style: .plain, target: self, action: #selector(self.actionPickerCancel))
                pick2?.setDoneButton(bar1)
                pick2?.setCancelButton(bar2)
                pick2?.show()
                
            }
            cell.regionButton = {
                    
                self.startAnimating()
                    
                API.POST(url: URLs.GetRegion, parameters: ["city_id": cell.chosenCityID!,"lang": Language.currentLanguage()], headers: nil, completion: {
                    (success, value) in
                        
                    self.stopAnimating()
                    if success {
                            
                        if let returnKey = value["key"] as? Int {
                            if returnKey == 1 {
                                 let Regions = value["Region"] as? [[String: Any]]
                                self.regionPickerName = []
                                self.regionPickerId = []

            
                                for i in Regions! {
                                    self.regionPickerName.append((i["name"] as? String)!)
                                    self.regionPickerId.append((i["Id"] as? Int)!)
                                        
                                }
                                let paragraphStyle = NSMutableParagraphStyle()
                                paragraphStyle.alignment = .center
                                
                                let pick = ActionSheetStringPicker(title: nil, rows: self.regionPickerName, initialSelection: 0, doneBlock: {
                                    picker, indexes, values in
                                    
                                    let chosenName = values as? String
                                    
                                    for i in 0..<self.regionPickerName.count {
                                        if chosenName == self.regionPickerName[i] {
                                            Call.regionId = self.regionPickerId[i]
                                        }
                                    }
                                    
                                    print("idddd \(Call.regionId)")
                                    
                                    cell.RegionTf.text = values as? String
                                    
                                    return
                                }, cancel: { ActionMultipleStringCancelBlock in return },origin: cell.cityBtn)
                                
                                let bar1 = UIBarButtonItem.init(title: "Done".localized(), style: .plain, target: self, action: #selector(self.actionPickerDone))
                                let bar2 = UIBarButtonItem.init(title: "Cancel".localized(), style: .plain, target: self, action: #selector(self.actionPickerCancel))
                                pick?.setDoneButton(bar1)
                                pick?.setCancelButton(bar2)
                                pick?.show()
                                
                            }
                        }
                    } else {
                            
                    }
                })
                
                
            }
            return cell
        }
        else
        {
            return BranchCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      
        return 250
    }
    
    @objc func actionPickerCancel(){
        
    }
    
    @objc func actionPickerDone(){
        
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = Storyboard.Home.instantiate(EmplyeeDetailsVc.self)
//        let order_id = AdsItem[indexPath.row].order_id
//        vc.order_id = order_id
//        present(vc,animated: true, completion: nil)
//
//    }
    
}

