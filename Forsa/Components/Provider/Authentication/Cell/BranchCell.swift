//
//  BranchCell.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/2/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class BranchCell: UITableViewCell {
    @IBOutlet weak var TheCityTf: TextFieldRadius!
    @IBOutlet weak var RegionTf: TextFieldRadius!
    @IBOutlet weak var StreetTf: TextFieldRadius!
    @IBOutlet weak var HourTf: TextFieldRadius!
    @IBOutlet weak var cityBtn: UIButton!
    @IBOutlet weak var regionBtn: UIButton!
    
    var chosenCityID: Int?
    var chosenRegionID: Int?
    var cityButon: (()->())?
    var regionButton: (()->())?
    
    var doneBtn: (()->())?
    var cancelBtn: (()->())?
    
    var doneBtnTitle = "Done"
    var cancelBtnTitle = "Cancel"
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cityBtn.setImage(#imageLiteral(resourceName: "down"), for: .normal)
        regionBtn.setImage(#imageLiteral(resourceName: "down"), for: .normal)
    }

    func updateView(BranchDataItem: BrachDataModel) {
        TheCityTf.text               = BranchDataItem.TheCity
        RegionTf.text                = BranchDataItem.Region
        StreetTf.text                = BranchDataItem.Street
        HourTf.text                  = BranchDataItem.Hour
        BranchDataItem.Hour          = HourTf.text
        BranchDataItem.Street        = StreetTf.text
        
    }
    
    @objc func actionPickerCancel(){
        cancelBtn?()
    }
    
    @objc func actionPickerDone(){
        doneBtn?()
    }
    
    @IBAction func cityBtnPressed(_ sender: UIButton) {
        cityButon?()
    }
    @IBAction func regionBtnPressed(_ sender: UIButton) {
        regionButton?()
    }
    
    
}
