//
//  AvaliableCell.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/16/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class AvaliableCell: UICollectionViewCell {
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(DayItem: Daymodel){
        self.title.text = DayItem.title
      
    }
}
