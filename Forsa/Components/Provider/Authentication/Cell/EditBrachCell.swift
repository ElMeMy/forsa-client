//
//  BrachCell.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/16/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class EditBrachCell: UITableViewCell {

    
    @IBOutlet weak var TheCityTf: TextFieldRadius!
    @IBOutlet weak var RegionTf: TextFieldRadius!
    @IBOutlet weak var StreetTf: TextFieldRadius!
    @IBOutlet weak var HourTf: TextFieldRadius!
    
    var DeleteAction: (()->())?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func updateView(BranchDataItem: BrachDataModel) {
        TheCityTf.text               = BranchDataItem.TheCity
        RegionTf.text                = BranchDataItem.Region
        StreetTf.text                = BranchDataItem.Street
        HourTf.text                  = BranchDataItem.Hour
        BranchDataItem.Hour          = HourTf.text
        BranchDataItem.Street        = StreetTf.text
        
    }
    
    
    @IBAction func DeleteActionPressed(_ sender: UIButton) {
        DeleteAction?()
    }

}
