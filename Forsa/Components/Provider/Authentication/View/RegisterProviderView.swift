//
//  RegisterProviderView.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/2/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
import BEMCheckBox
class RegisterProviderView: UIView {

    @IBOutlet weak var BackAction: UIButton!
    @IBOutlet weak var Name: TextFieldRadius!
    @IBOutlet weak var CommericalTextField: TextFieldRadius!
    @IBOutlet weak var ActivityTextField: TextFieldRadius!
    @IBOutlet weak var HourTextField: TextFieldRadius!
    @IBOutlet weak var PhoneTextField: TextFieldRadius!
    @IBOutlet weak var EmailTextFiled: TextFieldRadius!
    @IBOutlet weak var NumberBranchTextField: TextFieldRadius!
    @IBOutlet weak var BranchTableView: UITableView!
    @IBOutlet weak var UserNameTf: TextFieldRadius!
    @IBOutlet weak var PasswordTf: TextFieldRadius!
    @IBOutlet weak var DownImage: UIImageView!
    @IBOutlet weak var HeightTableView: NSLayoutConstraint!
    @IBOutlet weak var AcceptTerms: BEMCheckBox!

}
