//
//  LoginProvider.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/25/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
extension LoginProviderVc {
    
    func LoginProvider()
    {
        
        guard let UserName = loginProviderview.UserNameTf.text , !(loginProviderview.UserNameTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "UserName Is Requried".localized())}
        
        guard let Password = loginProviderview.PasswordTf.text , !(loginProviderview.PasswordTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Password Is Requried".localized())}
        
        let token = UserDefaults.standard.value(forKey: "mobileToken") as? String ?? "token"

        let parameters = [
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            "user_name" :UserName,
            "password" :Password,
            "token" :token,
            
            ] as [String : Any]
        
        print("paraaaaa",parameters,URLs.Login)
        startAnimating()
        API.POST(url: URLs.Login, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    let msg = dict["msg"]
                    
                    let alert = UIAlertController(title: "ERROR".localized(), message: msg as? String, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                case 1:
                    if let userInfo = dict["provider"] as? [String: Any]{
                        
                        let def = UserDefaults.standard
                        let Id = userInfo["Id"] as? Int ?? 0
                        def.set(Id, forKey: "Id")
                        
                        
                        
                        let token = userInfo["token"] as? String ?? ""
                        def.set(token, forKey: "token")
                        
                        
                        
                        let name = userInfo["name"] as? String ?? ""
                        def.set(name, forKey: "name")
                        
                        
                        
                        let img = userInfo["img"] as? String ?? ""
                        def.set(img, forKey: "img")
                        
                        let type_user = userInfo["type_user"] as? String ?? ""
                        def.set(type_user, forKey: "type_user")
                        
                        let lang = Language.currentLanguage()
                        if lang == "en"
                        {
                            let vc2 = Storyboard.Home.instantiate(MainProviderVc.self)
                            let right = Storyboard.Home.instantiate(SideMenuVC.self)
                            let sideMenu = SlideMenuController(mainViewController: vc2, leftMenuViewController: right)
                            self.view.window?.rootViewController = sideMenu
                            
                        }
                        else
                        {
                            let vc2 = Storyboard.Home.instantiate(MainProviderVc.self)
                            let right = Storyboard.Home.instantiate(SideMenuVC.self)
                            let sideMenu = SlideMenuController(mainViewController: vc2, rightMenuViewController: right)
                            self.view.window?.rootViewController = sideMenu
                            
                        }
                      
                    }
                    else
                    {
                        let alert = UIAlertController(title: "ERROR".localized(), message: "This User does not exist", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
}
