//
//  ForgetProvider.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/25/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

extension ForgetProviderVc {
    
    func ForgetProvider()
    {
        guard let phone = forgetView.PhoneTf.text , !(forgetView.PhoneTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Phone Is Requried".localized())}
        
        
        let parameters = [
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            "phone" :phone,
            ] as [String : Any]
        
        
        startAnimating()
        
        API.POST(url: URLs.ReSendCodeProvider, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    let msg = dict["msg"]
                    
                    let alert = UIAlertController(title: "ERROR".localized(), message: msg as? String, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                case 1:
                    let vc = Storyboard.Authentication.instantiate(NewPasswordProviderVc.self)
                    vc.phone = phone
                    self.present(vc, animated: true, completion: nil)
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
    
}
