//
//  BrachDataModel.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/2/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

class BrachDataModel {
    var id: Int?
    var TheCity: String?
    var Region: String?
    var Street: String?
    var Hour: String?
    var cityId: Int?
    var regionId: Int?

    init(id: Int, TheCity: String,Region:String,Street:String,Hour:String,cityId:Int,regionId: Int) {
        self.id = id
        self.cityId = cityId
        self.TheCity = TheCity
        self.Region  = Region
        self.Street  = Street
        self.Hour    = Hour
        self.regionId = regionId
    }
}

class ListModel {
    
    var city_Id: Int?
    var region: Int?
    var street: String?
    var work_Hours: String?
    init(city_id: Int, region: Int,street: String, work_hours: String) {
        self.city_Id = city_id
        self.region = region
        self.street = street
        self.work_Hours = work_hours
    }
    
    
}
