//
//  BranchModel.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/2/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


class BranchModel {
    var id: Int?
    var name: String?
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
}
