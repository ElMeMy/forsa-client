//
//  LangVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/4/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class LangVc: UIViewController {

  

    @IBOutlet weak var MainScreanOutlelt: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MainScreanOutlelt.image = #imageLiteral(resourceName: "logo_login")
    }
    
    @IBAction func arabicButtonPressed(_ sender: Any) {
        
        Language.setAppLanguage(lang: "ar")
        UIView.appearance().semanticContentAttribute = .forceRightToLeft
        let vc = Storyboard.Authentication.instantiate(LoginProviderClientVc.self)
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func En(_ sender: Any) {
        
        Language.setAppLanguage(lang: "en")
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        let vc = Storyboard.Authentication.instantiate(LoginProviderClientVc.self)
        self.present(vc, animated: true, completion: nil)
    }

}
