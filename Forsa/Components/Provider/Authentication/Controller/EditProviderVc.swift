//
//  EditProviderVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/15/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
import SwiftyJSON

class EditProviderVc: UIViewController {

   
    var Branch          = [BranchModel]()
    let pickerViewBranch = UIPickerView()
    var Branch_id   : Int?
    
    var listData = [ListModel]()
    
    var citiesPickerName = [String]()
    var citiesPickerId = [Int]()
    
    var regionPickerName = [String]()
    var regionPickerId = [Int]()
    
    var Region  = [RegionModel]()
    
    public var RegisterPro: EditProviderView! {
        guard isViewLoaded else { return nil }
        return (view as! EditProviderView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GetDataOfProvider()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        RegisterPro.BackAction.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)
//        RegisterPro.HeightTableView.constant = 0
        pickerViewBranch.delegate = self
        RegisterPro.NumberBranchTextField.inputView = pickerViewBranch
        pickerViewBranch.tag = 1
        Branch.append(BranchModel(id: 1, name: "1"))
        Branch.append(BranchModel(id: 2, name: "2"))
        Branch.append(BranchModel(id: 3, name: "3"))
        RegisterPro.BackGroundImage.image = #imageLiteral(resourceName: "bg")

        getCities()
        
    }
    
    func getCities(){
        
        self.startAnimating()
        
        API.POST(url: URLs.GetCities, parameters: ["lang": Language.currentLanguage()], headers: nil, completion: {
            (success, value) in
            
            self.stopAnimating()
            if success {
                
                if let returnKey = value["key"] as? Int {
                    if returnKey == 1 {
                        let cities = value["City"] as? [[String: Any]]
                        for i in cities! {
                            self.citiesPickerName.append((i["name"] as? String)!)
                            self.citiesPickerId.append((i["Id"] as? Int)!)
                            
                        }
                    }
                }
            } else {
                
            }
        })
        
    }

    func GetDataOfProvider(){
        
        let user_id = UserDefaults.standard.value(forKey: "Id") as? Int ?? 0

        
        let parameters = [
            "provider_id" : user_id,
         
            ] as [String : Any]
        
        self.startAnimating()
        
        API.POST(url: URLs.GetDataOfProvider, parameters: parameters, headers: nil, completion: {
            (success, value) in
            
            self.stopAnimating()
            if success {
                
                if let returnKey = value["key"] as? Int {
                    if returnKey == 1 {
                        let provider = value["provider"] as? [String: Any]
                        let user_name = provider!["user_name"] as? String ?? ""
                        self.RegisterPro.UserNameTf.text = user_name
                        
                        
                        let name = provider!["name"] as? String ?? ""
                        self.RegisterPro.Name.text = name
                        
                        
                        let email = provider!["email"] as? String ?? ""
                        self.RegisterPro.EmailTextFiled.text = email
                        
                        
                        
                        let phone = provider!["phone"] as? String ?? ""
                        self.RegisterPro.PhoneTextField.text = phone
                        
                        
                        let commercial_registration = provider!["commercial_registration_no"] as? String ?? ""
                        self.RegisterPro.CommericalTextField.text = commercial_registration
                        
                        
                        let about = provider!["about"] as? String ?? ""
                        self.RegisterPro.AboutTf.text = about
                        
                        
                        let branch_count = provider!["branch_count"] as? Int ?? 0
                        self.RegisterPro.NumberBranchTextField.text = "\(branch_count)"
                        
                        
                        let work_hours = provider!["work_hours"] as? String ?? ""
                        self.RegisterPro.HourTextField.text = work_hours
                        
                        
                        let activity = provider!["activity"] as? String ?? ""
                        self.RegisterPro.ActivityTextField.text = activity  
                        
                        
                        
                    }
                }
            } else {
                
            }
        })
        
    }
    
    @IBAction func BackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    @IBAction func EditBranchVc(_ sender: Any) {
        let vc = Storyboard.Authentication.instantiate(EditBranchViewController.self)
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func RegisterPressed(_ sender: Any) {
        
        
        
        guard let Name = RegisterPro.Name.text , !( RegisterPro.Name.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Name Is Requried".localized())}
        
    
        guard let commercial_registration = RegisterPro.CommericalTextField.text , !( RegisterPro.CommericalTextField.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "commercial registration Is Requried".localized())}
        
        
        guard let activity = RegisterPro.ActivityTextField.text , !( RegisterPro.ActivityTextField.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Activity Is Requried".localized())}
        
        
        guard let work_hours = RegisterPro.HourTextField.text , !( RegisterPro.HourTextField.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Hour Is Requried".localized())}
        
        
        guard let phone = RegisterPro.PhoneTextField.text , !( RegisterPro.PhoneTextField.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Phone Is Requried".localized())}
        
        
        guard let email = RegisterPro.EmailTextFiled.text , !( RegisterPro.EmailTextFiled.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Email Is Requried".localized())}
        
        
        guard let user_name = RegisterPro.UserNameTf.text , !( RegisterPro.UserNameTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "UserName Is Requried".localized())}
        
        
        guard let About = RegisterPro.AboutTf.text , !( RegisterPro.AboutTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "About Is Requried".localized())}

        
        guard let NumberBranchText = RegisterPro.NumberBranchTextField.text , !( RegisterPro.NumberBranchTextField.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Number Branch Is Requried".localized())}
        

        guard RegisterPro.PhoneTextField.text!.count >= 10  else {return Alert.showAlertOnVC(target: self, title: "error".localized(), message: "Mobile number must be greater than 10 digits".localized())}
        
        
        let user_id = UserDefaults.standard.value(forKey: "Id") as? Int ?? 0

        
        let parameters = [
            "id": user_id,
            "name": Name,
            "commercial_registration_no": commercial_registration,
            "activity": activity,
            "work_hours": work_hours,
            "phone": phone,
            "email": email,
            "about": About,
            "user_name": user_name,
            "password": RegisterPro.PasswordTf.text!,
            "lang": Language.currentLanguage()
            ] as [String : Any]
        
        self.startAnimating()
        
        API.POST(url: URLs.UpdateUserProvider, parameters: parameters, headers: nil, completion: {
            (success, value) in
            
            self.stopAnimating()
            
            if success {
                
                if let returnValue = value["key"] as? Int {
                    
                    if returnValue == 0 {
                        let msg = value["msg"]
                        
                        let alert = UIAlertController(title: "ERROR".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    if returnValue == 1 {
                        
                        let msg = value["msg"]
                        
                        let alert = UIAlertController(title: "Done".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                } else {
                    
                    let msg = value["msg"] as? String
                    let alert = UIAlertController(title: "ERROR".localized(), message: msg!, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                
            } else {
                
            }
        })
        
        
    }
    
   
}
