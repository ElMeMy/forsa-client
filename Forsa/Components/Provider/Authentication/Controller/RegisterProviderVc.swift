//
//  RegisterProviderVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/17/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
import SwiftyJSON

class RegisterProviderVc: UIViewController {

    var Branch          = [BranchModel]()
    var BranchDataItem  = [BrachDataModel]()
    let pickerViewBranch = UIPickerView()
    var Branch_id   : Int?
    
    var listData = [ListModel]()
    
    var citiesPickerName = [String]()
    var citiesPickerId = [Int]()
    
    var regionPickerName = [String]()
    var regionPickerId = [Int]()

    var Region  = [RegionModel]()

    public var RegisterPro: RegisterProviderView! {
        guard isViewLoaded else { return nil }
        return (view as! RegisterProviderView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpBranch()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            RegisterPro.BackAction.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)

        }
        else
        {
            RegisterPro.BackAction.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        
        RegisterPro.HeightTableView.constant = 0
        pickerViewBranch.delegate = self
        RegisterPro.NumberBranchTextField.inputView = pickerViewBranch
        pickerViewBranch.tag = 1
        Branch.append(BranchModel(id: 1, name: "1"))
        Branch.append(BranchModel(id: 2, name: "2"))
        Branch.append(BranchModel(id: 3, name: "3"))

        getCities()
        
    }
    
    func getCities(){
        
        self.startAnimating()
        
        API.POST(url: URLs.GetCities, parameters: ["lang": Language.currentLanguage()], headers: nil, completion: {
            (success, value) in
            
            self.stopAnimating()
            if success {
                
                if let returnKey = value["key"] as? Int {
                    if returnKey == 1 {
                        let cities = value["City"] as? [[String: Any]]
                        for i in cities! {
                            self.citiesPickerName.append((i["name"] as? String)!)
                            self.citiesPickerId.append((i["Id"] as? Int)!)
                            
                        }
                    }
                }
            } else {
                
            }
        })
        
    }
    
    @IBAction func BackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func TermsPressed(_ sender: Any) {
        let vc = Storyboard.Home.instantiate(TermsVc.self)
        present(vc,animated: true,completion: nil)
    }
    
    @IBAction func RegisterPressed(_ sender: Any) {

    

    
        
        guard let Name = RegisterPro.Name.text , !( RegisterPro.Name.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Name Is Requried".localized())}
        

        guard let commercial_registration = RegisterPro.CommericalTextField.text , !( RegisterPro.CommericalTextField.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "commercial registration Is Requried".localized())}

        
        guard let activity = RegisterPro.ActivityTextField.text , !( RegisterPro.ActivityTextField.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Activity Is Requried".localized())}
        
        
        guard let work_hours = RegisterPro.HourTextField.text , !( RegisterPro.HourTextField.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Hour Is Requried".localized())}
        
        
        guard let phone = RegisterPro.PhoneTextField.text , !( RegisterPro.PhoneTextField.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Phone Is Requried".localized())}


        guard let email = RegisterPro.EmailTextFiled.text , !( RegisterPro.EmailTextFiled.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Email Is Requried".localized())}


        guard let user_name = RegisterPro.UserNameTf.text , !( RegisterPro.UserNameTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "UserName Is Requried".localized())}
        
        
        guard let NumberBranchText = RegisterPro.NumberBranchTextField.text , !( RegisterPro.NumberBranchTextField.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Number Branch Is Requried".localized())}

        
        guard let password = RegisterPro.PasswordTf.text , !( RegisterPro.PasswordTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Password Is Requried".localized())}

        
        
        guard RegisterPro.PasswordTf.text!.count >= 10  else {return Alert.showAlertOnVC(target: self, title: "error".localized(), message: "Password must be greater than 9 digits".localized())}
        
        
        
        guard RegisterPro.PhoneTextField.text!.count >= 10  else {return Alert.showAlertOnVC(target: self, title: "error".localized(), message: "Mobile number must be greater than 10 digits".localized())}

        
        
        
        guard RegisterPro.AcceptTerms.on == true else {
            Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Please accept the terms and conditions of the application".localized())
            return
        }
        
        
        
        let cells = RegisterPro.BranchTableView.visibleCells
        
        for i in 0..<cells.count {
            let cell = RegisterPro.BranchTableView.cellForRow(at: IndexPath(row: i, section: 0)) as! BranchCell
            
            var cityID: Int?
            
            for i in 0..<citiesPickerName.count {
                if cell.TheCityTf.text == citiesPickerName[i] {
                    cityID = citiesPickerId[i]
                    print(citiesPickerId[i])
                    print(cityID!)
                }
            }
            
            var regionID: Int?
            for i in 0..<regionPickerName.count {
                if cell.RegionTf.text == regionPickerName[i] {
                    regionID = regionPickerId[i]
                    print(regionPickerId[i])
                    print(regionID!)
                }
            }
            
            let item = ListModel(city_id: cityID ?? 0, region: regionID ?? 0, street: cell.StreetTf.text ?? "", work_hours: cell.HourTf.text ?? "")
            
            
            listData.append(item)
            
            
            
        }
        
        var dictArray = [[String: Any]]()
        
        for i in listData {
            let jsonObj = convertToDeict(item: i)
            dictArray.append(jsonObj)
        }
        
        print(dictArray)
        let token = UserDefaults.standard.value(forKey: "mobileToken") as? String ?? "token"

        let parameters = [
            "name": Name,
            "commercial_registration_no": commercial_registration,
            "activity": activity,
            "work_hours": work_hours,
            "phone": phone,
            "email": email,
            "branch_count": BranchDataItem.count,
            "branch": JSON(dictArray).rawString() as AnyObject,
            "user_name": user_name,
            "password": password,
            "token": token,
            "lang": Language.currentLanguage()
            ] as [String : Any]
        
        self.startAnimating()
        
        API.POST(url: URLs.registerURL, parameters: parameters, headers: nil, completion: {
            (success, value) in
            
            self.stopAnimating()
            
            if success {
                
                if let returnValue = value["key"] as? Int {
                    
                    if returnValue == 0 {
                        let msg = value["msg"]
                        
                        let alert = UIAlertController(title: "ERROR".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    if returnValue == 1 {
                        
                        let vc = Storyboard.Authentication.instantiate(LoginProviderVc.self)
                        self.present(vc, animated: true, completion: nil)
                        
                       
                        
                    }
                } else {
                    
                    let msg = value["msg"] as? String
                    let alert = UIAlertController(title: "ERROR".localized(), message: msg!, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                
            } else {
                
            }
        })
        
        
    }
    
    func convertToDeict(item: ListModel) -> [String: Any] {
        return ["city_id": item.city_Id!, "region": item.region!, "street": item.street!, "work_hours": item.work_Hours!]
    }
    
}
