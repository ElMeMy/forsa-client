//
//  AddBranchVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 7/16/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON
import  SlideMenuControllerSwift
class AddBranchVc: UIViewController {
    @IBOutlet weak var BackButton: UIButton!
    @IBOutlet weak var NumberBranchTextField: UITextField!
    @IBOutlet weak var BranchTableView: UITableView!
    @IBOutlet weak var HeightTableView: NSLayoutConstraint!

    var listData = [ListModel]()

    let pickerViewBranch = UIPickerView()

    var branchDataItem = [BrachDataModel]()
    
    var newBranches = [BrachDataModel]()
    
    var Branch          = [BranchModel]()
    var Branch_id   : Int?
    
    
    
    var citiesPickerName = [String]()
    var citiesPickerId = [Int]()
    
    var regionPickerName = [String]()
    var regionPickerId = [Int]()
    
    var Region  = [RegionModel]()


    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)

        }
        else
        {
            BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        
        NumberBranchTextField.inputView = pickerViewBranch
        pickerViewBranch.dataSource = self
        pickerViewBranch.delegate = self
        pickerViewBranch.tag = 1
        
        Branch.append(BranchModel(id: 1, name: "1"))
        Branch.append(BranchModel(id: 2, name: "2"))
        Branch.append(BranchModel(id: 3, name: "3"))
        
        getCities()
     

    }
    
    func getCities(){
        
        self.startAnimating()
        
        API.POST(url: URLs.GetCities, parameters: ["lang": Language.currentLanguage()], headers: nil, completion: {
            (success, value) in
            
            self.stopAnimating()
            if success {
                
                if let returnKey = value["key"] as? Int {
                    if returnKey == 1 {
                        let cities = value["City"] as? [[String: Any]]
                        for i in cities! {
                            self.citiesPickerName.append((i["name"] as? String)!)
                            self.citiesPickerId.append((i["Id"] as? Int)!)
                            
                        }
                    }
                }
            } else {
                
            }
        })
        
    }

    @IBAction func BackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func AddBranch(_ sender: Any) {
        
        let cells = BranchTableView.visibleCells
        
        for i in 0..<cells.count {
            let cell = BranchTableView.cellForRow(at: IndexPath(row: i, section: 0)) as! BranchCell
            
            var cityID: Int?
            
            for i in 0..<citiesPickerName.count {
                if cell.TheCityTf.text == citiesPickerName[i] {
                    cityID = citiesPickerId[i]
                    print(citiesPickerId[i])
                    print(cityID!)
                }
            }
            
            var regionID: Int?
            for i in 0..<regionPickerName.count {
                if cell.RegionTf.text == regionPickerName[i] {
                    regionID = regionPickerId[i]
                    print(regionPickerId[i])
                    print(regionID!)
                }
            }
            
            let item = ListModel(city_id: cityID ?? 0, region: regionID ?? 0, street: cell.StreetTf.text ?? "", work_hours: cell.HourTf.text ?? "")
            
            
            listData.append(item)
            
            
            
        }
        
        var dictArray = [[String: Any]]()
        
        for i in listData {
            let jsonObj = convertToDeict(item: i)
            dictArray.append(jsonObj)
        }
        
        
        for i in branchDataItem {
            let jsonObj = convertToDeictitem(item: i)
            dictArray.append(jsonObj)
        }
        
        print(dictArray)
        
        
        let user_id = UserDefaults.standard.value(forKey: "Id") as? Int ?? 0

        let parameters = [
            "provider_id" : user_id,
            "branch": JSON(dictArray).rawString() as AnyObject,
            ] as [String : Any]
        
        self.startAnimating()
        
        API.POST(url: URLs.EditBranch, parameters: parameters, headers: nil, completion: {
            (success, value) in
            
            self.stopAnimating()
            
            if success {
                
                if let returnValue = value["key"] as? Int {
                    
                    if returnValue == 0 {
                        let msg = value["msg"]
                        
                        let alert = UIAlertController(title: "ERROR".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    if returnValue == 1 {
                        
                       
                        let type_user = UserDefaults.standard.value(forKey: "type_user") as? String
                        let lang = Language.currentLanguage()
                        
                        if type_user == "provider"
                        {
                            
                            if lang == "en"
                            {
                                let vc2 = Storyboard.Home.instantiate(MainProviderVc.self)
                                let right = Storyboard.Home.instantiate(SideMenuVC.self)
                                let sideMenu = SlideMenuController(mainViewController: vc2, leftMenuViewController: right)
                                self.view.window?.rootViewController = sideMenu
                            }
                            else
                            {
                                let vc2 = Storyboard.Home.instantiate(MainProviderVc.self)
                                let right = Storyboard.Home.instantiate(SideMenuVC.self)
                                let sideMenu = SlideMenuController(mainViewController: vc2, rightMenuViewController: right)
                                self.view.window?.rootViewController = sideMenu

                            }
                        }
                        
                    }
                } else {
                    
                    let msg = value["msg"] as? String
                    let alert = UIAlertController(title: "ERROR".localized(), message: msg!, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                
            } else {
                
            }
        })
        

        
    }
    
}
extension AddBranchVc: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1
        {
            return Branch.count
        }
        return 1
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 1
        {
            return Branch[row].name
        }
        
        return ""
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1
        {
            self.NumberBranchTextField.text = self.Branch[row].name
            Branch_id = Branch[row].id!
            
            if Branch_id == 1 {
                
                self.newBranches = []
                self.newBranches.append(BrachDataModel(id: 0, TheCity: "", Region: "", Street: "", Hour: "", cityId: 0, regionId: 0))
                HeightTableView.constant = 250
                
                self.BranchTableView.reloadData()
                
            }else if Branch_id == 2{
                self.newBranches = []
                self.newBranches.append(BrachDataModel(id: 0, TheCity: "", Region: "", Street: "", Hour: "", cityId: 0, regionId: 0))
                self.newBranches.append(BrachDataModel(id: 0, TheCity: "", Region: "", Street: "", Hour: "", cityId: 0, regionId: 0))
                HeightTableView.constant = 250 * 2
                
                self.BranchTableView.reloadData()
            }else if Branch_id == 3{
                self.newBranches = []
                self.newBranches.append(BrachDataModel(id: 0, TheCity: "", Region: "", Street: "", Hour: "", cityId: 0, regionId: 0))
                self.newBranches.append(BrachDataModel(id: 0, TheCity: "", Region: "", Street: "", Hour: "", cityId: 0, regionId: 0))
                self.newBranches.append(BrachDataModel(id: 0, TheCity: "", Region: "", Street: "", Hour: "", cityId: 0, regionId: 0))
                HeightTableView.constant = 250 * 3
                
                self.BranchTableView.reloadData()
            }
            
            //            self.BranchDataItem = []
            //            self.BranchDataItem.append([BranchModel(id: 1, name: "ed"),BranchModel(id: 1, name: "ed")])
            //            self.RegisterPro.BranchTableView.reloadData()
        }
        
    }
    
    
}
extension AddBranchVc: UITableViewDataSource, UITableViewDelegate{
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return newBranches.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "BranchCell") as? BranchCell
        {
            let Call = newBranches[indexPath.row]
            cell.updateView(BranchDataItem: Call)
            cell.regionBtn.isHidden = true
            
            cell.cityButon = {
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.alignment = .center
                
                let pick2 = ActionSheetStringPicker(title: nil, rows: self.citiesPickerName, initialSelection: 0, doneBlock: {
                    picker, indexes, values in
                    
                    let chosenName = values as? String
                    
                    for i in 0..<self.citiesPickerName.count {
                        if chosenName == self.citiesPickerName[i] {
                            cell.chosenCityID = self.citiesPickerId[i]
                        }
                    }
                    cell.regionBtn.isHidden = false
                    //                    print("idddd \(Call.cityId)")
                    
                    cell.TheCityTf.text = values as? String
                    
                    return
                }, cancel: { ActionMultipleStringCancelBlock in return },origin: cell.cityBtn)
                
                let bar1 = UIBarButtonItem.init(title: "Done".localized(), style: .plain, target: self, action: #selector(self.actionPickerDone))
                let bar2 = UIBarButtonItem.init(title: "Cancel".localized(), style: .plain, target: self, action: #selector(self.actionPickerCancel))
                pick2?.setDoneButton(bar1)
                pick2?.setCancelButton(bar2)
                pick2?.show()
                
            }
            cell.regionButton = {
                
                self.startAnimating()
                
                API.POST(url: URLs.GetRegion, parameters: ["city_id": cell.chosenCityID!,"lang": Language.currentLanguage()], headers: nil, completion: {
                    (success, value) in
                    
                    self.stopAnimating()
                    if success {
                        
                        if let returnKey = value["key"] as? Int {
                            if returnKey == 1 {
                                let Regions = value["Region"] as? [[String: Any]]
                                self.regionPickerName = []
                                self.regionPickerId = []
                                
                                
                                for i in Regions! {
                                    self.regionPickerName.append((i["name"] as? String)!)
                                    self.regionPickerId.append((i["Id"] as? Int)!)
                                    
                                }
                                let paragraphStyle = NSMutableParagraphStyle()
                                paragraphStyle.alignment = .center
                                
                                let pick = ActionSheetStringPicker(title: nil, rows: self.regionPickerName, initialSelection: 0, doneBlock: {
                                    picker, indexes, values in
                                    
                                    let chosenName = values as? String
                                    
                                    for i in 0..<self.regionPickerName.count {
                                        if chosenName == self.regionPickerName[i] {
                                            Call.regionId = self.regionPickerId[i]
                                        }
                                    }
                                    
                                    print("idddd \(Call.regionId)")
                                    
                                    cell.RegionTf.text = values as? String
                                    
                                    return
                                }, cancel: { ActionMultipleStringCancelBlock in return },origin: cell.cityBtn)
                                
                                let bar1 = UIBarButtonItem.init(title: "Done".localized(), style: .plain, target: self, action: #selector(self.actionPickerDone))
                                let bar2 = UIBarButtonItem.init(title: "Cancel".localized(), style: .plain, target: self, action: #selector(self.actionPickerCancel))
                                pick?.setDoneButton(bar1)
                                pick?.setCancelButton(bar2)
                                pick?.show()
                                
                            }
                        }
                    } else {
                        
                    }
                })
                
                
            }
            return cell
        }
        else
        {
            return BranchCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 250
    }
    
    @objc func actionPickerCancel(){
        
    }
    
    @objc func actionPickerDone(){
        
    }
    
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //        let vc = Storyboard.Home.instantiate(EmplyeeDetailsVc.self)
    //        let order_id = AdsItem[indexPath.row].order_id
    //        vc.order_id = order_id
    //        present(vc,animated: true, completion: nil)
    //
    //    }
 
    
    func convertToDeict(item: ListModel) -> [String: Any] {
        return ["city_id": item.city_Id!, "region": item.region!, "street": item.street!, "work_hours": item.work_Hours!]
    }
    
    func convertToDeictitem(item: BrachDataModel) -> [String: Any] {
        return ["city_id": item.cityId!, "region": item.regionId!, "street": item.Street!, "work_hours": item.Hour!]
    }
}
