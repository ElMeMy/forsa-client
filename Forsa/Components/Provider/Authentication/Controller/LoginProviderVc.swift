//
//  LoginVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/17/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class LoginProviderVc: UIViewController {

    public var loginProviderview: LoginProviderView! {
        guard isViewLoaded else { return nil }
        return (view as! LoginProviderView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginProviderview.BgImg.image = #imageLiteral(resourceName: "bg")
        loginProviderview.LogoImg.image = #imageLiteral(resourceName: "logo_login")
    }
    

    @IBAction func BackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func LoginPressed(_ sender: Any) {
        LoginProvider()
    }
    @IBAction func ForGetPassword(_ sender: Any) {
        let vc = Storyboard.Authentication.instantiate(ForgetProviderVc.self)
        present(vc,animated: true,completion: nil)
    }
    
    @IBAction func RegisterPressed(_ sender: Any) {
        let vc = Storyboard.Authentication.instantiate(RegisterProviderVc.self)
        present(vc,animated: true,completion: nil)
    }
    
}
