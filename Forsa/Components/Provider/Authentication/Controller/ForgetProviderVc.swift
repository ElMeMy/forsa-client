//
//  ForgetProviderVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/25/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class ForgetProviderVc: UIViewController {

    public var forgetView: ForGetView! {
        guard isViewLoaded else { return nil }
        return (view as! ForGetView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            forgetView.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)

        }
        else
        {
            forgetView.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        forgetView.BgImg.image = #imageLiteral(resourceName: "bg")
        forgetView.LogoImg.image = #imageLiteral(resourceName: "logo_splash")
    }
    @IBAction func SendCodePreesed(_ sender: Any) {
        ForgetProvider()
    }
    
    
    @IBAction func BackPreesed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
