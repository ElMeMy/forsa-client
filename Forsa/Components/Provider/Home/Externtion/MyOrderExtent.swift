//
//  MyOrderExtent.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/30/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

extension MyOdersVc: UITableViewDataSource, UITableViewDelegate{
    
    func setUpMyOrders(){
        Orders.OrdersTableView.delegate = self
        Orders.OrdersTableView.dataSource = self
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return OrderItem.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
            if let cell = tableView.dequeueReusableCell(withIdentifier: "OrdersCell") as? OrdersCell
            {
                
                let Call   = OrderItem[indexPath.row]
                let Status = OrderItem[indexPath.row].stutes_no
                if Status == 1
                {
                    cell.OrderStatus.image = #imageLiteral(resourceName: "check-box")
                }
                else if Status == 2
                {
                    cell.OrderStatus.image = #imageLiteral(resourceName: "clock")
                }
                else 
                {
                    cell.OrderStatus.image = #imageLiteral(resourceName: "multiply")
                }

                cell.updateView(OrderItem: Call)
                return cell
            }
            else
            {
                return OrdersCell()
            }
            
        }
            
    
        
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170.0
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let Status = OrderItem[indexPath.row].stutes_no

        if  Status == 1
        {
            let vc = Storyboard.Home.instantiate(EmployeeDetailsVc.self)
            vc.order_id = OrderItem[indexPath.row].job_id
            present(vc,animated: true, completion: nil)
        }
        else if  Status == 2
        {
            let vc = Storyboard.Home.instantiate(EmployeeDetailsVc.self)
            vc.order_id = OrderItem[indexPath.row].job_id
            present(vc,animated: true, completion: nil)
        }
        else
        {
            let vc = Storyboard.Home.instantiate(EmployeeDetailsWithReasonVc.self)
            vc.order_id = OrderItem[indexPath.row].job_id
            present(vc,animated: true, completion: nil)
        }
        
    }
    

        
        
    }
    


