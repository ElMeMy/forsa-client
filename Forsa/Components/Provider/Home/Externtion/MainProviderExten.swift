//
//  MainProviderExten.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/26/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit


extension MainProviderVc: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1
        {
            return City.count
        }
        else if pickerView.tag == 2
        {
            return Region.count
        }
        else if pickerView.tag == 3
        {
            return Jobs.count
        }
        else if pickerView.tag == 4
        {
            return Gender.count
        }
        else if pickerView.tag == 5
        {
            return Moahel.count
        }
        
        return 1
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 1
        {
            return City[row].name
        }
        else if pickerView.tag == 2
        {
            return Region[row].name
        }
        else if pickerView.tag == 3
        {
            return Jobs[row].name
        }
        else if pickerView.tag == 4
        {
            return Gender[row].name
        }
        else if pickerView.tag == 5
        {
            return Moahel[row].name
        }
        
        return ""
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1
        {
            self.MainProvider.CityTf.text = self.City[row].name
            city_id = City[row].id!
            getRegion()
            
        }
        else if pickerView.tag == 2
        {
            self.MainProvider.RegionTf.text = self.Region[row].name
            Region_id = Region[row].id!
        }
        
        if pickerView.tag == 3
        {
            self.MainProvider.JopTitle.text = self.Jobs[row].name
            Job_id = Jobs[row].id!
        }
        else if pickerView.tag == 4
        {
            self.MainProvider.GenderTitle.text = self.Gender[row].name
            Gender_id = Gender[row].id!
            
        }
        else if pickerView.tag == 5
        {
            self.MainProvider.QualificationTextField.text = self.Moahel[row].name
            Moahed_id = Moahel[row].id!
            
        }
        
        
    }
    
}
