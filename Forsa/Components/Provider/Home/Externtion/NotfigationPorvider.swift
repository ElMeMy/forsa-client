//
//  NotfigationPorvider.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/27/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

extension NotfigationProviderVc {
    func NatLayer()
    {
        let user_id = UserDefaults.standard.value(forKey: "Id") as? Int ?? 0
        
        let parameters = [
            "provider_id"    : user_id,
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            ] as [String : Any]
         startAnimating()
        API.POST(url: URLs.GetNotifactionProvider, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    break
                case 1:
                    if let data = dict["notify"] as? [[String: Any]]{
                        print("notify",data)
                        self.NatItem=[]
                        //get data from JSON
                        for i in data{
                            let Id          = i["Id"] as? Int ?? 0
                            let type        = i["type"] as? Int ?? 0
                            let user_name   = i["user_name"] as? String ?? ""
                            let fk_order    = i["fk_order"] as? Int ?? 0
                            let name        = i["name"] as? String ?? ""
                            let reason      = i["reason"] as? String ?? ""
                            let fk_job      = i["fk_job"] as? Int ?? 0
                            let provider_id = i["clientid"] as! Int

                            
                            self.NatItem.append(NotfigationModel(Id: Id, type: type, user_name: user_name, fk_order: fk_order, name: name + user_name, reason: reason, fk_job: fk_job, provider_id: provider_id))
                            
                            
                        }
                        
                        self.natView.NatTableView.reloadData()
                        
                        
                        
                        
                        
                        
                    }else{
                        self.stopAnimating()
                        
                        //weak internet
                        let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                    
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
}
