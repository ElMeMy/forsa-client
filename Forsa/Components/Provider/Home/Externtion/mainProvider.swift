//
//  mainProvider.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/26/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

extension MainProviderVc {
    
   
    
    
    func GetCity() {
        let body = ["lang" : Language.currentLanguage().contains("en") ? "en" : "ar",]
        
        startAnimating()
        API.POST(url: URLs.GetCities, parameters: body, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                let dict = value
                
                if let v = dict["key"] as? Int{
                    
                    switch v {
                    case  0:
                        
                        let msg = dict["msg"]
                        
                        let alert = UIAlertController(title: "error".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    case 1:
                        
                        if let City = dict["City"] as? [[String: Any]]{
                            
                            for a in City{
                                let id = a["Id"] as! Int
                                let name = a["name"] as! String
                                self.City.append(CityModel(id: id, name: name))
                                
                            }
                            
                            
                        }else{
                            self.stopAnimating()
                            
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                        
                        
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    
    
    func getRegion() {
        let body = ["lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
                    "city_id" : city_id!
            ] as [String : Any]
        
        startAnimating()
        API.POST(url: URLs.GetRegion, parameters: body, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                let dict = value
                
                if let v = dict["key"] as? Int{
                    
                    switch v {
                    case  0:
                        
                        let msg = dict["msg"]
                        
                        let alert = UIAlertController(title: "error".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    case 1:
                        
                        if let Region = dict["Region"] as? [[String: Any]]{
                            
                            
                            if self.emptyCity{
                                self.MainProvider.CityTf.isUserInteractionEnabled = true
                                self.MainProvider.CityTf.inputView = self.pickerViewCity
                            }
                            
                            self.Region = []
                            for a in Region{
                                let id = a["Id"] as! Int
                                let name = a["name"] as! String
                                self.Region.append(RegionModel(id: id, name: name))
                                
                            }
                            
                            
                        }else{
                            self.stopAnimating()
                            
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                        
                        
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    
    func GetJob() {
        let body = ["lang" : Language.currentLanguage().contains("en") ? "en" : "ar",]
        
        startAnimating()
        API.POST(url: URLs.GetJob_title, parameters: body, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                let dict = value
                
                if let v = dict["key"] as? Int{
                    
                    switch v {
                    case  0:
                        
                        let msg = dict["msg"]
                        
                        let alert = UIAlertController(title: "error".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    case 1:
                        
                        if let jobs = dict["Job_title"] as? [[String: Any]]{
                            
                            for a in jobs{
                                let id = a["Id"] as! Int
                                let name = a["name"] as! String
                                self.Jobs.append(JobModel(id: id, name: name))
                                
                            }
                            
                            
                        }else{
                            self.stopAnimating()
                            
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                        
                        
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    
}




