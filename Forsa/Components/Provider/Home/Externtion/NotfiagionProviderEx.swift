//
//  NotfiagionProvider.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/27/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

extension NotfigationProviderVc: UITableViewDataSource, UITableViewDelegate{
    
    func setUpNatfigationProvider(){
        natView.NatTableView.delegate = self
        natView.NatTableView.dataSource = self
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return NatItem.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  NatItem[indexPath.row].type == 2
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "NatCell") as? NatCell
            {
                
                let Call = NatItem[indexPath.row]
                cell.updateView(NatItem: Call)
                return cell
            }
            else
            {
                return NatCell()
            }
            
        }
        else
        {
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "NatRefusedCell") as? NatRefusedCell
            {
                
                let Call = NatItem[indexPath.row]
                cell.updateView(NatItem: Call)
                return cell
            }
            else
            {
                return AdsCell()
            }
            
        }

        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if  NatItem[indexPath.row].type == 2
            {
                let vc = Storyboard.Home.instantiate(ReasonRejectionVc.self)
                vc.Reasons = NatItem[indexPath.row].reason
                present(vc,animated: true, completion: nil)
            }
            else
            {
                let type = NatItem[indexPath.row].type
                let vc = Storyboard.Home.instantiate(EmployeeDetailsVc.self)
                vc.order_id = NatItem[indexPath.row].fk_job
                vc.type = type
                present(vc,animated: true, completion: nil)
            }
    
        }
    
}
