//
//  OrderModel.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/30/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import Foundation

struct OrderModel {
    
    public  var order_id : Int
    public  var job_id : Int
    public  var name : String
    public  var user_name : String
    public  var stutes : String
    public  var stutes_no : Int
    public  var city : String
    public  var Region : String
    public  var Job_title : String

    
    init(order_id:Int,job_id:Int,name:String,user_name:String,stutes:String,stutes_no:Int,city:String,Region:String,Job_title:String) {
        
        self.order_id = order_id
        self.job_id = job_id
        self.name = name
        self.user_name = user_name
        self.stutes = stutes
        self.stutes_no = stutes_no
        self.city = city
        self.Region = Region
        self.Job_title = Job_title
    }
    
    
}
