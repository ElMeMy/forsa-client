//
//  EmployeeDetailsReasonView.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/30/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class EmployeeDetailsReasonView: UIView {
  
        
        @IBOutlet weak var BackButton: UIButton!
        @IBOutlet weak var ProfileImage: CircleImageView!
        @IBOutlet weak var NameLabel: UILabel!
        @IBOutlet weak var HourLabel: UILabel!
        @IBOutlet weak var AboutLabel: UITextView!
        @IBOutlet weak var AgeLabel: UILabel!
        @IBOutlet weak var CityLabel: UILabel!
        @IBOutlet weak var RegionLabel: UILabel!
        @IBOutlet weak var QualificationLabel: UILabel!
        @IBOutlet weak var ExperiecneLabel: UILabel!
        @IBOutlet weak var SkillLabel: UILabel!
        @IBOutlet weak var NumHourLabel: UILabel!
       @IBOutlet weak var RefusedText: UITextView!
    

}
