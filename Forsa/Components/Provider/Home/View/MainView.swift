//
//  MainView.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/17/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class MainView: UIView {
    @IBOutlet weak var NatButton: UIButton!
    @IBOutlet weak var SideMenuButton: UIButton!
    @IBOutlet weak var AdsTableView: UITableView!
    @IBOutlet weak var BoxImage: UIImageView!
    @IBOutlet weak var NoOrderLabel: UILabel!
    
}
