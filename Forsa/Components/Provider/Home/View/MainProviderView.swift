//
//  MainProviderView.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/25/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class MainProviderView: UIView {

    @IBOutlet weak var NatButton: UIButton!
    @IBOutlet weak var SideMenuButton: UIButton!
    @IBOutlet weak var LogoImage: UIImageView!
    @IBOutlet weak var Down1image: UIImageView!
    @IBOutlet weak var Down2image: UIImageView!
    @IBOutlet weak var Down3image: UIImageView!
    @IBOutlet weak var Down4image: UIImageView!
    @IBOutlet weak var Down5image: UIImageView!
    @IBOutlet weak var CityTf: TextFieldRadius!
    @IBOutlet weak var RegionTf: TextFieldRadius!
    @IBOutlet weak var JopTitle: TextFieldRadius!
    @IBOutlet weak var GenderTitle: TextFieldRadius!
    @IBOutlet weak var QualificationTextField: TextFieldRadius!

}
