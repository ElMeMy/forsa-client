//
//  OrdersCell.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/30/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class OrdersCell: UITableViewCell {

    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var stutesLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var RegionLabel: UILabel!
    @IBOutlet weak var Job_titleLabel: UILabel!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var OrderStatus: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.outerView.clipsToBounds = true
        self.outerView.layer.cornerRadius = 15
        outerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
    }
    
    func updateView(OrderItem: OrderModel) {
        NameLabel.text          = OrderItem.name
        stutesLabel.text        = OrderItem.stutes
        cityLabel.text          = OrderItem.city
        RegionLabel.text        = OrderItem.Region
        Job_titleLabel.text     = OrderItem.Job_title
    }

}
