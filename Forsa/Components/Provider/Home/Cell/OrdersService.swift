//
//  OrdersService.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/30/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

extension MyOdersVc {
    func OrderService()
    {
        let user_id = UserDefaults.standard.value(forKey: "Id") as? Int ?? 0
        
        let parameters = [
            "provider_id"    : user_id,
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            ] as [String : Any]
        
        startAnimating()
        API.POST(url: URLs.GetOrders, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    break
                case 1:
                    if let data = dict["result"] as? [[String: Any]]{
                        self.OrderItem=[]
                        //get data from JSON
                        for i in data{
                            let order_id    = i["order_id"] as? Int ?? 0
                            let job_id      = i["job_id"] as? Int ?? 0
                            let name        = i["name"] as? String ?? ""
                            let user_name   = i["user_name"] as? String ?? ""
                            let stutes      = i["stutes"] as? String ?? ""
                            let stutes_no   = i["stutes_no"] as? Int ?? 0
                            let city        = i["city"] as? String ?? ""
                            let Region      = i["Region"] as? String ?? ""
                            let Job_title   = i["Job_title"] as? String ?? ""

                            
                            self.OrderItem.append(OrderModel(order_id: order_id, job_id: job_id, name: name, user_name: user_name, stutes: stutes, stutes_no: stutes_no, city: city, Region: Region, Job_title: Job_title))
                            
                            
                        }
                        
                        self.Orders.OrdersTableView.reloadData()
                        
                        
                        if self.OrderItem.count <= 0
                        {
                            self.Orders.ImageStatus.isHidden = false
                        } 
                        else
                        {
                            self.Orders.ImageStatus.isHidden = true
                        }
                        
                        
                        
                    }else{
                        self.stopAnimating()
                        
                        //weak internet
                        let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                    
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
}
