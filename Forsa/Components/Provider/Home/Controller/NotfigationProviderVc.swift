//
//  NotfigationProviderVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/27/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class NotfigationProviderVc: UIViewController {

    
    var NatItem = [NotfigationModel]()
    
    public var natView: NotfigationView! {
        guard isViewLoaded else { return nil }
        return (view as! NotfigationView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            natView.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)
            
        }
        else
        {
            natView.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNatfigationProvider()
        NatLayer()
    }
    
    @IBAction func BackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
