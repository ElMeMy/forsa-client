//
//  ResultSearch.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/26/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

extension ResultSearchVc {
    func ResultSearch()
    {
        
        let parameters = [
            "job_title_id"    : Job_id!,
            "city_id"         : city_id!,
            "region_id"       : Region_id!,
            "Gender"          : Gender_id ?? 0,
            "qualification"   : Moahed_id ?? 0,
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            ] as [String : Any]
        

        startAnimating()
        API.POST(url: URLs.Search, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    break
                case 1:
                    if let data = dict["result"] as? [[String: Any]]{
                        print("result",data)
                        self.AdsItem=[]
                        //get data from JSON
                        for i in data{
                            let order_id          = i["Id"] as? Int ?? 0
                            let name              = i["name"] as? String ?? ""
                            let user_name         = i["user_name"] as? String ?? ""
                            let img               = i["img"] as? String ?? ""
                            let expected_salary   = i["expected_salary"] as? String ?? ""
                            let about             = i["about"] as? String ?? ""
                         
                            
                            self.AdsItem.append(AdsModel(order_id: order_id, job_id: 0, name: name, user_name: user_name, img: img, expected_salary: expected_salary, about: about, fk_provider: 0, stutes: "", stutes_no: 0))
                            
                            
                        }
                        
                        self.mainview.AdsTableView.reloadData()
                        
                        
                        if self.AdsItem.count == 0
                        {
                            self.mainview.BoxImage.isHidden = false
                            self.mainview.NoOrderLabel.isHidden = false
                        }
                        else
                        {
                            self.mainview.BoxImage.isHidden = true
                            self.mainview.NoOrderLabel.isHidden = true
                        }
                        
                        
                        
                    }else{
                        self.stopAnimating()
                        
                        //weak internet
                        let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                    
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
}
