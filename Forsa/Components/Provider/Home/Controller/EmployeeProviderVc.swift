//
//  EmployeeDetailsVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/26/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class EmployeeProviderVc: UIViewController {

    
    
    var order_id: Int?
    var DayItem = [Daymodel]()

    var phone: String?

    public var Employee: EmployeeProviderView! {
        guard isViewLoaded else { return nil }
        return (view as! EmployeeProviderView)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            Employee.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)

        }
        else
        {
            Employee.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        
        
        
        Employee.AvaliableDayCollectionView.delegate = self
        Employee.AvaliableDayCollectionView.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        EmplyeeDetailsService()
    }
    
    @IBAction func BackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func MakeOrderPressed(_ sender: Any) {
        MakeOder()
    }
    
    
    

}



extension EmployeeProviderVc: UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DayItem.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AvaliableCell", for: indexPath) as? AvaliableCell else {return  UICollectionViewCell()}
        let Day = DayItem[indexPath.row]
        cell.configureCell(DayItem: Day)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.Employee.AvaliableDayCollectionView.bounds.width/3) - 10.0, height: 100.0)
    }
    
    
    
    
    
  
}
