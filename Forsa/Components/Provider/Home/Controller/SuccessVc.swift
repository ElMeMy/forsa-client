//
//  SuccessVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/26/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class SuccessVc: UIViewController {


    public var Success: SuccessView! {
        guard isViewLoaded else { return nil }
        return (view as! SuccessView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Success.BackGroundImage.image = #imageLiteral(resourceName: "bg")
        Success.Imagelogo.image = #imageLiteral(resourceName: "vvvvector")
    }
    

   
    @IBAction func GoHomePressed(_ sender: Any) {
        let vc2 = Storyboard.Home.instantiate(MainProviderVc.self)
        let right = Storyboard.Home.instantiate(SideMenuVC.self)
        let sideMenu = SlideMenuController(mainViewController: vc2, rightMenuViewController: right)
        self.view.window?.rootViewController = sideMenu
    }
    
}
