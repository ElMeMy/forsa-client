//
//  SideMenuVC.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/18/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class SideMenuVC: UIViewController {
    
    @IBOutlet weak var profileImageView: CircleImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var sideMenuTable: UITableView!
    
    
    
    var sideMenuDataSource = [SideMenuItem]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let type_user = UserDefaults.standard.value(forKey: "type_user") as? String
        
        if  type_user == "client"
        {
            
            sideMenuDataSource = [
                SideMenuItem(title: "My account" .localized(), img: #imageLiteral(resourceName: "home"), active: false),
                SideMenuItem(title: "Make Ads".localized(), img: #imageLiteral(resourceName: "home"), active: false),
                SideMenuItem(title: "My Ads".localized(), img: #imageLiteral(resourceName: "hiring"), active: false),
                SideMenuItem(title: "call us".localized(), img: #imageLiteral(resourceName: "telephone"), active: false),
                SideMenuItem(title: "Terms and Conditions".localized(), img: #imageLiteral(resourceName: "policy"), active: false),
                SideMenuItem(title: "About Us".localized(), img: #imageLiteral(resourceName: "care"), active: false),
                SideMenuItem(title: "Log out".localized(), img: #imageLiteral(resourceName: "logout"), active: false),
            ]
            
        }
        else
        {
            sideMenuDataSource = [
                SideMenuItem(title: "My Orders".localized(), img: #imageLiteral(resourceName: "hiring"), active: false),
                SideMenuItem(title: "My account".localized(), img: #imageLiteral(resourceName: "user-male-black-shape"), active: false),
                SideMenuItem(title: "call us".localized(), img: #imageLiteral(resourceName: "telephone"), active: false),
                SideMenuItem(title: "Terms and Conditions".localized(), img: #imageLiteral(resourceName: "policy"), active: false),
                SideMenuItem(title: "About Us".localized(), img: #imageLiteral(resourceName: "care"), active: false),
                SideMenuItem(title: "Log out".localized(), img: #imageLiteral(resourceName: "logout"), active: false),
            ]
        }
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        let avatar = UserDefaults.standard.value(forKey: "img") as? String ?? ""
        profileImageView.setImageWith(avatar)
        
        
        let name = UserDefaults.standard.value(forKey: "name") as? String ?? ""
        userNameLabel.text = name
        
    }
    
    
    
    
    
    
}


extension SideMenuVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as? SideMenuCell else {return UITableViewCell()}
        
        cell.selectionStyle = .none
        cell.layer.masksToBounds = true
        cell.layer.cornerRadius = 5
        cell.layer.borderWidth = 0.1
        cell.layer.shadowOffset = CGSize(width: -1, height: 1)
        let item = sideMenuDataSource[indexPath.row]
        cell.configCell(item: item as! SideMenuItem)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = indexPath.row
        
        
        let type_user = UserDefaults.standard.value(forKey: "type_user") as? String
        
        if  type_user == "client"
        {
            switch item {
                
            case 0:
                let vc = Storyboard.Authentication.instantiate(ProfileVc.self)
                present(vc,animated: true,completion: nil)
            case 1:
                let vc = Storyboard.Home.instantiate(MainClientVc.self)
                present(vc,animated: true,completion: nil)
            case 2:
                let vc = Storyboard.Home.instantiate(MyOrderClientVC.self)
                present(vc,animated: true,completion: nil)
            case 3:
                let vc = Storyboard.Home.instantiate(CallUsVC.self)
                present(vc,animated: true,completion: nil)
                
            case 4:
                let vc = Storyboard.Home.instantiate(TermsVc.self)
                present(vc,animated: true,completion: nil)
            case 5:
                
                let vc = Storyboard.Home.instantiate(AboutUsVc.self)
                present(vc,animated: true,completion: nil)
                
            case 6:
                let vc = Storyboard.Home.instantiate(LogOutVc.self)
                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                present(vc,animated: true, completion: nil)
            default:
                break
            }
            
        }
        else
        {
            switch item {
                
            case 0:
                let vc = Storyboard.Home.instantiate(MyOdersVc.self)
                present(vc,animated: true,completion: nil)
                
            case 1:
                let vc = Storyboard.Authentication.instantiate(EditProviderVc.self)
                present(vc,animated: true,completion: nil)
            case 2:
                let vc = Storyboard.Home.instantiate(CallUsVC.self)
                present(vc,animated: true,completion: nil)
            case 3:
                
                let vc = Storyboard.Home.instantiate(TermsVc.self)
                present(vc,animated: true,completion: nil)
            case 4:
                let vc = Storyboard.Home.instantiate(AboutUsVc.self)
                present(vc,animated: true,completion: nil)
            case 5:
                let vc = Storyboard.Home.instantiate(LogOutVc.self)
                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                present(vc,animated: true, completion: nil)
            default:
                break
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45.0
    }
    
    
    
    
}

