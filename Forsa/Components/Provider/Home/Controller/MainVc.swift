//
//  MainVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/17/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class MainVc: UIViewController {
    
    @IBOutlet weak var CountNat: UILabel!
    
    
    var AdsItem = [AdsModel]()
    
    public var mainModel: MainModel? {
        didSet {
            Home()
        }
    }
    
    public var mainview: MainView! {
        guard isViewLoaded else { return nil }
        return (view as! MainView)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainview.NatButton.setImage(#imageLiteral(resourceName: "notifications"), for: .normal)
        mainview.SideMenuButton.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
        mainview.BoxImage.image = #imageLiteral(resourceName: "box")
        
    }
    
    func GetCountNat() {
        let User_id = UserDefaults.standard.value(forKey: "Id") as? Int
        
        let body = ["client_id" : User_id]
        
        startAnimating()
        API.POST(url: URLs.GetNotifyByClient, parameters: body as [String : Any], headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                let dict = value
                
                if let v = dict["key"] as? Int{
                    
                    switch v {
                    case  0:
                        
                        break
                        
                    case 1:
                        
                        let CountNotify = dict["CountNotify"] as! Int
                        
                        
                        self.CountNat.text = "\(CountNotify)"
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Home()
        
        GetCountNat()
        setUp()
    }
    
    @IBAction func NatAction(_ sender: Any) {
        let vc = Storyboard.Home.instantiate(NotfigationVc.self)
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func SideMenu(_ sender: Any) {
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            self.slideMenuController()?.openLeft()
        }
        else
        {
            self.slideMenuController()?.openRight()
        }
    }
    
}
