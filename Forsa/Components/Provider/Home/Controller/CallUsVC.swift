//
//  CallUsVC.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/26/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class CallUsVC: UIViewController {

    var url : String?

    public var Call: CallUsView! {
        guard isViewLoaded else { return nil }
        return (view as! CallUsView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let lang = Language.currentLanguage()

        if lang == "en"
        {
            Call.BackOutlelt.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)

        }
        else
        {
            Call.BackOutlelt.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        
        Call.FaceOutltet.setImage(#imageLiteral(resourceName: "facebook"), for: .normal)
        Call.TwitterOutletl.setImage(#imageLiteral(resourceName: "twitter"), for: .normal)
        Call.LiknkedButton.setImage(#imageLiteral(resourceName: "linkedin"), for: .normal)
        Call.LogoImage.image = #imageLiteral(resourceName: "logo_login")
        
        GetSettings()

    }
    @IBAction func SendPreseed(_ sender: Any) {
        CallUs()
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func FaceBookAction(_ sender: Any) {
        FaceBook()
    }
    @IBAction func TwitterAction(_ sender: Any) {
       Twitter()
    }
    @IBAction func LinkedAction(_ sender: Any) {
        linkedIn()
    }

    

}
