//
//  MainProviderVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/25/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class MainProviderVc: UIViewController {

    
    @IBOutlet weak var CountNat: UILabel!

    var City      = [CityModel]()
    var Region    = [RegionModel]()
    var Jobs      = [JobModel]()
    var Gender    = [GenderModel]()
    var Moahel    = [MoahelModel]()

    
    let pickerViewCity   = UIPickerView()
    let pickerViewRegion = UIPickerView()
    let pickerViewJop    = UIPickerView()
    let pickerViewGender = UIPickerView()
    let pickerViewMoahel = UIPickerView()
    
    var city_id   : Int?
    var Region_id : Int?
    var Job_id    : Int?
    var Gender_id : String?
    var Moahed_id : String?
    var emptyCity = false
    
    public var MainProvider: MainProviderView! {
        guard isViewLoaded else { return nil }
        return (view as! MainProviderView)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        GetCountNat()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        MainProvider.NatButton.setImage(#imageLiteral(resourceName: "notifications"), for: .normal)
        MainProvider.SideMenuButton.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
        MainProvider.Down1image.image = #imageLiteral(resourceName: "down")
        MainProvider.Down2image.image = #imageLiteral(resourceName: "down")
        MainProvider.Down3image.image = #imageLiteral(resourceName: "down")
        MainProvider.Down4image.image = #imageLiteral(resourceName: "down")
        MainProvider.Down5image.image = #imageLiteral(resourceName: "down")
        MainProvider.LogoImage.image = #imageLiteral(resourceName: "vector")
        
        pickerViewCity.delegate = self
        MainProvider.CityTf.inputView = pickerViewCity
        pickerViewCity.tag = 1
        
        
        pickerViewRegion.delegate = self
        MainProvider.RegionTf.inputView = pickerViewRegion
        pickerViewRegion.tag = 2
        
        pickerViewJop.delegate = self
        MainProvider.JopTitle.inputView = pickerViewJop
        pickerViewJop.tag = 3
        
        
        
        pickerViewGender.delegate = self
        MainProvider.GenderTitle.inputView = pickerViewGender
        pickerViewGender.tag = 4
        
        
        pickerViewMoahel.delegate = self
        MainProvider.QualificationTextField.inputView = pickerViewMoahel
        pickerViewMoahel.tag = 5
        
        GetJob()
        GetCity()
        
        Moahel.append(MoahelModel(id: "" , name: "Choose qualification".localized()))
        Moahel.append(MoahelModel(id: "1", name: "secondary".localized()))
        Moahel.append(MoahelModel(id: "2", name: "Collectors".localized()))
        Moahel.append(MoahelModel(id: "3", name: "M.A.".localized()))
        
        
        self.Gender.append(GenderModel(id: "", name: "Choose Gender".localized()))
        self.Gender.append(GenderModel(id: "M", name: "Male".localized()))
        self.Gender.append(GenderModel(id: "F", name: "Female".localized()))
    }
    
    func GetCountNat() {
        let User_id = UserDefaults.standard.value(forKey: "Id") as? Int
        
        let body = ["provider_id" : User_id]
        
        startAnimating()
        API.POST(url: URLs.GetNotifyByprovider, parameters: body as [String : Any], headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                let dict = value
                
                if let v = dict["key"] as? Int{
                    
                    switch v {
                    case  0:
                        
                        break
                        
                    case 1:
                        
                        let CountNotify = dict["CountNotify"] as! Int
                        
                        
                        self.CountNat.text = "\(CountNotify)"
                    default : print(" ")
                    }
                }
                
            }
        }
    }

  

    @IBAction func NotfigationPressed(_ sender: Any) {
        let vc = Storyboard.Home.instantiate(NotfigationProviderVc.self)
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func SideMenuPressed(_ sender: Any) {
        let lang = Language.currentLanguage()
        if lang == "en"
            
        {
            self.slideMenuController()?.openLeft()
        }
        else
        {
            self.slideMenuController()?.openRight()
            
        }
        
    }
    @IBAction func ResultSearchPreesed(_ sender: Any) {
        guard let Jop = MainProvider.JopTitle.text , !(MainProvider.JopTitle.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "job Is Requried".localized())}

        guard let ciry = MainProvider.CityTf.text , !(MainProvider.CityTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "City Is Requried".localized())}

        guard let Region = MainProvider.RegionTf.text , !(MainProvider.RegionTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Region Is Requried".localized())}

        let vc = Storyboard.Home.instantiate(ResultSearchVc.self)
        vc.city_id   = city_id
        vc.Job_id    = Job_id
        vc.Region_id = Region_id
        vc.Gender_id = Gender_id
        vc.Moahed_id = Moahed_id
      

        self.present(vc, animated: true, completion: nil)
    }
    
}
