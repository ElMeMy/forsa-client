//
//  EmployeeDetailsVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/27/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class EmployeeDetailsVc: UIViewController {

    
    var order_id: Int?
    var type:Int?
    
    public var Employee: EmployeeProviderView! {
        guard isViewLoaded else { return nil }
        return (view as! EmployeeProviderView)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            Employee.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)

        }
        else
        {
            Employee.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        
        if type != 10
        {
            Employee.phoneCountLabel.isHidden = true
            Employee.phoneLabel.isHidden = true
        }
        else
        {
            Employee.phoneCountLabel.isHidden = false
            Employee.phoneLabel.isHidden = false
            
            
        }
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        EmplyeeDetailsService()
    }
    
    @IBAction func BackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
}
