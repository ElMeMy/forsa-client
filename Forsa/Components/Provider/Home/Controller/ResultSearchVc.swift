//
//  ResultSearchVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/26/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

class ResultSearchVc: UIViewController {
    
    @IBOutlet weak var CountNat: UILabel!

    var AdsItem = [AdsModel]()
    var city_id   : Int?
    var Region_id : Int?
    var Job_id    : Int?
  
    var Gender_id : String?
    var Moahed_id : String?
    
    public var mainModel: MainModel? {
        didSet {
            ResultSearch()
            
        }
    }
    
    public var mainview: MainView! {
        guard isViewLoaded else { return nil }
        return (view as! MainView)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainview.NatButton.setImage(#imageLiteral(resourceName: "notifications"), for: .normal)
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            mainview.SideMenuButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)

        }
        else
        {
            mainview.SideMenuButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        mainview.BoxImage.image = #imageLiteral(resourceName: "box")
        GetCountNat()
    }
    
    func GetCountNat() {
        let User_id = UserDefaults.standard.value(forKey: "Id") as? Int
        
        let body = ["provider_id" : User_id]
        
        startAnimating()
        API.POST(url: URLs.GetNotifyByprovider, parameters: body as [String : Any], headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                let dict = value
                
                if let v = dict["key"] as? Int{
                    
                    switch v {
                    case  0:
                        
                        break
                        
                    case 1:
                        
                        let CountNotify = dict["CountNotify"] as! Int
                        
                        
                        self.CountNat.text = "\(CountNotify)"
                    default : print(" ")
                    }
                }
                
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        ResultSearch()
        setUpEmployee()
    }
    
    @IBAction func NatAction(_ sender: Any) {
        let vc = Storyboard.Home.instantiate(NotfigationProviderVc.self)
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func BackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
