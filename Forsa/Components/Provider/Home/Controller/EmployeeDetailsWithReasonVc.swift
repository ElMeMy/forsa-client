//
//  EmployeeDetailsWithReasonVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/30/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class EmployeeDetailsWithReasonVc: UIViewController {

    
    var order_id: Int?
    
    
    public var Employee: EmployeeDetailsReasonView! {
        guard isViewLoaded else { return nil }
        return (view as! EmployeeDetailsReasonView)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let lang = Language.currentLanguage()
        if lang == "en"
        {
            Employee.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)
            
        }
        else
        {
            Employee.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        EmplyeeDetailsRefused()
    }
    
    @IBAction func BackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
