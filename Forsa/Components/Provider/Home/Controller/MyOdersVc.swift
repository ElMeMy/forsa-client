//
//  MyOdersVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/30/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class MyOdersVc: UIViewController {

    var OrderItem = [OrderModel]()

    public var Orders: OrdersView! {
        guard isViewLoaded else { return nil }
        return (view as! OrdersView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let lang = Language.currentLanguage()

        if lang == "en"
        {
            Orders.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)

        }
        else
        {
            Orders.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_left-1"), for: .normal)
        }
        Orders.ImageStatus.image = #imageLiteral(resourceName: "box")
        Orders.ImageStatus.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpMyOrders()
        OrderService()

    }
    
    @IBAction func BackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
