//
//  ReasonRejectionVc.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/27/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class ReasonRejectionVc: UIViewController {

    var Reasons:String?
    public var Reason: TermsView! {
        guard isViewLoaded else { return nil }
        return (view as! TermsView)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        Reason.Text.text = Reasons!
        Reason.BackButton.setImage(#imageLiteral(resourceName: "ic_chevron_right"), for: .normal)
    }
    
    @IBAction func BackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
