//
//  EmployeeDetails.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/26/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

extension EmployeeProviderVc {
    
    func EmplyeeDetailsService()
    {
        
        let parameters = [
            "id_job"      : order_id!,
            "provider_id" : "0",
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            ] as [String : Any]
        
        startAnimating()
        API.POST(url: URLs.AdvertsmentDetails, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    break
                case 1:
                    if let data = dict["result"]
                    {
                        print("result",data)
                        
                        
                        let name        = data["name"] as? String ?? ""
                        self.Employee.NameLabel.text = name
                        
                        let img        = data["img"] as? String ?? ""
                        let url = URL(string: img ?? "")
                        self.Employee.ProfileImage.kf.setImage(with: url)
                        
                        let HourLabel   = data["hours_no"] as? String ?? ""
                        self.Employee.HourLabel.text = HourLabel
                        
                        
                        let Price   = data["expected_salary"] as? String ?? ""
                        self.Employee.Price.text = Price
                        
                        let AboutLabel   = data["about"] as? String ?? ""
                        self.Employee.AboutLabel.text = AboutLabel
                        
                        let AgeLabel    = data["age"] as? Int ?? 0
                        self.Employee.AgeLabel.text = "\(AgeLabel)"
                        
                        let CityLabel    = data["city"] as? String ?? ""
                        self.Employee.CityLabel.text = CityLabel
                        
                        let RegionLabel  = data["Region"] as? String ?? ""
                        self.Employee.RegionLabel.text = RegionLabel
                        
                        let QualificationLabel  = data["qualification"] as? String ?? ""
                        self.Employee.QualificationLabel.text = QualificationLabel
                        
                        
                        let ExperiecneLabel  = data["previous_experience"] as? String ?? ""
                        self.Employee.ExperiecneLabel.text = ExperiecneLabel
                        
                        
                        let SkillLabel  = data["educational_courses"] as? String ?? ""
                        self.Employee.SkillLabel.text = SkillLabel
                        
                        
                        let NumHourLabel  = data["hours_no"] as? String ?? ""
                        self.Employee.NumHourLabel.text = NumHourLabel
                        
                        self.phone  = data["phone"] as? String ?? ""
                       
                        
                       let TotalDays  = data["number_days"] as? Int ?? 0
                        self.Employee.TotalDay.text = "\(TotalDays)"
                        
                        if let Avilable_dayss = dict["result"]!["Avilable_days"] as? [String]{
                            self.DayItem=[]
                         
                            for i in Avilable_dayss{
                            self.DayItem.append(Daymodel(id: 0, title: i as! String))
                            }
                            
                            self.Employee.AvaliableDayCollectionView.reloadData()
                        
                        
                   
                        
                        
                        }
                        
                    }else{
                        self.stopAnimating()
                        
                        //weak internet
                        let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                    
                    
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
                let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                    self.EmplyeeDetailsService()
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    
    func MakeOder()
    {
        let user_id = UserDefaults.standard.value(forKey: "Id") as? Int ?? 0

        let parameters = [
            "id_job"      : order_id!,
            "provider_id" : user_id,
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            ] as [String : Any]
        
        startAnimating()
        API.POST(url: URLs.AddOrder, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    if let msg = dict["msg"] as? String
                    {
                        
                        let alert = UIAlertController(title: "error".localized(), message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                case 1:
                    if let msg = dict["msg"] as? String
                    {
                        
                        let alert = UIAlertController(title: "Success".localized(), message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            let vc = Storyboard.Home.instantiate(SuccessVc.self)
                            self.present(vc, animated: true, completion: nil)
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        self.stopAnimating()
                        
                        //weak internet
                        let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                    
                    
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
}
