//
//  EditPasswordService.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/24/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

extension EditNewPasswordVc {
    
    func EditPassword()
    {
        
        guard let current_pass = editpassword.NewPasswordEdit.text , !(editpassword.NewPasswordEdit.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "New Password Is Requried".localized())}
        
        guard let OldPassword = editpassword.OldPasswordEdit.text , !(editpassword.OldPasswordEdit.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Confirm Password Is Requried".localized())}
        
        
        guard editpassword.OldPasswordEdit.text!.count >= 9  else {return Alert.showAlertOnVC(target: self, title: "error".localized(), message: "Old Password must be greater than 9 digits".localized())}

        guard editpassword.NewPasswordEdit.text!.count >= 9  else {return Alert.showAlertOnVC(target: self, title: "error".localized(), message: "New Password must be greater than 9 digits".localized())}


   
        let user_id = UserDefaults.standard.value(forKey: "Id") as? Int ?? 0

       
        let parameters = [
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            "last_pass" :OldPassword,
            "current_pass" :current_pass,
            "ID" :user_id,
            ] as [String : Any]
        
        
        startAnimating()
        
        API.POST(url: URLs.ChangePassword, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    let msg = dict["msg"]
                    
                    let alert = UIAlertController(title: "error".localized(), message: msg as? String, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                case 1:
                    self.dismiss(animated: true, completion: nil)
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
    
}
