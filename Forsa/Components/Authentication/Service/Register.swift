//
//  Register.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/23/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

extension RegisterClientVc {

    func Register()
    {
        
        guard let Name = registerView.NameTf.text , !(registerView.NameTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Name Is Requried".localized())}
        
//        guard let IdNumber = registerView.IdNumberTf.text , !(registerView.IdNumberTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Id Number Is Requried".localized())}
//
        
//        guard registerView.IdNumberTf.text!.count >= 10  else {return Alert.showAlertOnVC(target: self, title: "error".localized(), message: "Id Number  must be greater than 10 digits".localized())}
        
//        guard let Gender = registerView.GenderTf.text , !(registerView.GenderTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Gender Is Requried".localized())}

        
//        guard let Birthday = registerView.DateTf.text , !(registerView.DateTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "BirthDay Is Requried".localized())}

//          guard let Phone = registerView.PhoneTf.text , !(registerView.PhoneTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Phone Is Requried".localized())}
//        
//          guard registerView.PhoneTf.text!.count >= 10  else {return Alert.showAlertOnVC(target: self, title: "error".localized(), message: "Mobile number must be greater than 10 digits".localized())}
        
        guard let Email = registerView.EmailTf.text , !(registerView.EmailTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Email Is Requried".localized())}
        
        guard let UserName = registerView.UserNameTf.text , !(registerView.UserNameTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "UserName Is Requried".localized())}
        
        
         guard let Password = registerView.PasswordTf.text , !(registerView.PasswordTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Password Is Requried".localized())}
        
        
        guard registerView.PasswordTf.text!.count >= 10  else {return Alert.showAlertOnVC(target: self, title: "error".localized(), message: "Password must be greater than 9 digits".localized())}
        
        
      
        guard let City = registerView.CityTf.text , !(registerView.CityTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "City Is Requried".localized())}
        
        guard let Region = registerView.RegionTf.text , !(registerView.RegionTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Region Is Requried".localized())}
        
//        guard let Jop = registerView.JopTitle.text , !(registerView.JopTitle.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Jop Title Is Requried".localized())}
        
       
        
        guard let Qualification = registerView.QualificationTg.text , !(registerView.QualificationTg.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Qualification Is Requried".localized())}
        
        
      
        
//        guard let experience = registerView.experienceTf.text , !(registerView.experienceTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "experience Is Requried".localized())}
//
//
//
//
//        guard let courses = registerView.coursesTf.text , !(registerView.coursesTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "courses Is Requried".localized())}
//
      
        
        guard registerView.AcceptTerms.on == true else {
            Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Please accept the terms and conditions of the application".localized())
            return
        }


        
        
        let token = UserDefaults.standard.value(forKey: "mobileToken") as? String ?? "token"

        let parameters = [
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            "name" :Name,
            "id_number" :registerView.IdNumberTf.text ?? "",
            "user_name" :UserName,
            "phone" :registerView.PhoneTf.text ?? "",
            "Email" :Email,
            "educational_courses" :registerView.coursesTf.text,
            "previous_experience" :registerView.experienceTf.text,
            "qualification" :Moahed_id ?? 0,
            "fk_city" :city_id ?? 0,
            "fk_region" :Region_id ?? 0,
            "password" :Password,
            "Gender" : Gender_id ?? "",
            "birth_date" :registerView.DateTf.text ?? "",
            "token" :token,

            ] as [String : Any]
        
    
        startAnimating()
        
        API.POST(url: URLs.RegisterClient, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    let msg = dict["msg"]
                    
                    let alert = UIAlertController(title: "ERROR".localized(), message: msg as? String, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                case 1:
                    let msg = dict["msg"]
                    
                    
                        let vc = Storyboard.Authentication.instantiate(LoginClientVc.self)
                        self.present(vc, animated: true, completion: nil)
                  
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
    
    
    
    func GetCity() {
        let body = ["lang" : Language.currentLanguage().contains("en") ? "en" : "ar",]
        
        startAnimating()
        API.POST(url: URLs.GetCities, parameters: body, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                let dict = value
                
                if let v = dict["key"] as? Int{
                    
                    switch v {
                    case  0:
                        
                        let msg = dict["msg"]
                        
                        let alert = UIAlertController(title: "error".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    case 1:
                        
                        if let City = dict["City"] as? [[String: Any]]{
                            
                            for a in City{
                                let id = a["Id"] as! Int
                                let name = a["name"] as! String
                                self.City.append(CityModel(id: id, name: name))
                                
                            }
                            
                            
                        }else{
                            self.stopAnimating()
                            
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                        
                        
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    
    
    func getRegion() {
        let body = ["lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
                    "city_id" : city_id!
            ] as [String : Any]
        
        startAnimating()
        API.POST(url: URLs.GetRegion, parameters: body, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                let dict = value
                
                if let v = dict["key"] as? Int{
                    
                    switch v {
                    case  0:
                        
                        let msg = dict["msg"]
                        
                        let alert = UIAlertController(title: "error".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    case 1:
                        
                        if let Region = dict["Region"] as? [[String: Any]]{
                            
                            
                            if self.emptyCity{
                                self.registerView.CityTf.isUserInteractionEnabled = true
                                self.registerView.CityTf.inputView = self.pickerViewCity
                            }
                            
                            self.Region = []
                            for a in Region{
                                let id = a["Id"] as! Int
                                let name = a["name"] as! String
                                self.Region.append(RegionModel(id: id, name: name))
                                
                            }
                            
                            
                        }else{
                            self.stopAnimating()
                            
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                        
                        
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    
//    func GetJob() {
//        let body = ["lang" : Language.currentLanguage().contains("en") ? "en" : "ar",]
//        
//        startAnimating()
//        API.POST(url: URLs.GetJob_title, parameters: body, headers: nil) { (success, value) in
//            if success{
//                self.stopAnimating()
//                let dict = value
//                
//                if let v = dict["key"] as? Int{
//                    
//                    switch v {
//                    case  0:
//                        
//                        let msg = dict["msg"]
//                        
//                        let alert = UIAlertController(title: "error".localized(), message: msg as? String, preferredStyle: .alert)
//                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
//                            
//                        }))
//                        self.present(alert, animated: true, completion: nil)
//                        
//                    case 1:
//                        
//                        if let jobs = dict["Job_title"] as? [[String: Any]]{
//                            
//                            for a in jobs{
//                                let id = a["Id"] as! Int
//                                let name = a["name"] as! String
//                                self.Jobs.append(JobModel(id: id, name: name))
//                                
//                            }
//                            
//                            
//                        }else{
//                            self.stopAnimating()
//                            
//                            //weak internet
//                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
//                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
//                                
//                            }))
//                            self.present(alert, animated: true, completion: nil)
//                            
//                            
//                        }
//                        
//                        
//                        
//                    default : print(" ")
//                    }
//                }
//                
//            }
//        }
//    }
    
    func GetGender() {
        self.Gender.append(GenderModel(id: "" , name: "Choose Gender".localized()))
        self.Gender.append(GenderModel(id: "F", name: "Female".localized()))
        self.Gender.append(GenderModel(id: "M", name: "Male".localized()))
        
    }
}




