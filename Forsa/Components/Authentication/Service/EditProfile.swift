//
//  EditProfile.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/20/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

extension EditProfileVc {
    func GetProfile()
    {
        let user_id = UserDefaults.standard.value(forKey: "Id") as? Int ?? 0
        
        let parameters = [
            "client_id"    : user_id,
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            ] as [String : Any]
        
        startAnimating()
        API.POST(url: URLs.GetDataOfClient, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    break
                case 1:
                    if let data = dict["client"]{
                        
                        let user_name = data["user_name"] as! String
                        self.profileView.UserNameTf.text = user_name
                        
                        let name = data["name"] as! String
                        self.profileView.NameTf.text = name
                        
                        let phone = data["phone"] as! String
                        self.profileView.PhoneTf.text = phone
                        
                        let email = data["email"] as! String
                        self.profileView.EmailTf.text = email
                        
                        let city = data["city"] as! String
                        self.profileView.CityTf.text = city
                        
                        let Region = data["Region"] as! String
                        self.profileView.RegionTf.text = Region
                        
                        let id_number = data["id_number"] as! String
                        self.profileView.IdNumberTf.text = id_number

                        
                       
                        
                        let qualification = data["qualification"] as! String
                        self.profileView.QualificationTg.text = qualification
                        
                        self.qualification_id = data["qualification_id"] as? String ?? ""
                       
                        
                        let previous_experience = data["previous_experience"] as! String
                        self.profileView.experienceTf.text = previous_experience
                        
                        let educational_courses = data["educational_courses"] as! String
                        self.profileView.coursesTf.text = educational_courses
                        
                        let about = data["about"] as! String
                        self.profileView.AboutTf.text = about
                        
                        let avatar = data["img"] as! String
                        self.profileView.ProfileImage.setImageWith(avatar)
                        
                        
                        
                        
                        
                    }else{
                        self.stopAnimating()
                        
                        //weak internet
                        let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                    
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
    
    
    func UpdateUserData()
    {
        guard let Name = profileView.NameTf.text , !(profileView.NameTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Name Is Requried".localized())}
        
        guard let UserName = profileView.UserNameTf.text , !(profileView.UserNameTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Required".localized(), message: "UserName Is Requried".localized())}

        
        guard let Phone = profileView.PhoneTf.text , !(profileView.PhoneTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Required".localized(), message: "Phone Is Requried".localized())}

        
        guard profileView.PhoneTf.text!.count >= 10  else {return Alert.showAlertOnVC(target: self, title: "Error".localized(), message: "Mobile number must be greater than 10 digits".localized())}

        
        guard let Email = profileView.EmailTf.text , !(profileView.EmailTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Required".localized(), message: "Email Is Requried".localized())}

        
        guard let Qualification = profileView.QualificationTg.text , !(profileView.QualificationTg.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Required".localized(), message: "Qualification Is Requried".localized())}
        
        
        guard let City = profileView.CityTf.text , !(profileView.CityTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Required".localized(), message: "City Is Requried".localized())}
        
        guard let Region = profileView.RegionTf.text , !(profileView.RegionTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Required".localized(), message: "Region Is Requried".localized())}
        
//        guard let Jop = profileView.JopTitle.text , !(profileView.JopTitle.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Required".localized(), message: "Jop Title Is Requried".localized())}

        
        guard let IdNumber = profileView.IdNumberTf.text , !(profileView.IdNumberTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Required".localized(), message: "Id Number Is Requried".localized())}



        
        guard let courses = profileView.coursesTf.text , !(profileView.coursesTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Required".localized(), message: "courses Is Requried".localized())}

        
        guard let experience = profileView.experienceTf.text , !(profileView.experienceTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Required".localized(), message: "experience Is Requried".localized())}
        
        guard let About = profileView.AboutTf.text , !(profileView.AboutTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Required".localized(), message: "About Is Requried".localized())}


//        guard let Password = profileView.PasswordTf.text , !(profileView.PasswordTf.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Required", message: "Password Is Requried")}



        let user_id = UserDefaults.standard.value(forKey: "Id") as? Int ?? 0

        
        let parameters = [
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            "id" :user_id,
            "name" :Name,
            "id_number" :IdNumber,
            "user_name" :UserName,
            "phone" :Phone,
            "Email" :Email,
            "about" :About,
            "educational_courses" :courses,
            "previous_experience" :experience,
            "qualification" :Moahed_id ?? qualification_id ?? "",
            "fk_city" :city_id ?? 0,
            "fk_region" :Region_id ?? 0,
            ] as [String : Any]
        
        var imagesData = [Data]()
        var imagesKeys = [String]()
        
        for i in images{
            imagesData.append(i.imgData)
            imagesKeys.append("Img")
        }
        startAnimating()
        
        API.POSTImage(url: URLs.UpdateUserData, Images: imagesData, Keys: imagesKeys, header: [:], parameters: parameters) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    let msg = dict["msg"]
                    
                    let alert = UIAlertController(title: "ERROR".localized(), message: msg as? String, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                case 1:
                    if let userInfo = dict["client"] as? [String: Any]{
                        
                        let def = UserDefaults.standard
                        let Id = userInfo["Id"] as? Int ?? 0
                        def.set(Id, forKey: "Id")
                        
                        
                        
                        let token = userInfo["token"] as? String ?? ""
                        def.set(token, forKey: "token")
                        
                        
                        
                        let name = userInfo["name"] as? String ?? ""
                        def.set(name, forKey: "name")
                        
                        
                        
                        let img = userInfo["img"] as? String ?? ""
                        def.set(img, forKey: "img")
                        

                        let msg = dict["msg"]
                        
                        let alert = UIAlertController(title: "Done".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
    

    
    func GetCity() {
        let body = ["lang" : Language.currentLanguage().contains("en") ? "en" : "ar",]
        
        startAnimating()
        API.POST(url: URLs.GetCities, parameters: body, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                let dict = value
                
                if let v = dict["key"] as? Int{
                    
                    switch v {
                    case  0:
                        
                        let msg = dict["msg"]
                        
                        let alert = UIAlertController(title: "error".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    case 1:
                        
                        if let City = dict["City"] as? [[String: Any]]{
                            
                            for a in City{
                                let id = a["Id"] as! Int
                                let name = a["name"] as! String
                                self.City.append(CityModel(id: id, name: name))
                                
                            }
                            
                            
                        }else{
                            self.stopAnimating()
                            
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                        
                        
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    
    
    func getRegion() {
        let body = ["lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
                    "city_id" : city_id!
            ] as [String : Any]
        
        startAnimating()
        API.POST(url: URLs.GetRegion, parameters: body, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                let dict = value
                
                if let v = dict["key"] as? Int{
                    
                    switch v {
                    case  0:
                        
                        let msg = dict["msg"]
                        
                        let alert = UIAlertController(title: "error".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    case 1:
                        
                        if let Region = dict["Region"] as? [[String: Any]]{
                            
                    
                            if self.emptyCity{
                                self.profileView.CityTf.isUserInteractionEnabled = true
                                self.profileView.CityTf.inputView = self.pickerViewCity
                            }
                            
                            self.Region = []
                            for a in Region{
                                let id = a["Id"] as! Int
                                let name = a["name"] as! String
                                self.Region.append(RegionModel(id: id, name: name))
                                
                            }
                            
                            
                        }else{
                            self.stopAnimating()
                            
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                        
                        
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    
    func GetJob() {
        let body = ["lang" : Language.currentLanguage().contains("en") ? "en" : "ar",]
        
        startAnimating()
        API.POST(url: URLs.GetJob_title, parameters: body, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                let dict = value
                
                if let v = dict["key"] as? Int{
                    
                    switch v {
                    case  0:
                        
                        let msg = dict["msg"]
                        
                        let alert = UIAlertController(title: "error".localized(), message: msg as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    case 1:
                        
                        if let jobs = dict["Job_title"] as? [[String: Any]]{
                            
                            for a in jobs{
                                let id = a["Id"] as! Int
                                let name = a["name"] as! String
                                self.Jobs.append(JobModel(id: id, name: name))
                                
                            }
                            
                            
                        }else{
                            self.stopAnimating()
                            
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                        
                        
                    default : print(" ")
                    }
                }
                
            }
        }
    }

}




