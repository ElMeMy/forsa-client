//
//  NewPassword.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/24/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

extension NewPasswordVc {
    
    func resendPassword()
    {
        guard let code = PasswordLogin.Code.text , !(PasswordLogin.Code.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Code Is Requried".localized())}
        
        guard let current_pass = PasswordLogin.NewPassword.text , !(PasswordLogin.NewPassword.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "New Password Is Requried".localized())}

        guard let ConfirmPassword = PasswordLogin.confirmPassword.text , !(PasswordLogin.confirmPassword.text?.isEmpty)! else {return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Confirm Password Is Requried".localized())}

        guard current_pass == ConfirmPassword else
        {
           return Alert.showAlertOnVC(target: self, title: "Requried".localized(), message: "Password and Confirm Password Must be Equal".localized())
        }
        
        
        
        let parameters = [
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            "code" :code,
            "current_pass" :current_pass,
            "phone" :phone!,
            ] as [String : Any]
        
        
        startAnimating()
        
        API.POST(url: URLs.ChangePasswordByCode, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    let msg = dict["msg"]
                    
                    let alert = UIAlertController(title: "ERROR".localized(), message: msg as? String, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: { (_) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                case 1:
                    let vc = Storyboard.Authentication.instantiate(LoginClientVc.self)
                    self.present(vc, animated: true, completion: nil)
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
    
}
