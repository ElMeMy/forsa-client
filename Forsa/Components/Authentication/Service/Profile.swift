//
//  Profile.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/20/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

extension ProfileVc {
    func Profile()
    {
        let user_id = UserDefaults.standard.value(forKey: "Id") as? Int ?? 0
        
        let parameters = [
            "client_id"    : user_id,
            "lang" : Language.currentLanguage().contains("en") ? "en" : "ar",
            ] as [String : Any]
        
        startAnimating()
        API.POST(url: URLs.GetDataOfClient, parameters: parameters, headers: nil) { (success, value) in
            if success{
                self.stopAnimating()
                
                let key = value["key"] as! Int
                let dict = value
                
                switch key {
                case  0:
                    break
                case 1:
                    if let data = dict["client"]{
                    
                        let user_name = data["user_name"] as! String
                        self.profileView.UserNameTf.text = user_name
                        
                        let name = data["name"] as! String
                        self.profileView.NameTf.text = name
                        
                        let phone = data["phone"] as! String
                        self.profileView.PhoneTf.text = phone

                        let email = data["email"] as! String
                        self.profileView.EmailTf.text = email

                        let city = data["city"] as! String
                        self.profileView.CityTf.text = city

                        let Region = data["Region"] as! String
                        self.profileView.RegionTf.text = Region
                        
//                        let Job_title = data["Job_title"] as! String
//                        self.profileView.JopTitle.text = Job_title

                        let qualification = data["qualification"] as! String
                        self.profileView.QualificationTg.text = qualification
                        
                        let previous_experience = data["previous_experience"] as! String
                        self.profileView.experienceTf.text = previous_experience

                        let educational_courses = data["educational_courses"] as! String
                        self.profileView.coursesTf.text = educational_courses

                        let about = data["about"] as! String
                        self.profileView.AboutTf.text = about

                        let avatar = data["img"] as! String
                        self.profileView.ProfileImage.setImageWith(avatar)
                        
                        
                        
                        
                        
                    }else{
                        self.stopAnimating()
                        
                        //weak internet
                        let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                    
                    
                default : print(" ")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
}
