//
//  ResendPassword.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/24/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class ResendPassword: UIView {

    @IBOutlet weak var BackButton: UIButton!
    @IBOutlet weak var BgImg: UIImageView!
    @IBOutlet weak var LogoImage: UIImageView!
    @IBOutlet weak var Code: TextFieldRadius!
    @IBOutlet weak var NewPassword: TextFieldRadius!
    @IBOutlet weak var confirmPassword: TextFieldRadius!


}
