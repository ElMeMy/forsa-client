//
//  ProfileVIew.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/20/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class ProfileVIew: UIView {
    
    @IBOutlet weak var BackButton: UIButton!
    @IBOutlet weak var EditButton: UIButton!
    @IBOutlet weak var NameTf: TextFieldRadius!
    @IBOutlet weak var UserNameTf: TextFieldRadius!
    @IBOutlet weak var PhoneTf: TextFieldRadius!
    @IBOutlet weak var EmailTf: TextFieldRadius!
    @IBOutlet weak var CityTf: TextFieldRadius!
    @IBOutlet weak var RegionTf: TextFieldRadius!
    @IBOutlet weak var JopTitle: TextFieldRadius!
    @IBOutlet weak var QualificationTg: TextFieldRadius!
    @IBOutlet weak var experienceTf: TextFieldRadius!
    @IBOutlet weak var coursesTf: TextFieldRadius!
    @IBOutlet weak var AboutTf: UITextView!
    @IBOutlet weak var ProfileImage: CircleImageView!
    
}
