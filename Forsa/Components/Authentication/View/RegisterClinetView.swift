//
//  RegisterClinetView.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/23/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit
import BEMCheckBox

class RegisterClinetView: UIView {
    
    @IBOutlet weak var BackButton: UIButton!
    @IBOutlet weak var NameTf: TextFieldRadius!
    @IBOutlet weak var UserNameTf: TextFieldRadius!
    @IBOutlet weak var IdNumberTf: TextFieldRadius!
    @IBOutlet weak var PhoneTf: TextFieldRadius!
    @IBOutlet weak var EmailTf: TextFieldRadius!
    @IBOutlet weak var CityTf: TextFieldRadius!
    @IBOutlet weak var RegionTf: TextFieldRadius!
    @IBOutlet weak var JopTitle: TextFieldRadius!
    @IBOutlet weak var QualificationTg: TextFieldRadius!
    @IBOutlet weak var experienceTf: TextFieldRadius!
    @IBOutlet weak var coursesTf: TextFieldRadius!
    @IBOutlet weak var PasswordTf: TextFieldRadius!
    @IBOutlet weak var downImage: UIImageView!
    @IBOutlet weak var down2Image: UIImageView!
    @IBOutlet weak var down3Image: UIImageView!
    @IBOutlet weak var down4Image: UIImageView!
    @IBOutlet weak var down5Image: UIImageView!
    @IBOutlet weak var ClenderImage: UIImageView!
    @IBOutlet weak var GenderTf: TextFieldRadius!
    @IBOutlet weak var DateTf: TextFieldRadius!
    @IBOutlet weak var AcceptTerms: BEMCheckBox!
    
    
}
