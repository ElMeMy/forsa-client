//
//  LoginClientView.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/17/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class LoginClientView: UIView {
    
    @IBOutlet weak var BgImg: UIImageView!
    @IBOutlet weak var LogoImg: UIImageView!
    @IBOutlet weak var UserNameTf: TextFieldRadius!
    @IBOutlet weak var PasswordTf: TextFieldRadius!

}
