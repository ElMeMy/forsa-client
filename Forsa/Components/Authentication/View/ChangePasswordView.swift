//
//  ChangePasswordView.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/24/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//

import UIKit

class ChangePasswordView: UIView {

    @IBOutlet weak var BackButton: UIButton!
    @IBOutlet weak var NewPasswordEdit: TextFieldRadius!
    @IBOutlet weak var OldPasswordEdit: TextFieldRadius!
    @IBOutlet weak var BgImg: UIImageView!
    @IBOutlet weak var LogoImg: UIImageView!

}
