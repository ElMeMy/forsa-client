//
//  EditProfileExtent.swift
//  Forsa
//
//  Created by ahmed elmemy on 6/23/19.
//  Copyright © 2019 ahmed elmemy. All rights reserved.
//


import UIKit

extension EditProfileVc: UIImagePickerControllerDelegate ,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var selectedImage: UIImage?
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            let pic = image.jpegData(compressionQuality: 0.75)
            
            self.images.append(AdvImage(img: image, imgData: pic!))
            
            profileView.ProfileImage.image = images[0].img

            
            picker.dismiss(animated: true, completion: nil)
            
        }
            
        else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            let imageData:Data = (image).pngData()! as Data
            
            self.images.append(AdvImage(img: image, imgData: imageData))
            picker.dismiss(animated: true, completion: nil)
            
        }
        
        
    }
}


extension EditProfileVc: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1
        {
            return City.count
        }
        else if pickerView.tag == 2
        {
            return Region.count
        }
        else if pickerView.tag == 3
        {
            return Jobs.count
        }
        else if pickerView.tag == 4
        {
            return Moahel.count
        }

        return 1
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        if pickerView.tag == 1
        {
            return City[row].name
        }
        else if pickerView.tag == 2
        {
            return Region[row].name
        }
        else if pickerView.tag == 3
        {
            return Jobs[row].name
        }
        else if pickerView.tag == 4
        {
            return Moahel[row].name
        }
        return ""
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1
        {
            self.profileView.CityTf.text = self.City[row].name
            city_id = City[row].id!
            getRegion()

        }
        else if pickerView.tag == 2
        {
            self.profileView.RegionTf.text = self.Region[row].name
            Region_id = Region[row].id!
        }
        
        if pickerView.tag == 3
        {
            self.profileView.JopTitle.text = self.Jobs[row].name
            Job_id = Jobs[row].id!            
        }
        if pickerView.tag == 4
        {
            self.profileView.QualificationTg.text = self.Moahel[row].name
            Moahed_id = Moahel[row].id
        }
        
    }
    
}
